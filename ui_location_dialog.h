﻿/********************************************************************************
** Form generated from reading UI file 'location_dialog.ui'
**
** Created: Tue 24. May 19:46:01 2011
**      by: Qt User Interface Compiler version 4.7.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOCATION_DIALOG_H
#define UI_LOCATION_DIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGraphicsView>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpinBox>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Location_dialog
{
public:
    QHBoxLayout *horizontalLayout;
    QTreeWidget *solare_list;
    QVBoxLayout *verticalLayout;
    QLabel *coordinates;
    QComboBox *comboBox;
    QGraphicsView *graphicsView;
    QGridLayout *gridLayout;
    QLabel *lat_label;
    QLabel *log_label;
    QSpinBox *lat_deg;
    QSpinBox *log_deg;
    QSpinBox *lat_minute;
    QSpinBox *log_minute;
    QSpinBox *lat_second;
    QSpinBox *log_second;
    QComboBox *northern_South;
    QComboBox *east_west;
    QLabel *deg;
    QLabel *minute;
    QLabel *second;
    QLabel *height_label;
    QSpinBox *height;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *Location_dialog)
    {
        if (Location_dialog->objectName().isEmpty())
            Location_dialog->setObjectName(QString::fromUtf8("Location_dialog"));
        Location_dialog->resize(664, 421);
        horizontalLayout = new QHBoxLayout(Location_dialog);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        solare_list = new QTreeWidget(Location_dialog);
        solare_list->setObjectName(QString::fromUtf8("solare_list"));
        solare_list->setMinimumSize(QSize(211, 0));
        solare_list->setMaximumSize(QSize(211, 16777215));

        horizontalLayout->addWidget(solare_list);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        coordinates = new QLabel(Location_dialog);
        coordinates->setObjectName(QString::fromUtf8("coordinates"));

        verticalLayout->addWidget(coordinates);

        comboBox = new QComboBox(Location_dialog);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        verticalLayout->addWidget(comboBox);

        graphicsView = new QGraphicsView(Location_dialog);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));

        verticalLayout->addWidget(graphicsView);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lat_label = new QLabel(Location_dialog);
        lat_label->setObjectName(QString::fromUtf8("lat_label"));

        gridLayout->addWidget(lat_label, 3, 0, 1, 1);

        log_label = new QLabel(Location_dialog);
        log_label->setObjectName(QString::fromUtf8("log_label"));

        gridLayout->addWidget(log_label, 4, 0, 1, 1);

        lat_deg = new QSpinBox(Location_dialog);
        lat_deg->setObjectName(QString::fromUtf8("lat_deg"));
        lat_deg->setMinimumSize(QSize(60, 20));
        lat_deg->setMaximumSize(QSize(60, 20));
        lat_deg->setMinimum(0);
        lat_deg->setMaximum(90);

        gridLayout->addWidget(lat_deg, 3, 1, 1, 1);

        log_deg = new QSpinBox(Location_dialog);
        log_deg->setObjectName(QString::fromUtf8("log_deg"));
        log_deg->setMinimumSize(QSize(60, 20));
        log_deg->setMaximumSize(QSize(60, 20));
        log_deg->setMinimum(0);
        log_deg->setMaximum(180);

        gridLayout->addWidget(log_deg, 4, 1, 1, 1);

        lat_minute = new QSpinBox(Location_dialog);
        lat_minute->setObjectName(QString::fromUtf8("lat_minute"));
        lat_minute->setMinimumSize(QSize(60, 20));
        lat_minute->setMaximumSize(QSize(60, 20));
        lat_minute->setMaximum(60);

        gridLayout->addWidget(lat_minute, 3, 2, 1, 1);

        log_minute = new QSpinBox(Location_dialog);
        log_minute->setObjectName(QString::fromUtf8("log_minute"));
        log_minute->setMinimumSize(QSize(60, 20));
        log_minute->setMaximumSize(QSize(60, 20));
        log_minute->setMaximum(60);

        gridLayout->addWidget(log_minute, 4, 2, 1, 1);

        lat_second = new QSpinBox(Location_dialog);
        lat_second->setObjectName(QString::fromUtf8("lat_second"));
        lat_second->setMinimumSize(QSize(60, 20));
        lat_second->setMaximumSize(QSize(60, 20));
        lat_second->setMaximum(60);

        gridLayout->addWidget(lat_second, 3, 3, 1, 1);

        log_second = new QSpinBox(Location_dialog);
        log_second->setObjectName(QString::fromUtf8("log_second"));
        log_second->setMinimumSize(QSize(60, 20));
        log_second->setMaximumSize(QSize(60, 20));
        log_second->setMaximum(60);

        gridLayout->addWidget(log_second, 4, 3, 1, 1);

        northern_South = new QComboBox(Location_dialog);
        northern_South->setObjectName(QString::fromUtf8("northern_South"));

        gridLayout->addWidget(northern_South, 3, 4, 1, 1);

        east_west = new QComboBox(Location_dialog);
        east_west->setObjectName(QString::fromUtf8("east_west"));

        gridLayout->addWidget(east_west, 4, 4, 1, 1);

        deg = new QLabel(Location_dialog);
        deg->setObjectName(QString::fromUtf8("deg"));

        gridLayout->addWidget(deg, 2, 1, 1, 1);

        minute = new QLabel(Location_dialog);
        minute->setObjectName(QString::fromUtf8("minute"));

        gridLayout->addWidget(minute, 2, 2, 1, 1);

        second = new QLabel(Location_dialog);
        second->setObjectName(QString::fromUtf8("second"));

        gridLayout->addWidget(second, 2, 3, 1, 1);

        height_label = new QLabel(Location_dialog);
        height_label->setObjectName(QString::fromUtf8("height_label"));

        gridLayout->addWidget(height_label, 5, 0, 1, 1);

        height = new QSpinBox(Location_dialog);
        height->setObjectName(QString::fromUtf8("height"));
        height->setMinimumSize(QSize(60, 20));
        height->setMaximumSize(QSize(60, 20));
        height->setMaximum(5000);

        gridLayout->addWidget(height, 5, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        buttonBox = new QDialogButtonBox(Location_dialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        horizontalLayout->addLayout(verticalLayout);


        retranslateUi(Location_dialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), Location_dialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Location_dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(Location_dialog);
    } // setupUi

    void retranslateUi(QDialog *Location_dialog)
    {
        Location_dialog->setWindowTitle(QApplication::translate("Location_dialog", "Dialog", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = solare_list->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("Location_dialog", "\320\276\320\261\321\212\320\265\320\272\321\202\321\213 \321\201\320\276\320\273\320\275\320\265\321\207\320\275\320\276\320\271 \321\201\320\270\321\201\321\202\320\265\320\274\321\213", 0, QApplication::UnicodeUTF8));
        coordinates->setText(QApplication::translate("Location_dialog", "\320\223\320\265\320\276\320\263\321\200\320\260\321\204\320\270\321\207\320\265\321\201\320\272\320\270\320\265 \320\272\320\276\320\276\321\200\320\264\320\270\320\275\320\260\321\202\321\213", 0, QApplication::UnicodeUTF8));
        lat_label->setText(QApplication::translate("Location_dialog", "\321\210\320\270\321\200\320\276\321\202\320\260", 0, QApplication::UnicodeUTF8));
        log_label->setText(QApplication::translate("Location_dialog", "\320\224\320\276\320\273\320\263\320\276\321\202\320\260", 0, QApplication::UnicodeUTF8));
        northern_South->clear();
        northern_South->insertItems(0, QStringList()
         << QApplication::translate("Location_dialog", "\321\201\320\265\320\262\320\265\321\200\320\275\320\260\321\217", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("Location_dialog", "\321\216\320\266\320\275\320\260\321\217", 0, QApplication::UnicodeUTF8)
        );
        east_west->clear();
        east_west->insertItems(0, QStringList()
         << QApplication::translate("Location_dialog", "\320\262\320\276\321\201\321\202\320\276\321\207\320\275\320\260\321\217", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("Location_dialog", "\320\267\320\260\320\277\320\260\320\264\320\275\320\260\321\217", 0, QApplication::UnicodeUTF8)
        );
        deg->setText(QApplication::translate("Location_dialog", "\320\263\321\200\320\260\320\264\321\203\321\201\321\213", 0, QApplication::UnicodeUTF8));
        minute->setText(QApplication::translate("Location_dialog", "\320\274\320\270\320\275\321\203\321\202\321\213", 0, QApplication::UnicodeUTF8));
        second->setText(QApplication::translate("Location_dialog", "\321\201\320\265\320\272\321\203\320\275\320\264\321\213", 0, QApplication::UnicodeUTF8));
        height_label->setText(QApplication::translate("Location_dialog", "\320\222\321\213\321\201\320\276\321\202\320\260(\320\272\320\274)", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Location_dialog: public Ui_Location_dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOCATION_DIALOG_H
