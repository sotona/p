# -------------------------------------------------
# Project created by QtCreator 2010-02-16T11:01:25
#
# requairements FTGL, freetype, YAML
# -------------------------------------------------
QT += opengl
TARGET = p
TEMPLATE = app
#QTPLUGIN     += qjpeg qgif qmng
HEADERS += include/op_info.h \
#    include/calculation/APC_VecMat3D.h \
#    include/calculation/APC_Time.h \
#    include/calculation/APC_Sun.h \
#    include/calculation/APC_Spheric.h \
#    include/calculation/APC_PrecNut.h \
#    include/calculation/APC_Planets.h \
#    include/calculation/APC_Phys.h \
#    include/calculation/APC_Moon.h \
#    include/calculation/APC_Math.h \
#    include/calculation/APC_Kepler.h \
#    include/calculation/APC_IO.h \
#    include/calculation/APC_DE.h \
#    include/calculation/APC_Const.h \
#    include/calculation/APC_Cheb.h \
    include/feedback.h \
    include/e2/whatIsee.h \
    include/e2/oculus.h \
    include/e2/observer.h \
    include/e2/location.h \
    include/e2/engine2.h \
    include/v/visualization.h \
    include/v/screen.h \
    include/v/g_container.h \
    include/e2/e/pModel.h \
    include/e2/e/mundus.h \
    include/e2/e/elementus_mundus.h \
    include/e2/e/deserialization.h \
    include/e2/e/catalogues.h \
    keyboard.h \
    kbrd.h \
    invisible_interface.h \
    initialization.h \
    include/e2/e/astro_db.h \
    include/e2/e2.h \
    gui/window.h \
    gui/ui_setting.h \
#    gui/ui_menu_2.h \
#    gui/ui_menu.h \
    gui/ui_find_dialog.h \
    gui/menu.h \
    gui/glwidget.h \
    gui/perception_settings.h \
    gui/location_dialog.h \
    include/e2/e/coordinate.h \
    include/e2/e/vivus_tempus.h \
    include/e2/e/tempus.h \
    gui/find.h \
    include/straux.h \
    include/e2/qwerty.h \
    include/e2/e/cat_definitions.h \
    include/e2/telescope.h \
    include/e2/e/astro_types.h \
    gui/cats.h
SOURCES +=  include/op_info.cpp \
#    include/calculation/APC_VecMat3D.cpp \
#    include/calculation/APC_Time.cpp \
#    include/calculation/APC_Sun.cpp \
#    include/calculation/APC_Spheric.cpp \
#    include/calculation/APC_PrecNut.cpp \
#    include/calculation/APC_Planets.cpp \
#    include/calculation/APC_Phys.cpp \
#    include/calculation/APC_Moon.cpp \
#    include/calculation/APC_Math.cpp \
#    include/calculation/APC_Kepler.cpp \
#    include/calculation/APC_IO.cpp \
#    include/calculation/APC_DE.cpp \
#    include/calculation/APC_Cheb.cpp \
    include/feedback.cpp \
    include/e2/whatISee.cpp \
    include/e2/oculus.cpp \
    include/e2/observer.cpp \
    include/e2/location.cpp \
    include/v/visualization.cpp \
    include/v/screen.cpp \
    include/v/g_container.cpp \
    include/e2/e/pModel.cpp \
    include/e2/e/mundus.cpp \
    include/e2/e/elementus_mundus.cpp \
    include/e2/e/deserialization.cpp \
    include/e2/e/catalogues.cpp \
    main.cpp \
    keyboard.cpp \
    invisible_interface.cpp \
    initialization.cpp \
    include/e2/e/astro_db.cpp \
    include/e2/e2.cpp \
    gui/window.cpp \
    gui/menu.cpp \
    gui/glwidget.cpp \
    gui/perception_settings.cpp \
    gui/location_dialog.cpp \
    include/e2/e/coordinate.cpp \
    include/e2/e/vivus_tempus.cpp \
    include/e2/e/tempus.cpp \
    gui/find.cpp \
    include/e2/e/cat_definitions.cpp \
    include/straux.cpp \
    include/e2/telescope.cpp \
    include/e2/e/astro_types.cpp \
    gui/cats.cpp

INCLUDEPATH += 3rdparty/freetype/include
INCLUDEPATH += 3rdparty/ftgl/src
INCLUDEPATH += 3rdparty/yaml/include
INCLUDEPATH += 3rdparty/yaml/src

LIBS += l/freetype.a
LIBS += l/ftgl.dll.a
LIBS += l/ftgl_D.dll.a
LIBS += l/yamlcppd.a
LIBS += l/yamlcpp.a

FORMS += \
    gui/menu.ui \
    gui/perception_settings.ui \
    gui/location_dialog.ui \
    gui/find.ui \
    gui/dialog.ui \
    gui/cats.ui








