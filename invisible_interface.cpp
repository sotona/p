#include "invisible_interface.h"

void add_user_mark(int x, int y, observer::Observer &o){
GLint viewport[4];
GLdouble mvmatrix[16], projmatrix[16];
glGetIntegerv(GL_VIEWPORT,viewport);
glGetDoublev(GL_MODELVIEW_MATRIX,mvmatrix);
glGetDoublev(GL_PROJECTION_MATRIX,projmatrix);
GLdouble xyz[3];
gluUnProject(x,y,0,mvmatrix,projmatrix,viewport,&xyz[0],&xyz[1],&xyz[2]);
coordinates::Coordinate c = coordinates::converting::CartesianToSpher(xyz);
c.Range(coordinates::very_far/2);
//elementus_mundus::altus_mundus::Caelestis* mark = new elementus_mundus::altus_mundus::Caelestis(c,3);
//o.addUserMark(mark);
}
