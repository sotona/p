﻿
#include <map>
#include <QDialog>
#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QTextCodec>
#include <QCheckBox>
#include "keyboard.h"
#include "gui/perception_settings.h"
#include "gui/location_dialog.h"
#include "gui/find.h"
#include "gui/cats.h"

Interface::Interface(std::map<int, KEYB_PROC> _key_actions, E2 _e2){
e2=_e2 ,key_actions=_key_actions;}

 int Interface::do_something(QKeyEvent *e){
unsigned key =  e->key();

switch (key){
case Qt::Key_I: {switch (e->modifiers()){
                 case Qt::ControlModifier: this->dec_scr_info(); break;
                 case Qt::ShiftModifier:   this->inc_scr_info(); break;
                 default:                  this->shide_scr_info(); break;}
case Qt::Key_S: if (e->modifiers()==Qt::ControlModifier) this->screenshot(); break;}}

KEYB_PROC foo=key_actions[key];
if (foo!=NULL)foo(e2,e->modifiers());
return 0;}

void Interface::recv_asction(QAction *a){
a->data();
a->text();
}

/**/
namespace key_actions{
//----------------------------------------------
int left(E2& e2, int mode){

if (!e2.location->on_ground && mode==Qt::AltModifier)
e2.location->space_pos.Alpha(e2.location->space_pos.Alpha()+hor_quantum);
else e2.Galileo_Galilei->eye.Left(hor_quantum);
return 0;}
//----------------------------------------------
int right(E2& e2, int mode){

if (!e2.location->on_ground && mode==Qt::AltModifier)
e2.location->space_pos.Alpha(e2.location->space_pos.Alpha()-hor_quantum);
else e2.Galileo_Galilei->eye.Left(-hor_quantum);
return 0;}
//----------------------------------------------
int up(E2& e2, int mode){

if (!e2.location->on_ground && mode==Qt::ControlModifier)
e2.location->space_pos.Sigma(e2.location->space_pos.Sigma()+hor_quantum);
else e2.Galileo_Galilei->eye.Up(vert_quantum);
return 0;}
//----------------------------------------------
int down(E2& e2, int mode){

if (!e2.location->on_ground && mode==Qt::ControlModifier)
e2.location->space_pos.Sigma(e2.location->space_pos.Sigma()-hor_quantum);
else e2.Galileo_Galilei->eye.Up(-vert_quantum);
return 0;}
//----------------------------------------------
int inc_time_speed(E2& e2, int mode){
short x=1;
switch(mode){
case Qt::ControlModifier: x=0.25; break;
case Qt::ShiftModifier:   x=4;    break;}
e2.umodel->lifeTime.X(e2.umodel->lifeTime.X()*time_quantumX*x);
return 0;}
//----------------------------------------------
int dec_time_speed(E2& e2, int mode){
short x=1;
switch(mode){
case Qt::ControlModifier: x=0.25; break;
case Qt::ShiftModifier:   x=4;    break;}
e2.umodel->lifeTime.X(e2.umodel->lifeTime.X()/time_quantumX*x);
return 0;}
//----------------------------------------------
int time_stort(E2& e2, int mode){
e2.umodel->lifeTime.stort(!e2.umodel->lifeTime.is_stort());
return 0;}
//----------------------------------------------
int setdatetime(E2& e2,int mode){
QTextCodec::setCodecForCStrings(QTextCodec::codecForName("windows-1251"));
QDialog dialog( NULL,Qt::CustomizeWindowHint | Qt::WindowTitleHint);
dialog.setWindowTitle("Дата/Время");
QDialogButtonBox dialogbutton(QDialogButtonBox::Ok| QDialogButtonBox::Cancel);
QObject::connect(&dialogbutton, SIGNAL(accepted()), &dialog, SLOT(accept()));
QObject::connect(&dialogbutton, SIGNAL(rejected()), &dialog, SLOT(reject()));

QHBoxLayout layout;
QDateTimeEdit dte;
QDateTime tmp;
tmp=e2.umodel->lifeTime.Time;
QObject::connect(&dte,SIGNAL(dateTimeChanged(QDateTime)),&e2.umodel->lifeTime,SLOT(setDateTime(QDateTime)));
dte.setDateTime(e2.umodel->lifeTime.Time);
layout.addWidget(&dte);
layout.addWidget(&dialogbutton);
dialog.setLayout(&layout);
if (dialog.exec())
e2.umodel->lifeTime.Time=dte.dateTime();
else
e2.umodel->lifeTime.Time=tmp;
return 0;}

int set_back_time(E2& e2, int mode){
e2.umodel->lifeTime.X(-1*e2.umodel->lifeTime.X());
return 0;}
//----------------------------------------------
int observer_settings(E2& e2, int mode){
using namespace electum_necessarius::universe;
Setting _setting;
//if (e2.Galileo_Galilei->perception.what_i_see.universe[ss::planet])
electum_necessarius::whatISee wIs;
wIs=e2.Galileo_Galilei->perception.what_i_see;
_setting.setWhatISee(e2.Galileo_Galilei->perception.what_i_see);
if(!_setting.exec())
e2.Galileo_Galilei->perception.what_i_see=wIs;
return 0;}

int set_location(E2& e2, int mode){
using namespace electum_necessarius::universe;
location::Location *l=new location::Location();
l->setPlanet(e2.location->getPlanet());
l->gp=e2.location->gp;
l->on_ground=e2.location->on_ground;
l->space_pos=e2.location->space_pos;
//if (e2.Galileo_Galilei->perception.what_i_see.universe[ss::planet])
Location_dialog location_d(*e2.location);
location_d.fillTree(e2.adb->data->getNativeSS());

if(!location_d.exec()){
    e2.location->setPlanet(l->getPlanet());
    e2.location->gp=l->gp;
    e2.location->on_ground=l->on_ground;
    e2.location->space_pos=l->space_pos;
}return 0;}
//----------------------------------------------
int a_key(E2& e2, int mode){
using namespace electum_necessarius::imagination;
using namespace electum_necessarius::universe;
/*if (e.modifiers()==Qt::AltModifier) e2.make_asterizm=!e2.make_asterizm;
else */if (mode==Qt::AltModifier) //астеризмы в(ы)кл.
e2.Galileo_Galilei->perception.what_i_see.things.flip(electum_necessarius::imagination::asterism);
else e2.Galileo_Galilei->perception.what_i_see.things.flip(electum_necessarius::universe::ds::astra);
return 0;}
//----------------------------------------------
int grid(E2& e2, int mode){
using namespace electum_necessarius::imagination::grid;
if (mode==Qt::ControlModifier)
e2.Galileo_Galilei->perception.what_i_see.things[az]=!e2.Galileo_Galilei->perception.what_i_see.things[az];
else if (mode==Qt::AltModifier) e2.Galileo_Galilei->perception.what_i_see.things[ec]=
                        !e2.Galileo_Galilei->perception.what_i_see.things[ec];
else e2.Galileo_Galilei->perception.what_i_see.things[eq]=!e2.Galileo_Galilei->perception.what_i_see.things[eq];
return 0;}
//----------------------------------------------
//int fullscreen(E2& e2,const QKeyEvent &e){
//if (e2.isFullScreen()) e2.showNormal();
//else e2.showFullScreen();
//return 0;}
//----------------------------------------------
int esc(E2& e2, int mode){
e2.adb->setNoAsctiveId();
return 0;}
//----------------------------------------------
int set_current_time(E2& e2, int mode){
e2.umodel->lifeTime.setCurrentTime();
return 0;}
//----------------------------------------------
int normal_time_speed(E2& e2, int mode){
e2.umodel->lifeTime.X(1.0);
return 0;}
//----------------------------------------------
int n_astr(E2& e2, int mode){
using namespace electum_necessarius::universe;
e2.Galileo_Galilei->perception.what_i_see.things.flip(ds::dso);
//if (e.modifiers()==Qt::ControlModifier)to_file("asterizm","\n","")
return 0;}
//----------------------------------------------
int c_key(E2& e2, int mode){
using namespace electum_necessarius::universe;
switch (mode){
case Qt::ControlModifier:  set_central_planet(e2); break;
case Qt::AltModifier:      e2.Galileo_Galilei->perception.what_i_see.things.flip(ds::caelestis); break;
case Qt::ShiftModifier:    e2.Galileo_Galilei->perception.what_i_see.things.flip(ps::comet); break;
}

return 0;}
//----------------------------------------------
int sh_planets(E2& e2, int mode){
using namespace electum_necessarius::universe;
e2.Galileo_Galilei->perception.what_i_see.things.flip(ps::planet);
return 0;}
//----------------------------------------------
int to_from_space(E2& e2, int mode){
e2.location->on_ground=!e2.location->on_ground;
if (!e2.location->on_ground){
e2.location->space_pos = e2.location->getPlanet()->coordinate;
e2.location->space_pos.Range(e2.location->space_pos.Range()+.05);
e2.location->space_pos.Alpha(e2.location->space_pos.Alpha()-15);
}
if (mode==Qt::ControlModifier){
}
return 0;}

int sh_orbit(E2& e2, int mode){
using namespace electum_necessarius::imagination;
switch(mode){
case Qt::NoModifier: e2.Galileo_Galilei->perception.what_i_see.things.flip(orbit); break;
case Qt::ControlModifier: e2.Galileo_Galilei->perception.what_i_see.things.flip(electum_necessarius::imagination::orbit_features); break;
}
return 0;}

int space_forvard(E2& e2, int mode){
int _mode=2;
if (mode==Qt::ShiftModifier) _mode=3;
else if (mode==Qt::AltModifier) _mode=1;
motion::along_radius(*e2.location,_mode,-1);
return 0;}

int space_backward(E2& e2, int mode){
int _mode=2;
if (mode==Qt::ShiftModifier) _mode=3;
else if (mode==Qt::AltModifier) _mode=1;
motion::along_radius(*e2.location,_mode,+1);
return 0;}

int ground_to_planet(E2& e2, int mode){
e2.location->on_ground=1;
//e2._location->setPlanet();
return 0;}

int sh_c_borders(E2& e2, int mode){
using namespace electum_necessarius::imagination;
e2.Galileo_Galilei->perception.what_i_see.things.flip(c_borders);
return 0;}

int sh_ecliptic_disk(E2& e2, int mode){
using namespace electum_necessarius;
bool x = e2.Galileo_Galilei->perception.what_i_see.things[imagination::ecliptic_disc];
e2.Galileo_Galilei->perception.what_i_see.things.flip(imagination::ecliptic_disc);
x = e2.Galileo_Galilei->perception.what_i_see.things[imagination::ecliptic_disc];
return 0;}

int inc_permeability(E2& e2, int mode){
const float d = 0.25;
if (e2.Galileo_Galilei->perception.how_i_see+d<15)
e2.Galileo_Galilei->perception.how_i_see+=d;
return 0;}

int dec_permeability(E2& e2, int mode){
const float d = 0.25;
if (e2.Galileo_Galilei->perception.how_i_see>0.05)
    e2.Galileo_Galilei->perception.how_i_see-=d;
return 0;}

int relative_vmag(E2& e2, int mode){
e2.Galileo_Galilei->perception.relative_vmag=!e2.Galileo_Galilei->perception.relative_vmag;
return 0;}

int find_exec(E2& e2, int mode){
if (mode==Qt::NoModifier){
Find f;
f.setADB(*e2.adb);f.exec();}
else if (mode==Qt::ControlModifier){
Cats cat;
cat.setCats(*e2.cat);
cat.exec();}
return 0;}

int key_d(E2& e2, int mode){
e2.Galileo_Galilei->perception.what_i_see.things.flip(electum_necessarius::universe::ds::ds_image);
return 0;}

}/* end of key actions*/

void set_central_planet(E2& e2){
//const elementus_mundus::proxima_mundus::Planet *p = dynamic_cast<const elementus_mundus::proxima_mundus::Planet*>(e2.adb->selected);
//if (!p) p = dynamic_cast<const elementus_mundus::altus_mundus::Astrum*>(e2.adb->selected);
/*if (p) */
e2.location->setPlanet(e2.adb->selected/*p*/);}


/*********************************/
namespace motion{
void in_space(location::Location& location, int x_motion,int y_motion, double motion_k){
location.space_pos.Alpha(location.space_pos.Alpha()-x_motion*motion_k);
if (location.space_pos.Sigma()-y_motion*motion_k*0.01<89.9)
location.space_pos.Sigma(location.space_pos.Sigma()-y_motion*motion_k);}
//---------------------
void eye(oculus::Oculus &eye, int x_motion, int y_motion, double k_motion){
eye.Left(x_motion*k_motion);
eye.Up(y_motion*k_motion);}
//-------------
void along_radius(location::Location& location,unsigned char accuracy_mode, int _delta){
if (accuracy_mode==2) accuracy_mode*=10;else
if (accuracy_mode==3) accuracy_mode*=500;
double r = location.space_pos.Range();
r+=0.7; pow(r,1.857);
double delta=(1E-4)*accuracy_mode*_delta*r;
double min=elementus_mundus::radius(location.getPlanet());
if (min<0) min = coordinates::astronomical_unit*0.0001;
else  min=min*1.1/coordinates::astronomical_unit;
double max = 50; // 50 a.u.
if (location.space_pos.Range()+delta >min)
if (location.space_pos.Range()+delta <max)
location.space_pos.Range(location.space_pos.Range()+delta);
else location.space_pos.Range(max);
else location.space_pos.Range(min);}
}



void init_keyb_actions(KeyActArray& keyboard_functions){
keyboard_functions[Qt::Key_Left]=key_actions::left;
keyboard_functions[Qt::Key_Right]=key_actions::right;
keyboard_functions[Qt::Key_Up]=key_actions::up;
keyboard_functions[Qt::Key_Down]=key_actions::down;
keyboard_functions[76]=key_actions::inc_time_speed;     //j
keyboard_functions[74]=key_actions::dec_time_speed;     //l
keyboard_functions[93]=key_actions::inc_permeability;  //]
keyboard_functions[91]=key_actions::dec_permeability;  //[
keyboard_functions[75]=key_actions::time_stort;         //k
keyboard_functions[65]=key_actions::a_key;              //a
keyboard_functions[71]=key_actions::grid;               //g   - grid
//keyboard_functions[122]=key_actions::fullscreen;        //f10 - fullscreen
keyboard_functions[16777216]=key_actions::esc;
keyboard_functions[27]=key_actions::esc;
keyboard_functions[186]=key_actions::set_current_time;
keyboard_functions[222]=key_actions::normal_time_speed;
keyboard_functions[78]=key_actions::n_astr;                         // n
keyboard_functions[67]=key_actions::c_key;                          //c
keyboard_functions[80]=key_actions::sh_planets;                     //p
keyboard_functions[83]=key_actions::to_from_space;                  //s
keyboard_functions[79]=key_actions::sh_orbit;                       //o
keyboard_functions[36]=key_actions::space_forvard;                  //home
keyboard_functions[35]=key_actions::space_backward;                  //end
keyboard_functions[66]=key_actions::sh_c_borders;                    //b
keyboard_functions[68]=key_actions::setdatetime;                    //d
keyboard_functions[69]=key_actions::sh_ecliptic_disk;                //e
keyboard_functions[Qt::Key_Backspace]=key_actions::set_back_time;       // baskspace
keyboard_functions[Qt::Key_R]=key_actions::relative_vmag;               // r
keyboard_functions[Qt::Key_F2]=key_actions::setdatetime;
keyboard_functions[Qt::Key_F3]=key_actions::find_exec;              // F3. about catalogues
keyboard_functions[Qt::Key_F4]=key_actions::set_location;
keyboard_functions[Qt::Key_F5]=key_actions::observer_settings;      // F5
keyboard_functions[Qt::Key_D]=key_actions::key_d;                   // deep sky
//keyboard_functions[Qt::Key_F4]=key_actions::set_setting;
//keyboard_functions[Qt::Key_F11]=key_actions::fullscreen;
//keyboard_functions[71]=key_actions::ground_to_planet;
}
