﻿#ifndef INITIALIZATION_H
#define INITIALIZATION_H

#include <fstream>
#include <ostream>
#include <algorithm>
#include <stdio.h>
#include <time.h>
#include <GL/gl.h>
#include <QApplication>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QPixmap>
#include <QSplashScreen>
#include <qgl.h>
#include "3rdparty/yaml/include/yaml.h"
#include "include/e2/e/deserialization.h"
#include "include/e2/e/catalogues.h"
#include "include/v/g_container.h"
#include "include/v/screen.h"
#include "include/e2/e2.h"
#include "include/e2/telescope.h"

extern char* dont_panic;

void is_towler();

namespace file{
namespace log{
extern const char main[];
extern const char gl_info[];}

extern const char splash[];                       // фон для загрузочного окна
extern const char cat_file[];                     //файл c описанием каталогов
extern const char graparam[];                     //  описание графических элементов
extern const char observer[];                     //  настройки наблюдателя
extern const char location[];                     //  настройки положени
extern const char telescope[];                     //  телескопы и окуляры
}

namespace initialization{
QSplashScreen* splash_screen(QApplication &a,const char *splash_file);
bool OGLcheck(const char *);    // return 1 if all ok
/* initialization astronomical and etc classes */
namespace engine{
using  elementus_mundus::proxima_mundus::Planet;
u_model::uModel* pModel(AstroData &ad);
observer::Observer* observer(const char* filename);
location::geo_pos::GeoPos* geoPos();
tempus::VivusTempus* time();
location::Location* Location(const char* filename, const planetary_system::PlanetList& pl);}
/* graphic initialization and etc*/
namespace graphic{
typedef std::map<std::string,std::string> Texture_File;
//-----------
std::map<std::string,char*> texture_names_old(const char* filename);
Texture_File texture_names(const char* filename);
void save_gl_info(const char *file_name);}
/* graphic elements*/
namespace screen_params{
int graphic_elements(screen::plan_elements& p, const char* file);
}

telescope_etc telescope(const char* filename);

} /*end of initialization*/

//return's map [texture_name, file_name]

#endif
