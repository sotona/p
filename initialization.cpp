#include "include/straux.h"
#include "initialization.h"
#include <QDir>
namespace file{
namespace log{
const char main[] =        "p.log";
const char gl_info[] = "gl_info.log";}

const char splash[] =      "splash.png";                       // ��� ��� ������������ ����
const char cat_file[] =    "c/catalogues.yml";                     //���� c ��������� ���������
const char graparam[]=     "settings/graph_elements.yml";          //  �������� ����������� ���������
const char observer[]=     "settings/observer.yml";
const char location[] =    "settings/location.yml";                     //  ��������� ���������
const char telescope[] =    "settings/telescope.yml";                     //  ��������� ���������
}
char* dont_panic = "";
void is_towler(){
if (QDate::currentDate()==QDate(QDate::currentDate().year(),5,6)) {
delete[]dont_panic;dont_panic=strdup("toweler day!");}}

inline void to_file(const char* filename, std::string text){
std::ofstream file(filename,std::ofstream::app);
file << text;
file.close();}

namespace config{
namespace param{
namespace observing{
const std::string perception =      "perception";
const std::string how_i_see =       "how_i_see";
const std::string universe =        "universe";
const std::string astroobkects =    "astroobjects";
const std::string labels_k =        "labels_k";
const std::string labels =          "labels";
const std::string what_i_see =      "what_i_see";
const std::string ds_image =        "ds_image";
const std::string constellation =   "constellation";
const std::string asteroids =       "asteroids";
const std::string planet =          "planet";
const std::string dso =             "dso";
const std::string comet =           "comet";
const std::string caelestis =       "caelestis";
const std::string astra =           "astra";
const std::string imagination =     "imagination";
const std::string orbit_features =  "orbit_features";
const std::string c_borders =       "c_borders";

const std::string grid =            "grid";
const std::string equatorial =      "equatorial";
const std::string azimutal =        "azimutal";
const std::string ecliptic =        "ecliptic";

const std::string asterism =        "asterism";
const std::string orbit =           "orbit";
const std::string ecliptic_disc =   "ecliptic_disc";
const std::string planet_axis =     "planet_axis";

const std::string relative_vmag =   "relative_vmag";

const std::string how_isee =        "how_isee";
const std::string oculus =          "oculus";
const std::string where_i_see =     "where_i_see";
const std::string azimuth =         "azimuth";
const std::string height =          "height";
const std::string fov =             "fov";
}
namespace location{
const std::string planet =          "planet";
const std::string geo_pos =         "geo_pos";
const std::string latitude =        "latitude";
const std::string longitude =       "longitude";
const std::string height =          "height";
const std::string space_pos =       "space_pos";
const std::string range =           "range";
const std::string on_ground =       "on_ground";
}
namespace telescope{
const std::string telescopes =   "telescopes";
const std::string yey_pieces =   "yey pieces";
const std::string f =            "f";
const std::string d =            "d";
const std::string fov =          "fov";
}
}
namespace observerving{
using namespace param::observing;
// ��������� what i see universe ���� labels
void WIS_ul(const YAML::Node& node, electum_necessarius::bitset48 &b){
using namespace electum_necessarius::universe;
using namespace electum_necessarius;
unsigned char x;
node[asterism] >>x;         b[imagination::asterism]=x;
node[astra] >>x;            b[ds::astra]=x;
node[caelestis] >>x;        b[ds::caelestis]=x;
node[constellation] >>x;    b[ds::constellation]=x;
node[dso] >>x;              b[ds::dso]=x;
node[ds_image] >>x;         b[ds::ds_image]=x; // ds_image ��������� � universe
node[planet] >>x;           b[ps::planet]=x;
node[comet] >>x;            b[ps::comet]=x;
node[asteroids] >>x;        b[ps::asteroids]=x;}
//---------------
void perception(const YAML::Node& n, observer::perceptionem::Perceptionem& p){
const YAML::Node &wis = n[param::observing::what_i_see];
using namespace electum_necessarius::universe;
config::observerving::WIS_ul(wis[universe], p.what_i_see.things);
config::observerving::WIS_ul(wis[labels], p.what_i_see.things);
const YAML::Node &his = n[how_i_see];
his[astroobkects] >> p.how_i_see;
his[labels_k] >> p.labels_k;}
//------
void oculus_(const YAML::Node& n, oculus::Oculus& o){
float ffov;
n[fov] >> ffov;    o.setFoV(ffov);
n[azimuth] >>ffov; o.where_i_see.Alpha(ffov);
n[height] >> ffov; o.where_i_see.Sigma(ffov);}}
}

namespace initialization{
QSplashScreen* splash_screen(QApplication &a,const char *splash_file){
std::ofstream log_file(file::log::main,std::ios::app);
//QString s = a.applicationDirPath() + QDir::separator() + QString(splash_file);
QFileInfo ss(splash_file);
//QMessageBox::about(NULL,QString::number(ss.exists()),ss.absoluteFilePath());
QPixmap pixmap;
pixmap.load(ss.absoluteFilePath());
/*� ���� ������� ���� �������� �� 2 ��������... */
int width=(a.desktop()->width()/a.desktop()->height()>16./9.)? a.desktop()->width()/2 : a.desktop()->width();
if (pixmap.isNull()) {log_file << "splash screen image '";
log_file << splash_file;
log_file << "' not found\n";}
else pixmap=pixmap.scaled(width/3,.61*width/3);
QSplashScreen *splash = new QSplashScreen(pixmap);
splash->resize(width/3,.61*width/3);
splash->move((width-splash->width())/2, (a.desktop()->height()-splash->height())/2);
splash->show();
log_file.close();
return splash;}

/******************/
namespace engine{

using  elementus_mundus::proxima_mundus::Planet;
//filename - ���� ������������
//---------------------------------------
u_model::uModel* pModel(AstroData &ad){
u_model::uModel *pm = new u_model::uModel();
pm->setUniverse(ad);
return pm;}

//---------------------------------------
observer::Observer* observer(const char* filename){
YAML::Parser parser;
std::ifstream file(filename,std::iostream::in);
YAML::Node settings_node;
observer::Observer* obs = new observer::Observer();
if (file.is_open()){
try{
parser.Load(file);
parser.GetNextDocument(settings_node);
config::observerving::perception(settings_node[config::param::observing::perception],obs->perception);
config::observerving::oculus_(settings_node[config::param::observing::oculus],obs->eye);
} catch(...) {file.close(); return obs;}}
else
{obs->perception.how_i_see=6.0;  //����� �� ����� ��������
obs->perception.labels_k=.5;}
file.close();
return obs;}

//-------------------------------------
location::geo_pos::GeoPos* geoPos(){
location::geo_pos::GeoPos *gp = new location::geo_pos::GeoPos(/*lat, lon, height*/);
return gp;}
//--------------------------------
tempus::VivusTempus* time(){
tempus::VivusTempus* t = new tempus::VivusTempus(/*time zone*/);
t->X(10.);
return t;}
//--------------------------
location::Location* Location(const char* filename, const planetary_system::PlanetList& pl){
location::Location *l = new location::Location();
std::ifstream f(filename);
if (!f.is_open()) return l;
YAML::Parser parser;
YAML::Node node;
parser.Load(f);
try{
if (parser.GetNextDocument(node)){
using namespace config::param::location;
float x;
unsigned b;
node[on_ground] >> b;
l->on_ground=b;
if (l->on_ground){
std::string planet_name;
node[planet] >> planet_name;
/*����� � ������� �������� ������� ������ �������*/{
char* pl_name = strdup(planet_name.c_str()); strlwr(pl_name);
char tmp_plname[64];
for (planetary_system::PlanetList::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
strcpy(tmp_plname,(*it)->name()); strlwr(tmp_plname);
if (!strcmp(tmp_plname, pl_name)) {l->setPlanet(*it); break;}}
if (!l->getPlanet()) l->on_ground=0;
else{
const YAML::Node& geopos = node[geo_pos];
geopos[latitude]>>x; l->gp.setLatitude(x);
geopos[longitude]>>x; l->gp.setLongutude(x);
geopos[height]>>x; l->gp.setHeight(x);}
delete[] pl_name;}}

const YAML::Node &sp_pos = node[space_pos];
l->space_pos.setType(coordinates::type::ecliptic);
sp_pos[longitude] >> x; l->space_pos.Alpha(x);
sp_pos[latitude] >> x; l->space_pos.Sigma(x);
sp_pos[range] >> x; l->space_pos.Range(x);}
else{
l->on_ground=0;
l->space_pos=coordinates::Coordinate(200,-20,3.5,coordinates::type::ecliptic);}} catch(...) {f.close(); return l;}
return l;}
}

/*********************/
namespace graphic{
void save_gl_info(const char *file_name){
//�������� ����� ������������� OpenGL
FILE* f=fopen(file_name,"a"); if (f==NULL) return;
const time_t timer = time(NULL);
char *time_s = ctime(&timer);
fprintf(f, time_s);
delete[] time_s;
fprintf(f,"\nOpenGl info\n");
//char *x= (char*)glGetString(GL_EXTENSIONS);
//x+=2;
fprintf(f,"GL_VENDOR %s\n",(char*)glGetString(GL_VENDOR));
fprintf(f,"GL_RENDERER %s\n",(char*)glGetString(GL_RENDERER));
fprintf(f,"GL_VERSION %s\n",(char*)glGetString(GL_VERSION));
fprintf(f,"GL_EXTENSIONS %s\n\n",(char*)glGetString(GL_EXTENSIONS));
fclose(f);}

//return's map [texture_name, file_name]
std::map<std::string,char*> texture_names_old(const char* filename){
std::map<std::string,char*> T;
FILE* texture_f=fopen(filename,"r");
if (texture_f==NULL) return T;
char buff[512];
while (!feof(texture_f)){
fgets(buff,512,texture_f);
straux::trim::after_char(buff,'#');
char *tmp=strchr(buff,':');
 if (tmp==NULL) continue;
tmp[0]=0;tmp++;
straux::trim::beginend(tmp,' ');
straux::trim::beginend(tmp,'\t');
if (tmp[strlen(tmp)-1]=='\n') tmp[strlen(tmp)-1]=0;
strlwr(tmp);
T[tmp]=strdup(buff);
}
fclose(texture_f);
return T;}

Texture_File texture_names(const char* filename){
Texture_File tf;
std::ifstream texdeffile(filename); // texture definition file
if (!texdeffile.is_open()) return tf;
YAML::Parser parser(texdeffile);
YAML::Node doc;
try{parser.GetNextDocument(doc);}
catch(YAML::Exception &e){QMessageBox::about(NULL,
                "YAML: Error parsing texture definition file",e.what());}

for (YAML::Iterator it = doc.begin(), end=doc.end(); it!=end; it++){
std::string file, texture; // filename and texturename
const YAML::Node & key   = it.first(); key >> texture;
const YAML::Node & value = it.second(); value >> file;

char *cwhat=strdup(texture.c_str()); // std::string �����, � ��� tolower ���� �� ��������
texture = strlwr(cwhat); delete[] cwhat;
tf[texture]= file;}
texdeffile.close();
return tf;}
}
namespace screen_params{
namespace param{
/*��������� �������, ������� ��� ���*/
const char* selected = "selected";
const char* normal =   "normal";
const char* color =          "color";
const char* text =           "text";
const char* orbit =          "orbit";
const char* deep_space =     "ds";
const char* planet_system =  "ps";
const char* screen =         "screen";
const char* grid =           "grid";
const char* ecliptic_disc =  "ecliptic_disc";
const char* constellations = "constellation";
}
int graphic_elements(screen::plan_elements& p, const char* file){
std::ifstream cat_file(file);
if (cat_file.is_open()){
YAML::Parser parser(cat_file);
YAML::Node node;
try{parser.GetNextDocument(node);} catch(YAML::Exception &e) { return 0;}
const YAML::Node &orb = node[param::orbit];
deserialization::record::getColWidth(p.s_orbit,orb[param::selected]);
deserialization::record::getColWidth(p.orbit,orb[param::normal]);
const YAML::Node &text = node[param::text];//----------------------------------------- text
deserialization::record::getColWidth(p.text.ds,text[param::deep_space]);
deserialization::record::getColWidth(p.text.ps,text[param::planet_system]);
deserialization::record::getColWidth(p.text.constellation, text[param::constellations]);
deserialization::record::getColWidth(p.text.screen,text[param::screen]);
deserialization::record::getColWidth(p.text.grid,text[param::screen]);

deserialization::record::getColWidth(p.ecliptic_disc,node[param::ecliptic_disc]);
const YAML::Node& grid = node["grid"];
deserialization::record::getColWidth(p.grid.equatorial,grid["equatorial"]);
deserialization::record::getColWidth(p.grid.ecliptic,grid["ecliptic"]);
deserialization::record::getColWidth(p.grid.azimuthal,grid["azimuthal"]);
deserialization::record::getColWidth(p.orbit_features,node["orbit_features"]);
const YAML::Node &sel_mark = node["sel_mark"];
const YAML::Node &ds = sel_mark[param::deep_space];
const YAML::Node &ss = sel_mark[param::planet_system];
const YAML::Node& col = ds[param::color];
p.ds_sel_mark_color = new float[4];
for (unsigned short i=0; i<4; i++)col[i]>>p.ds_sel_mark_color[i];
p.ps_sel_mark_color = new float[4];
const YAML::Node& col2 = ss[param::color];
for (unsigned short i=0; i<4; i++)col2[i]>>p.ps_sel_mark_color[i];
return 1;}return 0;}

}
// get optical systems
std::vector<optical_system> get_optems(const YAML::Node& node){
using namespace config::param;
std::vector<optical_system> optems;
optical_system os;
for (unsigned i=0; i<node.size(); i++){
const YAML::Node& n = node[i];
n[telescope::f] >> os.f;
n[telescope::d] >> os.d;
optems.push_back(os);}
return optems;}

telescope_etc telescope(const char* filename){
using namespace config::param;
telescope_etc tetc;
std::ifstream file(filename, std::ios::in);
YAML::Node node;
YAML::Parser p(file);
tetc.o_index=0;tetc.t_index=0;
try{p.GetNextDocument(node);
if (node.FindValue(config::param::telescope::telescopes))tetc.t=get_optems(node[telescope::telescopes]);
if (node.FindValue(config::param::telescope::yey_pieces))tetc.y=get_optems(node[telescope::yey_pieces]);
}catch (YAML::Exception& e){file.close(); to_file(file::log::main,"[error] loading telescopes"); return tetc;}

return tetc;}
} /*initialization*/
