﻿/********************************************************************************
** Form generated from reading UI file 'perception_settings.ui'
**
** Created: Tue 24. May 19:46:01 2011
**      by: Qt User Interface Compiler version 4.7.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PERCEPTION_SETTINGS_H
#define UI_PERCEPTION_SETTINGS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSlider>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_setting
{
public:
    QGridLayout *gridLayout_2;
    QGroupBox *All;
    QGridLayout *gridLayout;
    QLabel *label_11;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_12;
    QLabel *label_6;
    QLabel *label;
    QCheckBox *star;
    QCheckBox *star_text;
    QSlider *horizontalSlider;
    QLabel *label_5;
    QCheckBox *asterizm;
    QCheckBox *asterizm_text;
    QSlider *horizontalSlider_2;
    QLabel *label_8;
    QCheckBox *odk;
    QCheckBox *odk_text;
    QSlider *horizontalSlider_3;
    QLabel *label_9;
    QCheckBox *comete;
    QCheckBox *comete_text;
    QCheckBox *comete_orbit;
    QSlider *horizontalSlider_4;
    QLabel *label_7;
    QCheckBox *nsat;
    QCheckBox *nsat_text;
    QCheckBox *nsat_orbit;
    QSlider *horizontalSlider_5;
    QLabel *label_4;
    QCheckBox *planet;
    QCheckBox *planet_text;
    QCheckBox *planet_orbit;
    QSlider *horizontalSlider_6;
    QLabel *label_10;
    QCheckBox *all;
    QCheckBox *all_text;
    QCheckBox *all_orbit;
    QSlider *horizontalSlider_7;
    QDialogButtonBox *buttonBox;
    QGroupBox *groupBox;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QCheckBox *eqv;
    QCheckBox *ecl;
    QCheckBox *az;
    QCheckBox *ecla;

    void setupUi(QDialog *setting)
    {
        if (setting->objectName().isEmpty())
            setting->setObjectName(QString::fromUtf8("setting"));
        setting->resize(549, 270);
        setting->setWindowOpacity(8);
        gridLayout_2 = new QGridLayout(setting);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        All = new QGroupBox(setting);
        All->setObjectName(QString::fromUtf8("All"));
        gridLayout = new QGridLayout(All);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_11 = new QLabel(All);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 0, 0, 1, 1);

        label_2 = new QLabel(All);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 1, 1, 1);

        label_3 = new QLabel(All);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 0, 2, 1, 1);

        label_12 = new QLabel(All);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 0, 3, 1, 1);

        label_6 = new QLabel(All);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 0, 4, 1, 1);

        label = new QLabel(All);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        star = new QCheckBox(All);
        star->setObjectName(QString::fromUtf8("star"));

        gridLayout->addWidget(star, 1, 1, 1, 1);

        star_text = new QCheckBox(All);
        star_text->setObjectName(QString::fromUtf8("star_text"));

        gridLayout->addWidget(star_text, 1, 2, 1, 1);

        horizontalSlider = new QSlider(All);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider, 1, 4, 1, 1);

        label_5 = new QLabel(All);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 2, 0, 1, 1);

        asterizm = new QCheckBox(All);
        asterizm->setObjectName(QString::fromUtf8("asterizm"));

        gridLayout->addWidget(asterizm, 2, 1, 1, 1);

        asterizm_text = new QCheckBox(All);
        asterizm_text->setObjectName(QString::fromUtf8("asterizm_text"));

        gridLayout->addWidget(asterizm_text, 2, 2, 1, 1);

        horizontalSlider_2 = new QSlider(All);
        horizontalSlider_2->setObjectName(QString::fromUtf8("horizontalSlider_2"));
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_2, 2, 4, 1, 1);

        label_8 = new QLabel(All);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 3, 0, 1, 1);

        odk = new QCheckBox(All);
        odk->setObjectName(QString::fromUtf8("odk"));

        gridLayout->addWidget(odk, 3, 1, 1, 1);

        odk_text = new QCheckBox(All);
        odk_text->setObjectName(QString::fromUtf8("odk_text"));

        gridLayout->addWidget(odk_text, 3, 2, 1, 1);

        horizontalSlider_3 = new QSlider(All);
        horizontalSlider_3->setObjectName(QString::fromUtf8("horizontalSlider_3"));
        horizontalSlider_3->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_3, 3, 4, 1, 1);

        label_9 = new QLabel(All);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout->addWidget(label_9, 4, 0, 1, 1);

        comete = new QCheckBox(All);
        comete->setObjectName(QString::fromUtf8("comete"));

        gridLayout->addWidget(comete, 4, 1, 1, 1);

        comete_text = new QCheckBox(All);
        comete_text->setObjectName(QString::fromUtf8("comete_text"));

        gridLayout->addWidget(comete_text, 4, 2, 1, 1);

        comete_orbit = new QCheckBox(All);
        comete_orbit->setObjectName(QString::fromUtf8("comete_orbit"));

        gridLayout->addWidget(comete_orbit, 4, 3, 1, 1);

        horizontalSlider_4 = new QSlider(All);
        horizontalSlider_4->setObjectName(QString::fromUtf8("horizontalSlider_4"));
        horizontalSlider_4->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_4, 4, 4, 1, 1);

        label_7 = new QLabel(All);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 5, 0, 1, 1);

        nsat = new QCheckBox(All);
        nsat->setObjectName(QString::fromUtf8("nsat"));

        gridLayout->addWidget(nsat, 5, 1, 1, 1);

        nsat_text = new QCheckBox(All);
        nsat_text->setObjectName(QString::fromUtf8("nsat_text"));

        gridLayout->addWidget(nsat_text, 5, 2, 1, 1);

        nsat_orbit = new QCheckBox(All);
        nsat_orbit->setObjectName(QString::fromUtf8("nsat_orbit"));

        gridLayout->addWidget(nsat_orbit, 5, 3, 1, 1);

        horizontalSlider_5 = new QSlider(All);
        horizontalSlider_5->setObjectName(QString::fromUtf8("horizontalSlider_5"));
        horizontalSlider_5->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_5, 5, 4, 1, 1);

        label_4 = new QLabel(All);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 6, 0, 1, 1);

        planet = new QCheckBox(All);
        planet->setObjectName(QString::fromUtf8("planet"));

        gridLayout->addWidget(planet, 6, 1, 1, 1);

        planet_text = new QCheckBox(All);
        planet_text->setObjectName(QString::fromUtf8("planet_text"));

        gridLayout->addWidget(planet_text, 6, 2, 1, 1);

        planet_orbit = new QCheckBox(All);
        planet_orbit->setObjectName(QString::fromUtf8("planet_orbit"));

        gridLayout->addWidget(planet_orbit, 6, 3, 1, 1);

        horizontalSlider_6 = new QSlider(All);
        horizontalSlider_6->setObjectName(QString::fromUtf8("horizontalSlider_6"));
        horizontalSlider_6->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_6, 6, 4, 1, 1);

        label_10 = new QLabel(All);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 7, 0, 1, 1);

        all = new QCheckBox(All);
        all->setObjectName(QString::fromUtf8("all"));

        gridLayout->addWidget(all, 7, 1, 1, 1);

        all_text = new QCheckBox(All);
        all_text->setObjectName(QString::fromUtf8("all_text"));

        gridLayout->addWidget(all_text, 7, 2, 1, 1);

        all_orbit = new QCheckBox(All);
        all_orbit->setObjectName(QString::fromUtf8("all_orbit"));

        gridLayout->addWidget(all_orbit, 7, 3, 1, 1);

        horizontalSlider_7 = new QSlider(All);
        horizontalSlider_7->setObjectName(QString::fromUtf8("horizontalSlider_7"));
        horizontalSlider_7->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_7, 7, 4, 1, 1);


        gridLayout_2->addWidget(All, 0, 0, 1, 1);

        buttonBox = new QDialogButtonBox(setting);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Apply|QDialogButtonBox::Cancel|QDialogButtonBox::RestoreDefaults|QDialogButtonBox::Save);

        gridLayout_2->addWidget(buttonBox, 1, 0, 1, 1);

        groupBox = new QGroupBox(setting);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        widget = new QWidget(groupBox);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(10, 54, 110, 88));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        eqv = new QCheckBox(widget);
        eqv->setObjectName(QString::fromUtf8("eqv"));

        verticalLayout->addWidget(eqv);

        ecl = new QCheckBox(widget);
        ecl->setObjectName(QString::fromUtf8("ecl"));

        verticalLayout->addWidget(ecl);

        az = new QCheckBox(widget);
        az->setObjectName(QString::fromUtf8("az"));

        verticalLayout->addWidget(az);

        ecla = new QCheckBox(widget);
        ecla->setObjectName(QString::fromUtf8("ecla"));

        verticalLayout->addWidget(ecla);


        gridLayout_2->addWidget(groupBox, 0, 1, 1, 1);


        retranslateUi(setting);
        QObject::connect(buttonBox, SIGNAL(accepted()), setting, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), setting, SLOT(reject()));

        QMetaObject::connectSlotsByName(setting);
    } // setupUi

    void retranslateUi(QDialog *setting)
    {
        setting->setWindowTitle(QApplication::translate("setting", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", 0, QApplication::UnicodeUTF8));
        All->setTitle(QApplication::translate("setting", "\320\236\321\202\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\320\265 \320\276\320\261\321\212\320\265\320\272\321\202\320\276\320\262 \320\270 \320\277\320\276\320\264\320\277\320\270\321\201\320\265\320\271, \320\276\321\200\320\261\320\270\321\202", 0, QApplication::UnicodeUTF8));
        label_11->setText(QString());
        label_2->setText(QApplication::translate("setting", "\320\276\320\261\321\212\320\265\320\272\321\202", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("setting", "\320\277\320\276\320\264\320\277\320\270\321\201\321\214", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("setting", "\320\276\321\200\320\261\320\270\321\202\321\213", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("setting", "\320\234\320\260\320\272\321\201\320\270\320\274\320\260\320\273\321\214\320\275\320\260\321\217 \320\262\320\270\320\264\320\270\320\274\320\260\321\217 \320\262\320\265\320\273\320\270\321\207\320\270\320\275\320\260", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("setting", "\320\227\320\262\320\265\320\267\320\264\321\213", 0, QApplication::UnicodeUTF8));
        star->setText(QString());
        star_text->setText(QString());
        label_5->setText(QApplication::translate("setting", "\320\220\321\201\321\202\320\265\321\200\320\270\320\267\320\274\321\213", 0, QApplication::UnicodeUTF8));
        asterizm->setText(QString());
        asterizm_text->setText(QString());
        label_8->setText(QApplication::translate("setting", "\320\236\320\224\320\232", 0, QApplication::UnicodeUTF8));
        odk->setText(QString());
        odk_text->setText(QString());
        label_9->setText(QApplication::translate("setting", "\320\232\320\276\320\274\320\265\321\202\321\213", 0, QApplication::UnicodeUTF8));
        comete->setText(QString());
        comete_text->setText(QString());
        comete_orbit->setText(QString());
        label_7->setText(QApplication::translate("setting", "\320\225\321\201.\320\241\320\277\321\203\321\202\320\275\320\270\320\272\320\270", 0, QApplication::UnicodeUTF8));
        nsat->setText(QString());
        nsat_text->setText(QString());
        nsat_orbit->setText(QString());
        label_4->setText(QApplication::translate("setting", "\320\237\320\273\320\260\320\275\320\265\321\202\321\213", 0, QApplication::UnicodeUTF8));
        planet->setText(QString());
        planet_text->setText(QString());
        planet_orbit->setText(QString());
        label_10->setText(QApplication::translate("setting", "\320\262\321\201\320\265", 0, QApplication::UnicodeUTF8));
        all->setText(QString());
        all_text->setText(QString());
        all_orbit->setText(QString());
        groupBox->setTitle(QApplication::translate("setting", "\320\272\320\276\320\276\321\200\320\264\320\270\320\275\320\260\321\202\320\275\320\260\321\217 \321\201\320\265\321\202\320\272\320\260", 0, QApplication::UnicodeUTF8));
        eqv->setText(QApplication::translate("setting", "\320\255\320\272\320\262\320\260\321\202\320\276\321\200\320\270\320\260\320\273\321\214\320\275\320\260\321\217", 0, QApplication::UnicodeUTF8));
        ecl->setText(QApplication::translate("setting", "\320\255\320\272\320\273\320\270\320\277\321\202\320\270\321\207\320\265\321\201\320\272\320\260\321\217", 0, QApplication::UnicodeUTF8));
        az->setText(QApplication::translate("setting", "\320\220\320\267\320\270\320\274\321\203\321\202\320\260\320\273\321\214\320\275\320\260\321\217", 0, QApplication::UnicodeUTF8));
        ecla->setText(QApplication::translate("setting", "\321\215\320\272\320\273\320\270\320\277\321\202\320\270\320\272\320\260", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class setting: public Ui_setting {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PERCEPTION_SETTINGS_H
