﻿#include "op_info.h"
namespace op_info{
namespace order{
 std::string context[] = {info::param::name,
                             info::param::type,
                             info::param::ids,
                             /*coordinates*/
                             info::param::coord::ra,
                            info::param::coord::declination,
                            info::param::coord::ecl_long,
                            info::param::coord::ecl_lat,
                            info::param::coord::range,
                            /**/
                            info::param::distance,
                            info::param::arc_daimetr,
                            /*self params*/
                            info::param::tp,
                            info::param::rotation_period,
                            info::param::polar_radius,
                            info::param::spec_class,
                            info::param::vmag,
                            /*other*/
                            info::param::note,
                            ""};
 std::string time[] = {info::param::tempus::time,
                            info::param::tempus::timespeed,
                            info::param::tempus::is_stopped,""};
 std::string observer[] = {info::param::observer::fov,
                           info::param::observer::pk,
                           /*for telescope*/
                           info::param::telescope::telescope_name,
                           info::param::telescope::aperture_d,
                           info::param::telescope::t_focal_length,
                           info::param::telescope::yey_piece_name,
                           info::param::telescope::out_fov,

                           info::param::telescope::yey_piece_name,
                           ""};
 std::string location[] = {info::param::name,
                                info::param::geo_pos::latitude,
                                info::param::geo_pos::longitude,
                                info::param::geo_pos::height,
                                info::param::coord::ecl_long,
                                info::param::coord::ecl_lat,
                            info::param::coord::range,""};
const std::string *all_things[] = {context, time};}

void init_titles(TitleSPm& titles){
/* <0 - don't show title, >0 - show title*/
titles[info::param::name] = - completeness::min;

titles[info::param::coord::declination] = completeness::normal;
titles[info::param::coord::ra] = completeness::normal;
titles[info::param::coord::ecl_lat] = completeness::normal;
titles[info::param::coord::ecl_long] = completeness::normal;
titles[info::param::coord::range] = completeness::max;

titles[info::param::distance] = completeness::normal;
titles[info::param::arc_daimetr] = completeness::normal;

titles[info::param::vmag] = completeness::normal;
titles[info::param::abvr] = completeness::normal;
titles[info::param::ids] = -completeness::normal;

titles[info::param::polar_radius] = completeness::max;
titles[info::param::rotation_period] = completeness::max;
titles[info::param::tp] = completeness::max;
titles[info::param::spec_class] = completeness::max;
titles[info::param::note] = completeness::max;

titles[info::param::tempus::time] = -completeness::min;
titles[info::param::tempus::timespeed] = completeness::normal;
titles[info::param::tempus::is_stopped] = completeness::normal;

titles[info::param::observer::fov] = completeness::normal;
titles[info::param::observer::pk] = completeness::normal;
//titles[param::observation::maxVmag] = completeness::max;

titles[info::param::telescope::aperture_d] = completeness::min;
titles[info::param::telescope::t_focal_length] = completeness::min;
titles[info::param::telescope::out_fov] = completeness::min;
titles[info::param::telescope::telescope_name] = -completeness::min;
titles[info::param::telescope::yey_piece_name] = -completeness::min;

}
void setObservationInfo(info::ParamValues& observing, const observer::Observer *o){
char t[8];
sprintf(t,"%5.2f",o->eye.getFoV());
observing[info::param::observer::fov] = t;
sprintf(t,"%5.2f",o->perception.how_i_see);
observing[info::param::observer::pk] = t;}
//---------------------------------
void setTimeInfo(info::ParamValues& t,const tempus::VivusTempus &time){
t[info::param::tempus::time] = time.Time.toString("yyyy.mm.dd hh:mm:ss").toStdString();
t[info::param::tempus::timespeed] = QString::number(time.X()).toStdString();
if (!time.is_stort()) t[info::param::tempus::is_stopped] = "stopped";
else t[info::param::tempus::is_stopped] = "";}
//----------------------------------
void setLocationInfo(info::ParamValues& loc, const location::Location *l){
loc.clear();
loc[info::param::name] = std::string(l->getPlanet()->name());
if (l->on_ground){
loc[info::param::geo_pos::height] = info::param::geo_pos::height+ ": " +QString::number(l->gp.Height()).toStdString();
loc[info::param::geo_pos::latitude] = info::param::geo_pos::latitude+ ": " + QString::number(l->gp.Latitude()).toStdString();
loc[info::param::geo_pos::longitude] = info::param::geo_pos::longitude + ": " + QString::number(l->gp.Longitude()).toStdString();}}
//--------------------------
void setObjectObservationInfo(info::ParamValues& obj, const elementus_mundus::BasicAstro* ba, const location::Location* l){
using namespace coordinates::converting;
if (ba){ obj = ba->getInfo();
double distance = location::distance(*l,ba);
double arc_d = location::arc_diametr(distance,ba);
obj[info::param::distance] = QString::number(distance).toStdString() + " " +ba->coordinate.geRangeUnits();
obj[info::param::arc_daimetr] = QString::number(arc_d).toStdString();
}else obj.clear();}


/*Observation planetarium info*/
OPInfo::OPInfo(){
op_info::init_titles(titles_show_params);
this->completeness=completeness::normal;
this->order=new std::string*[4];
this->order[informations::context] = order::context;
this->order[informations::location] = order::location;
this->order[informations::observer] = order::observer;
this->order[informations::time] = order::time;}

OPInfo::~OPInfo(){}

void OPInfo::init_titles(){
info::ParamValues &time = this->info[informations::time];
unsigned i=0;
while (order::time[i].size()) time[order::time[i++]];
info::ParamValues observer = this->info[informations::observer];
i=0;while (order::observer[i].size()) observer[order::observer[i++]];
info::ParamValues loc = this->info[informations::location];
i=0;while (order::location[i].size()) loc[order::location[i++]];}

void OPInfo::update(const E2 &e2){
setTimeInfo(this->info[informations::time], e2.umodel->lifeTime);
setObservationInfo(this->info[informations::observer], e2.Galileo_Galilei);
setObjectObservationInfo(this->info[informations::context],e2.adb->selected,e2.location);
setLocationInfo(this->info[informations::location], e2.location);}
//----------------------------
void OPInfo::setCompleteness(char c){
completeness=c;}

char OPInfo::getCompletness()const{
return this->completeness;}

void OPInfo::inc_scr_info(){
  if (this->completeness<completeness::max) this->completeness++;}

void OPInfo::dec_scr_info(){
if (this->completeness>completeness::min) this->completeness--;}

void OPInfo::set_telescope(const optical_system &t, const optical_system &o){
//info::ParamValues &observation = info[informations::observer];
//if (strlen(t.name)) observation[info::param::telescope::telescope_name] = t.name;
//else observation[info::param::telescope::telescope_name] = info::param::telescope::telescope_name;

//observation[info::param::telescope::telescope_name] = t.name;
//observation[info::param::telescope::telescope_name] = t.name;
}
}
