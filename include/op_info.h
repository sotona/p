﻿/*
Observation Planetarium Info
ionformation about
• Observer
• Location
• Time
• Selected AstroObject
*/

#ifndef OP_INFO_H
#define OP_INFO_H
#include <map>
#include <string>
#include <QString>
#include <QObject>
#include "e2/telescope.h"
#include "e2/e/elementus_mundus.h"
#include "e2/e2.h"

namespace op_info{

namespace order{
extern  std::string context[];
extern  std::string observer[];
extern  std::string time[];
extern  std::string location[];}

using info::ParamValues;
typedef std::map<std::string,signed char> TitleSPm; // title-show_params map
struct param_value{
param_value(std::string t, QString v): title(t), value(v){}
param_value(){title="",value="";}
std::string title;
QString value;};
//typedef std::map<std::string, QString> ValAmm; //[Val]uePar[am][v]ecctor


/*режимы полноты выводимой информации*/
namespace completeness{
const char max = 3;
const char normal = 2;
const char min = 1;             /*показывать поля с этим приоритетом всегда*/}

/*инициализация параметров важности и рисования названий параметров*/
void init_titles_completeness(TitleSPm& titles);

/*индексы для массива info класса OPinfo*/
namespace informations{
const unsigned char time =0;
const unsigned char observer =1;        // информация о наблюдении (fov, maxVmag, ...)
const unsigned char location = 2;
const unsigned char context = 3;         // контекстная информация
}

class OPInfo:public QObject{
Q_OBJECT
public:
    info::ParamValues info[4];
    char completeness; // use max(0), normal(1) or min(3) info;
    TitleSPm titles_show_params;
    std::string **order;        // dont delete this array
public:
    OPInfo();
    ~OPInfo();
    void setCompleteness(char m);
    void init_titles();                             //  инициилизирует titles массива info

    void update(const E2& e2);                                      // обновляет каждый-кадр-меняющиеся поля
    void set_telescope(const optical_system& t,const optical_system& o);
    /*get*/
//    const & getInfo() const;
//    const ValAmm& getInfo(char what) const;
    char getCompletness() const;
    unsigned getVisibleFieldsFor(char what) const;                        // получить количество выводимых полей (complitness<=current)
    public slots:
        void inc_scr_info();
        void dec_scr_info();

};
}



#endif // OP_INFO_H
