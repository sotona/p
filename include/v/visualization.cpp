﻿#include "visualization.h"
#include <stdio.h>
#include <qgl.h>
#include <GL/glu.h>
#include <QMessageBox>

PFNGLWINDOWPOS2IARBPROC glWindowPos2iARB;
typedef void(*extFun)(GLint,GLint);

static void  * getProcAddress ( const char * name )
{
#ifdef	_WIN32
    return (void *)wglGetProcAddress ( name );
#else
    return (void *)glXGetProcAddressARB ( (const GLubyte *)name );
#endif
}

bool initExtensions(){
int r = ((glWindowPos2iARB = (PFNGLWINDOWPOS2IARBPROC)getProcAddress("glWindowPos2iARB"))!=NULL);
return r;}

// какое-то неточное название
namespace visualization{
/*дополнительные вещи*/
namespace aux{
//--------------
bool get2Dcoordinate(GLdouble* xyz2d,const GLdouble* xyz3d){
GLint viewport[4];
GLdouble mvmatrix[16], projmatrix[16];
glGetIntegerv(GL_VIEWPORT,viewport);
glGetDoublev(GL_MODELVIEW_MATRIX,mvmatrix);
glGetDoublev(GL_PROJECTION_MATRIX,projmatrix);
bool r = gluProject (xyz3d[0], xyz3d[1], xyz3d[2], mvmatrix, projmatrix, viewport, &xyz2d[0], &xyz2d[1], &xyz2d[2]);
//xyz2d[1]=viewport[3]-xyz2d[1];
return r;}
//----------------
bool get2Dcoordinate(GLdouble &x, GLint &y,const GLdouble* xyz3d){
GLdouble xy_z[3];
bool r= get2Dcoordinate(xy_z,xyz3d);
x=xy_z[0]; y=xy_z[1];
return r;}
//---------------
double getZ(double *xyz){
GLint viewport[4];
GLdouble mvmatrix[16], projmatrix[16];
glGetIntegerv(GL_VIEWPORT,viewport);
glGetDoublev(GL_MODELVIEW_MATRIX,mvmatrix);
glGetDoublev(GL_PROJECTION_MATRIX,projmatrix);
double *tmp_xyz = new double[4];
memset(tmp_xyz,0,4*sizeof(double));
for (unsigned char i=0; i<4; i++){
for (unsigned char j=0; j<4; j++)
tmp_xyz[i]+=mvmatrix[j*4+i]*xyz[j];}
memset(xyz, 0, 4*sizeof(double));
for (unsigned char i=0; i<4; i++){
for (unsigned char j=0; j<4; j++)
xyz[i]+=projmatrix[j*4+i]*tmp_xyz[j];}
delete[] tmp_xyz;
return xyz[2];}
//-------------- is this point visible (2d_x+dx and 3d_y+dy)
bool is_visible(GLint* viewport, const double* xyz3d, int dx, int dy, bool check_z){
GLdouble xyz_2d[3];
get2Dcoordinate(xyz_2d,xyz3d);
bool z=1;
if (check_z) z = xyz_2d[2] < 1.0;
if ( z &&
    viewport[0] <= xyz_2d[0]+dx && viewport[2] >= xyz_2d[0]+dx
&&  viewport[1] <= xyz_2d[1]+dy && viewport[3] >= xyz_2d[1]+dy) return 1;
else return 0;}
//------------
bool is_visible(GLint *viewport, const double *xyz3d, double *xyz_2d, int dx, int dy, bool check_z){
get2Dcoordinate(xyz_2d,xyz3d);
bool z=1;
if (check_z) z = xyz_2d[2] < 1.0;
if ( z &&
    viewport[0] <= xyz_2d[0]+dx && viewport[2] >= xyz_2d[0]+dx
&&  viewport[1] <= xyz_2d[1]+dy && viewport[3] >= xyz_2d[1]+dy) return 1;
else return 0;
}
}

/* базовые функции рисования  */
namespace elementus{

void quasiGround(){
glColor3f(0,0,0);
glBegin(GL_TRIANGLE_FAN);
glVertex3f(0,-coordinates::very_far,0);
for (float i=0; i<M_PI*2; i+=M_PI/18) glVertex3f(coordinates::very_far*sin(i),0,coordinates::very_far*cos(i));
glEnd();

glColor3f(0.5,0.5,0.5);
glBegin(GL_LINE_LOOP);
glVertex3f(0,-coordinates::very_far,0);
for (float i=0; i<M_PI*2; i+=M_PI/18) glVertex3f(coordinates::very_far*sin(i),0,coordinates::very_far*cos(i));
glEnd();
}

//-------------
void markers(const profundus_mundus::CaelestisList& cl){
using profundus_mundus::CaelestisList;
using elementus_mundus::altus_mundus::Caelestis;
for (CaelestisList::const_iterator it = cl.begin(), end = cl.end(); it!=end; it++){
Caelestis *c = *it;
elementus::marker(c->coordinate);}}
//------------------------------------------------
void marker(const coordinates::Coordinate &c){
double xyz[3];
coordinates::converting::SpherToCartesian(c,xyz);
glColor3f(0,1,0);
glPointSize(5.0);
glBegin(GL_POINTS);
glVertex3dv(xyz);
glEnd();}

void text(FTPixmapFont &font,double coord[], char *text){
if (!strlen(text)) return;
GLint viewport[4]; glGetIntegerv(GL_VIEWPORT,viewport); // каждый раз ?
    GLdouble coord_2d[3];
    if (visualization::aux::get2Dcoordinate(coord_2d, coord)){
    if (coord_2d[2]<=1.)
    if (coord_2d[0] >= 0-50 && coord_2d[0] <= viewport[2] && coord_2d[1] >=0 && coord_2d[1] <=viewport[3])    {
    font.Render(text, strlen(text),FTPoint(coord_2d[0],coord_2d[1]));}}
}

void Sphere(float r /*float pr, float corner,float color,float style*/){
 GLUquadricObj *quadObj;
 quadObj = gluNewQuadric();
  gluQuadricDrawStyle(quadObj, GLU_FILL);
  gluSphere(quadObj, r, 100, 100);
gluDeleteQuadric(quadObj);}
//---------------------
void vert_array(const g_containers::Vert_array &va, GLenum mode){
glEnableClientState(GL_VERTEX_ARRAY);
glVertexPointer(3,GL_DOUBLE, 0, va.array);
glDrawArrays(mode,0,va.length);
glDisableClientState(GL_VERTEX_ARRAY);}
//-------------
void vert_arrays(const std::list<g_containers::Vert_array *>& vas, GLenum mode){
for (std::list<g_containers::Vert_array *>::const_iterator it=vas.begin(), end=vas.end(); it!=end; it++){
elementus::vert_array(*(*it),mode);}}
//--------------
void vert_arrays(const std::vector<g_containers::Vert_array *>& vas, GLenum mode){
for (std::vector<g_containers::Vert_array *>::const_iterator it=vas.begin(), end=vas.end(); it!=end; it++){
elementus::vert_array(*(*it),mode);}}
//---------------------
void line(const coordinates::CoordList& cl){
//массивы вершин
if (cl.size()==0) return;
glBegin(GL_LINE_STRIP);double xyz[3];
    for (coordinates::CoordList::const_iterator itt = cl.begin(), endd = cl.end(); itt!=endd; itt++){
    coordinates::Coordinate *a = *itt;
    if (a==NULL) break;
    coordinates::converting::SpherToCartesian(*a,xyz);
    glVertex3dv(xyz);    }
glEnd();}
//---------------
void line(const profundus_mundus::AstraList& al){
//массивы вершин
if (al.size()==0) return;
glBegin(GL_LINE_STRIP);
    const elementus_mundus::altus_mundus::Astrum* a;double xyz[3];
    for (profundus_mundus::AstraList::const_iterator itt = al.begin(), endd = al.end(); itt!=endd; itt++){
    a = *itt;   coordinates::converting::SpherToCartesian(a->coordinate,xyz);
    glVertex3dv(xyz);  }
glEnd();}
//--------------------
void caelestis(const elementus_mundus::altus_mundus::Caelestis& c){
visualization::universe::getSizeByVisualMag(c.vMag());
visualization::elementus::marker(c.coordinate);}
//---------
void caelestis_list(const profundus_mundus::CaelestisList &cl){
}

template <class AL>
unsigned otbiter_points(const AL& pl, elementus_mundus::ID_Count_type except){
glEnable(GL_POINT_SMOOTH);
double xyz[3];
for (typename AL::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
//p=*it;
if (/*сравнение с предельной видимой величиной*/ 0 || (*it)->absID()==except) continue;
coordinates::Coordinate c=(*it)->coordinate;
coordinates::converting::SpherToCartesian(c,xyz);
//glColor3f(0.9,0.9,0.9);
/* вычисление размера точки, и его цвета */
glPointSize(3);;
glBegin(GL_POINTS);glVertex3dv(xyz);glEnd();}
glDisable(GL_POINT_SMOOTH);
return 0;}
} /*end of elementus*/

/* рисования вспомогательных элементов */
namespace imaginary_draw{
using namespace observer::perceptionem;
// маркер. четвертовать сделать и красивым.
unsigned select_marker[] = {
0x80000001,0xC0000003,
0xE0000007,0xF000000F,
0xF800001F,0xFC00003F,
0xFE00007F,0xFF0000FF,
0xFF0000FF,0x00000000,
0x00000000,0x00000000,
0x00000000,0x00000000,
0x00000000,0x00000000,
0x00000000,0x00000000,
0x00000000,0x00000000,
0x00000000,0x00000000,
0x00000000,
0xFF0000FF,0xFF0000FF,
0xFE00007F,0xFC00003F,
0xF800001F,0xF000000F,
0xE0000007,0xC0000003,
0x80000001};

//здеь есть что-то лишнее
void equator(float r, unsigned parts){
if (parts==0) parts = 24*2;
glBegin(GL_LINE_LOOP);
for (unsigned i=0; i<parts; i++){
glVertex3f(r*sin(float(i)/parts*M_PI*2),0, r*cos(float(i)/parts*M_PI*2));
}
glEnd();
}

void meridian(float r, unsigned parts){
if (parts==0) parts = 24*2;
glBegin(GL_LINE_LOOP);
for (unsigned i=0; i<parts; i++)
glVertex3f(r*cos(float(i)/parts*M_PI*2), r*sin(float(i)/parts*M_PI*2),0);
glEnd();
}

//оптимизировать
void grid(float r, int parallels, int meridians){
float dp=1./parallels*2;
//float dm=1./meridians*2;
float circle_k=1/16.; //then lower then more circler
float tr;
glEnable(GL_LINE_STIPPLE);      //
glLineStipple(1,170);
glBegin(GL_LINES);
glVertex3f(-1.0, 0, 0);
glVertex3f(1.0, 0, 0);
glVertex3f(0, 0, 1.0);
glVertex3f(0, 0, -1.0);
glEnd();

tr=r;
for (float j=-1.0; j<1.0; j+=dp){
//parallels
glBegin(GL_LINE_STRIP);
for (float i=-1.0; i<=1.0; i+=circle_k){
tr=r*cos(j*M_PI_2);
glVertex3f(tr*sin(M_PI*i), r*sin(j*M_PI_2),tr*cos(M_PI*i));
}glEnd();}
//meridians
glPushMatrix();
for (int j=0; j<meridians; j++){
glBegin(GL_LINE_STRIP);
for (float i=-1.0; i<=1.0; i+=circle_k){
glVertex3f(0,r*sin(i*M_PI),r*cos(i*M_PI));}
glEnd();
glRotatef(360./meridians,0,1.0,0);}
glDisable(GL_LINE_STIPPLE);
glPopMatrix();
}

// сетка с экватороми и нулевым меридианом
void grid(float *color, float width, float radius,unsigned parallels,unsigned meridians){
glLineWidth(width*0.8);
glColor4f(color[0]*.75, color[1]*.75, color[2]*.75, color[3]*.75);
grid(radius, parallels, meridians);
glColor4fv(color);glLineWidth(width);
equator(radius,360);
meridian(radius,360);}

void grid(FTGLPixmapFont &font, float *color, float width, float radius,unsigned parallels,unsigned meridians){
using namespace coordinates;
glLineWidth(width*0.8);
glColor4f(color[0]*.75, color[1]*.75, color[2]*.75, color[3]*.75);
grid(radius, parallels, meridians);
glColor4fv(color);glLineWidth(width);
equator(radius,360);
meridian(radius,360);}

void ground(float r, unsigned parts){
if (parts==0) parts = 24*2;
glBegin(GL_TRIANGLE_FAN);
glVertex2d(0,0);
for (unsigned i=0; i<parts+1; i++)
glVertex3f(r*sin(float(parts-i)/parts*M_PI*2),0, r*cos(float(parts-i)/parts*M_PI*2));
glEnd();}

//обособление астрообъекта (находящегося в плоскости xz).
void draw_selection(float r, float line_width){
glColor3f(0,1,0);
glLineWidth(line_width); 
glBegin(GL_LINE_LOOP);
const float n=24;
for (int i=0 ;i<n; i++)
glVertex3f(r*sin(float(i)/n*2*M_PI),0,r*cos(float(i)/n*2*M_PI));
glEnd();}

void ecliptic_disc(double start_r, double r, unsigned segments, double r_step){
double orr = r*coordinates::astronomical_unit;
glLineWidth(0.5);
r*=coordinates::astronomical_unit;
double step = 2.0*M_PI/segments;
for (double i=0.0; i<M_PI*2; i+=step){
glBegin(GL_LINES);
glVertex3d(sin(i)*r, 0, cos(i)*r);
glVertex3d(sin(i)*start_r, 0, cos(i)*start_r);
glEnd();}
r_step*=coordinates::astronomical_unit;

/*радиус вектор проходящий через т. весеннего равноденствия*/
glLineWidth(2.0);
glBegin(GL_LINES);
glVertex3f(-start_r,0,0);
glVertex3f(-r,0,0);
glEnd();

/*окружноти*/
glLineWidth(1.0);
glBegin(GL_LINE_LOOP);
for (double i=0.0; i<M_PI*2; i+=step){
glVertex3d(start_r*sin(i), 0, start_r*cos(i));
}glEnd();

r=r_step;
for (unsigned j = 0; r<=orr; j++){
glBegin(GL_LINE_LOOP);
for (double i=0.0; i<M_PI*2; i+=step){
glVertex3d(r*sin(i), 0, cos(i)*r);
}r+=r_step*(int)log(j+1)*2;glEnd();}
}

void selected_mark(const elementus_mundus::BasicAstro &ba, FTPixmapFont &font, float* color){
GLint viewport[4]; glGetIntegerv(GL_VIEWPORT,viewport);// каждый раз ?
GLdouble coord_2d[3];
double x_planet[3]; coordinates::converting::SpherToCartesian(ba.coordinate,x_planet);
if (visualization::aux::get2Dcoordinate(coord_2d, x_planet))
if (coord_2d[2]<1.)
if (coord_2d[0] >= 0-50 && coord_2d[0] <= viewport[2] && coord_2d[1] >=0 && coord_2d[1] <=viewport[3]){
glColor3fv(color);
glWindowPos2iARB(coord_2d[0]-16, coord_2d[1]-16);
glBitmap(32,32,0,0, 32,32,(GLubyte*)imaginary_draw::select_marker);
glWindowPos2iARB(0, 0);
}}

//----------
void ss_labels(FTGLPixmapFont &font,const  planetary_system::PlanetList &pl,
                const  electum_necessarius::whatISee &w, const elementus_mundus::ID_Count_type& except, const screen::col_wid& cw){
elementus_mundus::proxima_mundus::Planet *p;
GLint viewport[4]; glGetIntegerv(GL_VIEWPORT,viewport);// каждый раз ?
//glColor3f(1,1,0);
//glClearColor(1,0,0,0);
GLdouble coord_2d[3];
double x_planet[3];
//glPixelTransferf(GL_ALPHA_BIAS, cw.color[3]);
//glColor3fv(cw.color);
glPixelTransferf(GL_RED_BIAS,   cw.color[0]);
glPixelTransferf(GL_BLUE_BIAS,  cw.color[2]);
glPixelTransferf(GL_GREEN_BIAS, cw.color[1]);
for (planetary_system::PlanetList::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
p=*it; if (p->absID()==except) continue;
coordinates::converting::SpherToCartesian(p->coordinate,x_planet);
if (visualization::aux::get2Dcoordinate(coord_2d, x_planet)){
if (coord_2d[2]<1.)
if (coord_2d[0] >= 0-50 && coord_2d[0] <= viewport[2] && coord_2d[1] >=0 && coord_2d[1] <=viewport[3]){
//pos.X(coord_2d[0]); pos.Y(coord_2d[1]);
const char *name = p->name();
coord_2d[1]+=8;
coord_2d[0]-=font.Advance(name)/2; //glColor3fv(cw.color);
font.Render(name, strlen(name),FTPoint(coord_2d[0],coord_2d[1]));}
}}}
//----------- not in use
void ds_labels(FTGLPixmapFont &font,const profundus_mundus::ProfundusMundus& pm,
                                             const observer::perceptionem::Perceptionem& p,  const screen::col_wid& cw){
const profundus_mundus::AstraList &al = pm.getStars();
elementus_mundus::altus_mundus::Astrum *a;
GLint viewport[4]; glGetIntegerv(GL_VIEWPORT,viewport); // каждый раз ?
GLdouble *coord_2d = new GLdouble[3];
glColor3fv(cw.color);
//glPixelTransferf(GL_RED_BIAS,   cw.color[0]);
//glPixelTransferf(GL_BLUE_BIAS,  cw.color[2]);
//glPixelTransferf(GL_GREEN_BIAS, cw.color[1]);
double *xyz = new double[3];
for (profundus_mundus::AstraList::const_iterator it=al.begin(), end=al.end(); it!=end; it++){
a=*it;
if (a->vMag() <= p.how_i_see*p.labels_k){
coordinates::converting::SpherToCartesian(a->coordinate, xyz);
if (visualization::aux::get2Dcoordinate(coord_2d, xyz)){
if (coord_2d[2]<=1.)
if (coord_2d[0] >= 0-50 && coord_2d[0] <= viewport[2] && coord_2d[1] >=0 && coord_2d[1] <=viewport[3]){
const char *name = a->name();glColor3fv(cw.color);
if (name!=NULL) if (strlen(name))
font.Render(name, strlen(name),FTPoint(coord_2d[0],coord_2d[1]));}}}}
delete[] xyz;
delete[] coord_2d;}


void label(FTGLPixmapFont &font,const elementus_mundus::BasicAstro& a, const screen::col_wid& cw,const int* addxy){
GLint viewport[4]; glGetIntegerv(GL_VIEWPORT,viewport); // каждый раз ?
GLdouble coord_2d[3];double xyz[3];
glPixelTransferf(GL_RED_BIAS,   cw.color[0]);
glPixelTransferf(GL_GREEN_BIAS, cw.color[1]);
glPixelTransferf(GL_BLUE_BIAS,  cw.color[2]);
coordinates::converting::SpherToCartesian(a.coordinate, xyz);
if (visualization::aux::get2Dcoordinate(coord_2d, xyz)){
if (addxy!=NULL) coord_2d[1]+=addxy[1]; coord_2d[0]+=addxy[0];
if (coord_2d[2]<=1.)
if (coord_2d[0] >= 0-50 && coord_2d[0] <= viewport[2] && coord_2d[1] >=0 && coord_2d[1] <=viewport[3]){
const char *name = a.name();
if (name!=NULL) if (strlen(name))
font.Render(name, strlen(name),FTPoint(coord_2d[0]-font.Advance(name)/2,coord_2d[1]));}}}


} /*end of imaginary draw */

/*********************/
namespace init{
using namespace elementus_mundus::proxima_mundus;
using namespace planetary_system;
void planet_rotation(const OrbiterBody& p, const location::geo_pos::GeoPos& gp, const tempus::VivusTempus& life_time){
//зы. долбаный медвед со своими часовыми поясами
float ti = life_time.toGMTf() + (gp.TimeZone()-life_time.timezone);
/*было бы здорово без 25.
теперь 25 нету. но всё равно чего-то нехватает. а может и что-нибудь неправильно
нет всё, вроде, правильно, только в году не 365 дней.
•точка равнодействия дрейфует по элептической орбите
•скорость движения планет по орбите неравномерна
ну и где тут РАЗНЫЕ планеты!?*/
ti=ti*15. + gp.Longitude() + 180; // :)
ti+=float(life_time.Time.date().dayOfYear()-80)/365.*360.;
glRotatef(ti,0,1,0);
glRotatef(-gp.Latitude()+12.5,0,0,1);
// похоже в  этом году, вместо 25 будет 12.5


}
//-------------------
void observer_look(coordinates::Coordinate look){
double x_look[3]; coordinates::converting::SpherToCartesian(look,x_look);
gluLookAt(0,0,0, x_look[0], x_look[1], x_look[2],    0,1,0);               //последнию тройку - наблюдателю
}

//-----observer-GeoPosHourAngel-look------ учитывается геогр. положение и часовой угол планеты
void observerGPHAlook(coordinates::Coordinate look,const location::geo_pos::GeoPos& gp, double ha){
look.Sigma(look.Sigma()+gp.Latitude());
//зы. долбаный медвед со своими часовыми поясами
float ti = ha;//life_time.toGMTf() + (gp.TimeZone()-life_time.timezone);
/*было бы здорово без 25.
теперь 25 нету. но всё равно чего-то нехватает. а может и что-нибудь неправильно
нет всё, вроде, правильно, только в году не 365 дней.
•точка равнодействия дрейфует по элептической орбите
•скорость движения планет по орбите неравномерна
ну и где тут РАЗНЫЕ планеты!?*/
ti=ti*15. + gp.Longitude();
//ti+=float(life_time.Time.date().dayOfYear()-80)/365.*360.;
//glRotatef(-gp.Latitude(),0,0,1);
look.Alpha(look.Alpha()+ti);
double o_look[3];coordinates::converting::SpherToCartesian(look,o_look);
gluLookAt(0,0,0, o_look[0], o_look[1], o_look[2],    0,1,0);               //последнию тройку - наблюдателю
}

namespace position{
//-------------------
void in_solar_system(const location::Location &location){
coordinates::Coordinate s_poisce; //сфер. координаты наблюдателя
double x_poise[3];
if (location.on_ground)  s_poisce=location.getPlanet()->coordinate;
else                     s_poisce=location.space_pos;
s_poisce.Alpha(s_poisce.Alpha()+180); // почему, блять, +180 !?
coordinates::converting::SpherToCartesian(s_poisce,x_poise);
glTranslated(x_poise[0], x_poise[1], x_poise[2]);}
//----------------------
void geopos(const location::Location& location){
coordinates::Coordinate geo_pos=coordinates::Coordinate(location.gp.Longitude()
                ,location.gp.Latitude(),1,coordinates::type::horizontal);
elementus_mundus::Sphere s;
if(dynamic_cast<const elementus_mundus::altus_mundus::Astrum*>(location.getPlanet())){
const elementus_mundus::altus_mundus::Astrum *a = dynamic_cast<const elementus_mundus::altus_mundus::Astrum*>(location.getPlanet());
s=a->sphere;}else{
const elementus_mundus::proxima_mundus::OrbiterBody* ob = dynamic_cast<const elementus_mundus::proxima_mundus::OrbiterBody*>(location.getPlanet());
s=ob->sphere;}
geo_pos.Range(location.gp.Height()/1.+s.PolarR()); /* + точное вычисление радиуса*/
//double *x_geopos=coordinates::converting::SpherToCartesian(geo_pos);
glRotatef(-location.gp.Latitude(),0,0,1);
glRotatef(-location.gp.Longitude(),0,1,0);
glTranslated(0,0,geo_pos.Range());}
}
//----------------------
void local_observers_center(const location::Location& location){
if (location.getPlanet()!=NULL){
coordinates::Coordinate center=location.getPlanet()->coordinate;
double center_xyz[3]; coordinates::converting::SpherToCartesian(center,center_xyz);
glTranslated(-center_xyz[0], -center_xyz[1], -center_xyz[2]);}}
}

/***********************************/
namespace universe{

const float max_vmag = -1.5;
 //херня какая-то с размерами
const float tVmag = 6.0;
const float max_star_size = -max_vmag+tVmag; //максимальный размер точки, символизирующей звезду
const float min_star_size = 1.0;

//видна ли звезда c блеском 'wm' при настройках видимости 'w'
inline bool is_visible(float vm,howIsee &h) {return (vm<h.astra*h.k);}

/*
Сириус -1,47 (?) если и понадобится большая яркость, то, вероятно, не очень скоро
• относительный блеск
• добавочный блеск
*/
//------------- блеск Одного Бритого Англичанина Финики Жевавшего Как Морковь
float getSizeByVisualMag(float vmag){
if (vmag>=tVmag) return 1/*GLmin_point_size*/; else
return max_star_size*1.5-(vmag-max_vmag)*1.5;}

/************************************************************/
using namespace g_containers;
/*функции рисование солнечной системы */
namespace solar_system{
void star(float polar_r, float equator_r,const universe::altus_mundus::spectral_class::SpectralClass& sc){
float* star_color=getColorBySpecClass(sc);
//float star_color_a[4]; star_color_a[3]=1;
//for (unsigned char i=0; i<3; i++) star_color_a[i]=star_color[i];
//glEnable(GL_COLOR_MATERIAL);
//glMaterialfv(GL_FRONT, GL_EMISSION, star_color_a);
glColor3fv(star_color); delete[] star_color;
elementus::Sphere((polar_r*10000000+equator_r)/2); // + точные пропорции
/*glDisable(GL_COLOR_MATERIAL);*/}
//-------------------------
void test_star(float polar_r, float equator_r,const universe::altus_mundus::spectral_class::SpectralClass& sc){
float* star_color=getColorBySpecClass(sc);
float star_color_a[4]; star_color_a[3]=1;
for (int i=0; i<3; i++) star_color_a[i]=star_color[i];
delete[] star_color;
//GLfloat b[]={0.0, 0., 0., 0.9};
glColor3fv(star_color);
elementus::Sphere((polar_r+equator_r)/2);
}
//-----------------------------------------
void planet(const proxima_mundus::Planet &p,const g_containers::Planet *graph_planet){
double x_planet[3]; coordinates::converting::SpherToCartesian(p.coordinate,x_planet);
glPushMatrix();
glTranslated(x_planet[0], x_planet[1], x_planet[2]);
GLUquadricObj *quadObj;
quadObj = gluNewQuadric();
gluQuadricTexture(quadObj, GL_TRUE);
gluQuadricNormals(quadObj, GLU_SMOOTH);
 gluQuadricDrawStyle(quadObj, GLU_FILL);
 if (graph_planet) glBindTexture(GL_TEXTURE_2D,graph_planet->getTextId());  // это тоже вредит пэинтеру
 glRotated(p.DayPhase(),0,1,0);

 glRotated(-90+p.EqToOrbit(),1,0,0); //+ наклон планеты. полюса расположены горизонтально
  /*ось вращения и экватор*/
// glColor3f(1,0.3,0.3); glLineWidth(2.1);
// glEnable(GL_DEPTH_TEST);
// glBegin(GL_LINES);
// glVertex3f(0,0,-p.sphere.PolarR()*1.25);
// glVertex3f(0,0,p.sphere.PolarR()*1.25);
// glEnd();
 glColor3f(1,1,1);
 gluSphere(quadObj,p.sphere.PolarR(), 32, 32);
 gluDeleteQuadric(quadObj);
 glPopMatrix();
}
//---------------------------
void planet(const proxima_mundus::Planet &p){
double x_planet[3];coordinates::converting::SpherToCartesian(p.coordinate,x_planet);
glTranslated(x_planet[0], x_planet[1], x_planet[2]);
glPushMatrix();
GLUquadricObj *quadObj;
quadObj = gluNewQuadric();
gluQuadricTexture(quadObj, GL_TRUE);
gluQuadricNormals(quadObj, GLU_SMOOTH);
 gluQuadricDrawStyle(quadObj, GLU_FILL);
 glRotated(p.DayPhase(),0,1,0);
 glRotated(-90+p.EqToOrbit(),1,0,0); //+ наклон планеты. полюса расположены горизонтально
 gluSphere(quadObj,p.sphere.PolarR(), 32, 32);
 gluDeleteQuadric(quadObj);
 glPopMatrix();
 glTranslated(-x_planet[0], -x_planet[1], -x_planet[2]);
}
//------------------------- draw planets except 'except'. textures
unsigned planets(const g_containers::PlanetMap& graphic_planets,
                                 const planetary_system::PlanetList& pl, const whatISee& w, ID_Count_type except){
unsigned drawed=0;double x_planet[3];
proxima_mundus::Planet *p;
g_containers::Planet *gp;
glEnable(GL_TEXTURE_2D);
for (planetary_system::PlanetList::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
p=*it; gp=graphic_planets.find(p->absID())->second;
if (/*сравнение с предельной видимой величиной*/ 0 || p->absID()!=except){
drawed++;
if(graphic_planets.count(p->absID()))
solar_system::planet(*p,gp);}
for(unsigned i=0;i<p->getNSats()->size();i++){
coordinates::converting::SpherToCartesian(p->coordinate,x_planet);
gp=graphic_planets.find((*it)->getNSats()->at(i)->absID())->second;
glPushMatrix();
glTranslated(x_planet[0], x_planet[1], x_planet[2]);
if(graphic_planets.count((*it)->getNSats()->at(i)->absID()))
solar_system::planet(*(*it)->getNSats()->at(i),gp);
drawed++;
glTranslated(-x_planet[0], -x_planet[1], -x_planet[2]);
glPopMatrix();}
}
glDisable(GL_TEXTURE_2D);
return drawed;}
//-------------------- simple spheres without textures
unsigned planets(const planetary_system::PlanetList& pl, const whatISee& w, ID_Count_type except){
unsigned drawed=0;double x_planet[3];
elementus_mundus::proxima_mundus::Planet *p;
elementus_mundus::proxima_mundus::Planet *sat;
for (planetary_system::PlanetList::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
p=*it;
if (p->absID()!=except) {solar_system::planet(*p);drawed++;}
for(unsigned i=0;i<p->getNSats()->size();i++){
sat = p->getNSats()->at(i);
if (sat->absID()!=except){
coordinates::converting::SpherToCartesian(sat->coordinate,x_planet);
glPushMatrix();
glTranslated(x_planet[0], x_planet[1], x_planet[2]);
solar_system::planet(*sat);drawed++;
glTranslated(-x_planet[0], -x_planet[1], -x_planet[2]);
glPopMatrix();}}
}return drawed;}
//------------------------
/*функция должна быть рекурсивной*/
unsigned planet_points(const planetary_system::PlanetList& pl, ID_Count_type except){
proxima_mundus::Planet *p;
glEnable(GL_POINT_SMOOTH);
double xyz[3],_xyz[3];
for (planetary_system::PlanetList::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
p=*it;
if (/*сравнение с предельной видимой величиной*/ 0) continue;
if (p->absID()!=except){
coordinates::converting::SpherToCartesian(p->coordinate,xyz);
glColor3f(0.9,0.9,0.9);
glPointSize(10);/*glEnable(GL_POINT_SMOOTH);*/
glBegin(GL_POINTS);glVertex3dv(xyz);glEnd();
glPointSize(1.5);glColor3f(1,1,1);
glBegin(GL_POINTS);glVertex3dv(xyz);glEnd();}

for(unsigned i=0;i<p->getNSats()->size();i++){
if (p->getNSats()->at(i)->absID()==except) continue;
coordinates::Coordinate &c=p->getNSats()->at(i)->coordinate;
coordinates::converting::SpherToCartesian(c,_xyz);
glPushMatrix();
glTranslated(xyz[0], xyz[1], xyz[2]);
glPointSize(9);
glBegin(GL_POINTS);glVertex3dv(_xyz);glEnd();
glPointSize(1.5);glColor3f(1,1,1);
glBegin(GL_POINTS);glVertex3dv(_xyz);glEnd();
glTranslated(-xyz[0], -xyz[1], -xyz[2]);
glPopMatrix();
}}glDisable(GL_POINT_SMOOTH);
return 0;}
//-----------------------------------------
void orbit(double *o, unsigned len){
glEnableClientState(GL_VERTEX_ARRAY);
glVertexPointer(3, GL_DOUBLE,0, o);
glDrawArrays(GL_LINE_LOOP,0,len/3);
glDisableClientState(GL_VERTEX_ARRAY);}
//-----------------------------------------
void orbit(const g_containers::vOrbit& o){
solar_system::orbit(o.orbit,o.getOrbSegCount());}
//-----------------------------------------
void orbits(const g_containers::vOrbitMap& om, const planetary_system::PlanetList& pl){
double x_planet[3];
elementus_mundus::proxima_mundus::Planet *p;
for (planetary_system::PlanetList::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
p=*it;
vOrbit *vo= om.find(p->absID())->second; solar_system::orbit(*vo); // орбита планеты
for(unsigned i=0;i<(*it)->getNSats()->size();i++){
coordinates::converting::SpherToCartesian((*it)->coordinate,x_planet);
glPushMatrix();
glTranslated(x_planet[0], x_planet[1], x_planet[2]);
vOrbit *vo=(*om.find(p->getNSats()->at(i)->absID())).second;
solar_system::orbit(*vo);
glTranslated(-x_planet[0], -x_planet[1], -x_planet[2]);
glPopMatrix();}
}}
//-----------------------------
void asteroid_belt(){

}
//---------------
void kuiper_belt(){

}
//------------
void comet(const proxima_mundus::Cometa &comet){
double x_planet[3];coordinates::converting::SpherToCartesian(comet.coordinate,x_planet);
glTranslated(x_planet[0], x_planet[1], x_planet[2]);
//glPushMatrix();
GLUquadricObj *quadObj;
quadObj = gluNewQuadric();
//gluQuadricTexture(quadObj, GL_TRUE);
//gluQuadricNormals(quadObj, GLU_SMOOTH);
gluQuadricDrawStyle(quadObj, GLU_FILL);
//glRotated(p.DayPhase(),0,1,0);
//glRotated(-90+p.EqToOrbit(),1,0,0); //+ наклон планеты. полюса расположены горизонтально
gluSphere(quadObj,comet.sphere.PolarR(), 32, 32);
gluDeleteQuadric(quadObj);
//glPopMatrix();
glTranslated(-x_planet[0], -x_planet[1], -x_planet[2]);}
//--------------
unsigned comets(const planetary_system::CometList& cl,const whatISee& w){
unsigned drawed=0;
proxima_mundus::Cometa *comet;
    for (planetary_system::CometList::const_iterator it=cl.begin(), end=cl.end(); it!=end; it++){
    comet=*it;
    if (/*сравнение с предельной видимой величиной*/ 0) continue;
    drawed++;solar_system::comet(*comet);
    }return drawed;}
//--------------
void spacecraft(){

}
void orbit_features(const planetary_system::PlanetList& pl){
proxima_mundus::Planet *p;
for (planetary_system::PlanetList::const_iterator it = pl.begin(), end = pl.end(); it!=end; it++){
p=*it;
double x=p->kep_orbit.WP()*coordinates::converting::toRad;
double r = p->kep_orbit.A()*AU/(1+p->kep_orbit.E());
glPushMatrix();
glRotated(p->kep_orbit.W(),0,1,0);
glRotated(p->kep_orbit.I(),1,0,0);
glBegin(GL_LINES);
glVertex3d(sin(x)*r,0, cos(x)*r);
glVertex3d(0,0,0);
glEnd();
glPopMatrix();
}
}

//---------------------------------------- all solar system
void all(const E2& e2,  g_containers::gMundus &vc,const screen::plan_elements& pe){
const observer::perceptionem::Perceptionem&p = e2.Galileo_Galilei->perception;
const planetary_system::PlanetarySystem& ss = e2.adb->data->getNativeSS();
using namespace electum_necessarius;
glEnable(GL_ALPHA_TEST);
glEnable(GL_LINE_SMOOTH);
glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
glEnable(GL_BLEND);
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
if (p.what_i_see.things[imagination::orbit]){glLineWidth(pe.orbit.width);
glColor4fv(pe.orbit.color); visualization::universe::solar_system::orbits(*vc.orbits,e2.adb->data->getNativeSS().getPlanets());}
if (p.what_i_see.things[imagination::orbit_features]){orbit_features(ss.getPlanets());glLineWidth(pe.orbit_features.width);
glColor4fv(pe.orbit_features.color);}
if (p.what_i_see.things[imagination::ecliptic_disc]){glColor4fv(pe.ecliptic_disc.color);glLineWidth(pe.ecliptic_disc.width);
if (!e2.location->on_ground) visualization::imaginary_draw::ecliptic_disc(coordinates::astronomical_unit/10);
else visualization::imaginary_draw::equator(coordinates::astronomical_unit*50);}

glDisable(GL_ALPHA_TEST);
glDisable(GL_LINE_SMOOTH);
glDisable(GL_BLEND); // должно быть включено, чтобы пэинтер не закрашивал весь экран

ID_Count_type ndtp = 0; // no draw this object
if (e2.location->on_ground) ndtp=e2.location->getPlanet()->absID();

if (p.what_i_see.things[electum_necessarius::universe::ps::planet]){
/*sun*/
double sun_r = ss.getStar()->sphere.PolarR();
float *sc=getColorBySpecClass(ss.getStar()->spec_class);
for (unsigned short i=0; i<3; i++) sc[i]*=0.75;
glPointSize(14); // зависимость от яркости
glEnable(GL_POINT_SMOOTH);
glColor3fv(sc); //color from spectral class
glBegin(GL_POINTS);glVertex3f(0,0,0);glEnd();
delete[] sc;
planet_points(ss.getPlanets(), ndtp);
/*planets*/
glEnable(GL_DEPTH_TEST); star(sun_r, sun_r, ss.getStar()->spec_class);
if (vc.gplanets->size()) planets(*vc.gplanets,ss.getPlanets(),p.what_i_see, ndtp);
else{ glDisable(GL_TEXTURE_2D);   planets(ss.getPlanets(),p.what_i_see,ndtp);}
glDisable(GL_DEPTH_TEST);} //+

asteroid_belt(/*some parameters*/);
spacecraft();

kuiper_belt(/*some parameters*/);
/*рассеяный диск*/
/*облако Оорта*/

if (p.what_i_see.things[electum_necessarius::universe::ps::comet]){glEnable(GL_DEPTH_TEST);
orbiter_points<planetary_system::CometList>(e2.adb->data->getNativeSS().getComets(),
    e2.Galileo_Galilei->perception.what_i_see,ndtp);glDisable(GL_DEPTH_TEST);}
glColor4f(1,1,1,1);
glDisable(GL_DEPTH_TEST);}
}

namespace deep_sky{
//--------------
void asterizm(const profundus_mundus::Asterizm& A){
profundus_mundus::AstraListsVector cl=A.pattern;
glLineWidth(A.Line_width());
glColor3fv(A.getColor());
for (unsigned i=0; i<cl.size(); i++){
const profundus_mundus::AstraList *a = cl.at(i);
if (a==NULL) break; else elementus::line(*a);}}
//--------------------------------------------------------
void asterism_list(const profundus_mundus::AsterismList& cl){
//массивы вершин
if (cl.size()==0) return;
for (std::list<profundus_mundus::Asterizm*>::const_iterator it=cl.begin(), end = cl.end(); it!=end; it++) asterizm(*(*it));}
//------------------------------------------
int draw_astras(const g_containers::VertAstras &vc,const howIsee &h,float add_vmag=0, int mode=0){
if (vc.arraysCount()<1) return -1;
//unsigned vMagRange = ceil(fabs(vc.minVMag())+fabs(vc.maxVMag()));
glEnable(GL_POINT_SMOOTH);
glEnableClientState(GL_VERTEX_ARRAY);
glEnableClientState(GL_COLOR_ARRAY);
float max_vmag = h.k*h.astra;
unsigned long i =0;
if (mode==0) glPointSize(1);
float size = 0;
while(i*vc.Step()+vc.minVMag() <= max_vmag && i<vc.arrays_count){
glVertexPointer(3, GL_DOUBLE,0, vc.vert[i]);
if (mode==1) size=getSizeByVisualMag(i*vc.Step()+vc.minVMag())+1;
else size=1;
glPointSize(size);
glColorPointer(3,GL_FLOAT, 0, vc.color[i]);
glDrawArrays(GL_POINTS, 0, vc.arrays_len[i]);
i++;}
glDisableClientState(GL_VERTEX_ARRAY);
glDisableClientState(GL_COLOR_ARRAY);
glDisable(GL_POINT_SMOOTH);
return 0;}
//------------------------------------------
template <class T>
int draw_caelestis(const g_containers::VertCaelestis<T> &vc,const howIsee &h, float add_vmag=0){
if (vc.arrays_count<=0) return -1;
//unsigned vMagRange = ceil(fabs(vc.min_vMag)+fabs(vc.max_vMag));
glEnableClientState(GL_VERTEX_ARRAY);
float max_vmag = h.k*h.astra;
for (unsigned i = 0; i<vc.arrays_count && i*vc.Step()+vc.minVMag() <= max_vmag; i++){
glPointSize(getSizeByVisualMag(i+vc.min_vMag)+add_vmag);
glVertexPointer(3, GL_DOUBLE,0, vc.vert[i]);
glDrawArrays(GL_POINTS, 0, vc.arrays_len[i]);}
glDisableClientState(GL_VERTEX_ARRAY);
return 0;}
//------------------------------------------
void sky_texture(unsigned tex_id){
glPushMatrix();
glEnable(GL_TEXTURE_2D);
glColor4f(1,1,1,0);
GLUquadricObj *quadObj;
quadObj = gluNewQuadric();
if (tex_id) gluQuadricTexture(quadObj, GL_TRUE);
else glColor3f(0,0,0);
gluQuadricOrientation(quadObj, GLU_INSIDE);
gluQuadricNormals(quadObj, GLU_SMOOTH);
gluQuadricDrawStyle(quadObj, GLU_FILL);
glBindTexture(GL_TEXTURE_2D,tex_id);
if (glGetError()!=GL_NO_ERROR) glColor3f(0,0,0);
glRotatef(1058, 0,0,1);  // правильного расположение млечного пути на небосводе.
glRotatef(-70, 0,1,0);
 gluSphere(quadObj,coordinates::very_far, 32, 32); /*for 7 see coordinates::very_far*/
 gluDeleteQuadric(quadObj);
glDisable(GL_TEXTURE_2D);
glPopMatrix();
}
/**/

//--------------------------------------------------------------------
/*обязательно ли небо передавать? */
//int all(const profundus_mundus::ProfundusMundus &pm, g_containers::gProfundusMundus &vc,const observer::Observer &o);
int all(const profundus_mundus::ProfundusMundus &pm,
        const g_containers::gProfundusMundus &vc,const observer::perceptionem::Perceptionem &p,const screen::ScreenParameters& sp){
using namespace electum_necessarius::universe;
using namespace electum_necessarius::imagination;
glDisable(GL_DEPTH_TEST);
if (p.what_i_see.things.test(imagination::grid::eq)){
screen::col_wid cw = sp.getPGElements().grid.equatorial;
visualization::imaginary_draw::grid(cw.color,cw.width, coordinates::very_far/100000,18,24);}
/*тут откуда-то берутся параметры отображения каждой хрени*/
glColor3f(0,1,0);
glPointSize(3.0);
if (p.what_i_see.isUvisible(imagination::c_borders)) {glColor3f(0,1,0); glPointSize(1.5); glEnable(GL_LINE_STIPPLE);  glLineStipple(1,52428);
            visualization::elementus::vert_arrays(*vc.getConstellations().constellation_borders,GL_LINE_LOOP);glDisable(GL_LINE_STIPPLE);}
if (p.what_i_see.isUvisible(imagination::asterism))  {asterism_list(pm.getAsterisms());}
if (p.what_i_see.things[ds::dso]){ glColor3f(0,1,0);draw_caelestis<profundus_mundus::DSOList>(vc.getODSs(),p.how_i_see,3);}
if (p.what_i_see.things[ds::caelestis]) draw_caelestis<profundus_mundus::CaelestisList>(vc.getCaelestis(),p.how_i_see,0);
if (p.what_i_see.isUvisible(ds::astra)) draw_astras(vc.getAstras(), p.how_i_see,0,p.relative_vmag);
//if (p.what_i_see.isLvisible(imagination::asterizm))
return 0;}
}
}}

/************   SELECT   ***************************************************/
namespace select_astro{
namespace draw{
using namespace electum_necessarius;
using namespace profundus_mundus;
using namespace planetary_system;
using namespace coordinates;
using namespace elementus_mundus::proxima_mundus;
using namespace elementus_mundus::altus_mundus;
using namespace visualization::elementus;
//--------------------------------------
void stars(const AstraList &stars,const howIsee &h){
Astrum *a;double xyz[3];
for(AstraList::const_iterator it=stars.begin(), end=stars.end(); it!=end; it++){
a=*it;
if (a->vMag()>h.astra*h.k+1) {continue;}
 coordinates::converting::SpherToCartesian(a->coordinate,xyz);
glLoadName(a->absID());
glBegin(GL_POINTS);
glVertex3dv(xyz);glEnd();}}
//--------------------------------------------------------
void caelestis(const CaelestisList& cl,const howIsee &h){
if (!cl.size()) return;
Caelestis *a;double xyz[3];
for(CaelestisList::const_iterator it=cl.begin(), end=cl.end(); it!=end; it++){
a=*it;
if (a->vMag()>h.astra*h.k) {continue;}
 coordinates::converting::SpherToCartesian(a->coordinate,xyz);
glLoadName(a->absID());
glBegin(GL_POINTS);
glVertex3dv(xyz);glEnd();}
}
//------------------------------------------------
void planets(const PlanetList &pl, const howIsee& w){
elementus_mundus::proxima_mundus::Planet *p;
glMatrixMode(GL_MODELVIEW);double xyz[3];
for (planetary_system::PlanetList::const_iterator it=pl.begin(), end =pl.end();it!=end;it++){
p=*it;
glPushMatrix();
//glLoadIdentity();
Coordinate c=p->coordinate;
c.Range(c.Range()*AU);
coordinates::converting::SpherToCartesian(c,xyz);
glTranslatef(xyz[0],xyz[1],xyz[2]);

glLoadName(p->absID());
glColor3f(1,1,1);
glPointSize(1.0);
glBegin(GL_POINTS);
glVertex3f(0,0,0);
glEnd();
Sphere(p->sphere.PolarR());
glPopMatrix();
}}
//-------------------------------------------------
void all(const AstroDB &adb, const location::Location& location, const QDateTime &t,
        const observer::perceptionem::Perceptionem &p, elementus_mundus::ID_Count_type nd){
using namespace electum_necessarius::universe;
glInitNames();
glPushName(0);
if (p.what_i_see.isUvisible(ds::astra)) {
stars(adb.data->global_space->getStars(),p.how_i_see);}
if (p.what_i_see.isUvisible(ds::caelestis)) caelestis(adb.data->global_space->getCaelestis(),p.how_i_see);
if (p.what_i_see.isUvisible(ds::dso))
     visualization::universe::deep_sky::some_deep_sky(adb.data->global_space->getDSOs(),p.how_i_see);
/*if (w.what & what::galaxy) select::galaxys(pm.deep_space.ShowCaelestis(),w]);
if (w.what & what::nebula) select::nebulas(pm.deep_space.ShowCaelestis(),w);
if (w.what & what::GC) select::GCs(pm.deep_space.ShowCaelestis(),w);
if (w.what & what::OC) select:OCs(pm.deep_space.ShowCaelestis(),w);*/
glRotatef(u_model::EclipAnglEquator(t.date()),1,0,0);
if (!location.on_ground) visualization::init::local_observers_center(location);
//
if (adb.data->getNativeSS().getStar()->absID()!=nd){
glPointSize(11);glLoadName(adb.data->getNativeSS().getStar()->absID());
glBegin(GL_POINTS); glVertex3f(0,0,0); glEnd(); Sphere(adb.data->getNativeSS().getStar()->sphere.PolarR());}

if (p.what_i_see.isUvisible(ps::planet)) ss_orbiters<PlanetList>(adb.data->getNativeSS().getPlanets(),p.how_i_see,nd);
if (p.what_i_see.isUvisible(ps::comet)) ss_orbiters<CometList>(adb.data->getNativeSS().getComets(),p.how_i_see,nd);
/*орбиты?*/ //see draw::solar_system
/*астеризмы*/ //see darw::altus_mundus
}
}}

/*


*/

