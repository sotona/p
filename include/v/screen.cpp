﻿#include "screen.h"
#include "visualization.h"

namespace screen{
//-------------------
ScreenParameters::ScreenParameters(){
glGetFloatv(GL_POINT_SIZE_RANGE,basicGLparams.point_size);
glGetFloatv(GL_POINT_SIZE_GRANULARITY,&basicGLparams.point_size_granularity);
}
//---------------------------------------
void ScreenParameters::resize(int _width, int _height){
width=_width; height=_height;
/*//smthng else*/}
//----------------------------------------
float ScreenParameters::aspectRatio(){
return float(width)/height;}
//------------------------------
void ScreenParameters::setPGElements(const plan_elements &pe){
this->p_elements = pe;}
//-----------
plan_elements ScreenParameters::getPGElements() const{
return this->p_elements;}

namespace labels{
using namespace profundus_mundus;
using namespace observer::perceptionem;

void deep_sky(QPainter &p, const ProfundusMundus&pm, const Perceptionem &pe, const col_wid &cw){
const AstraList &al = pm.getStars();
elementus_mundus::altus_mundus::Astrum *a;
GLint viewport[4]; glGetIntegerv(GL_VIEWPORT,viewport); // каждый раз ?
GLdouble coord_2d[3];double xyz[3];
//p.setPen(QColor(cw.color[0]*255, cw.color[1]*255,cw.color[0]*255));
for (profundus_mundus::AstraList::const_iterator it=al.begin(), end=al.end(); it!=end; it++){a=*it;
if (a->vMag() <= pe.how_i_see){
coordinates::converting::SpherToCartesian(a->coordinate, xyz);
if (visualization::aux::get2Dcoordinate(coord_2d, xyz)) if (coord_2d[2]<=1.)
if (coord_2d[0] >= 0-50 && coord_2d[0] <= viewport[2] && coord_2d[1] >=0 && coord_2d[1] <=viewport[3])
p.drawText(100,100/*coord_2d[0], coord_2d[1]*/,/*a->name()*/"123");}}}

}
}
