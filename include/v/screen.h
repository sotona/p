﻿#ifndef SCREEN_H
#define SCREEN_H

#include <GL/gl.h>
#include <QPainter>
#include "../e2/e/mundus.h"
#include "../e2/observer.h"
namespace screen{
struct col_wid{
float *color; // RGBA color. использовать четырёхбайтовый вариант.
float width;
};
// color and width
struct col_wid_str: col_wid{
unsigned factor;        // for glLineStripple
unsigned pattern;
};
/* параметры графических элементов планетария */
struct plan_elements{
col_wid orbit;
col_wid s_orbit;    // s is for selected
col_wid orbit_features; // это пока означает радиус векторы перигелия
//col_wid connections;    //
struct{
col_wid ds;
col_wid ps;
col_wid screen;
col_wid constellation;
col_wid grid;   // по идее, здесь цвет - не нужен, он такой же как и у сетки
} text;

struct{
col_wid_str azimuthal;
col_wid_str equatorial;
col_wid_str ecliptic;
} grid;

col_wid ecliptic_disc;
float* ps_sel_mark_color;
float* ds_sel_mark_color; // color for selected mark
};

class ScreenParameters{
private:
unsigned width, height;
float fov; //fov by y
plan_elements p_elements; // параметры граф. элементов планетария

struct {
float minXpoit;  // multiplier for minimal point size. used to get minimal program point size;
float minXwidth; // some, but for line
float point_size[2];
float point_size_granularity;} basicGLparams;

public:
ScreenParameters();
float minPointSize();
float maxPointSize();
float aspectRatio();
void setPGElements(const plan_elements& pe); // set planetarium graphic elements parameters
plan_elements getPGElements() const;
void resize(int width, int height);
void changeFOV(); //что за функция?
};

namespace labels{
void deep_sky(QPainter&p,const profundus_mundus::ProfundusMundus& pm,
                                             const observer::perceptionem::Perceptionem& pe,const col_wid& cw);
void planet_system(QPainter&p,const profundus_mundus::ProfundusMundus& pm,
                                             const observer::perceptionem::Perceptionem& pe,const col_wid& cw);}
}
#endif // SCREEN_H
