﻿#include "g_container.h"
#include "../e2/e/elementus_mundus.h"
#include "../e2/whatIsee.h"
//#include <QGLWidget>
namespace g_containers{
using coordinates::CoordList;
using profundus_mundus::CaelestisList;
using profundus_mundus::AstraList;
using profundus_mundus::DSOList;

using namespace elementus_mundus::altus_mundus;
template class VertCaelestis<AstraList>;
template class VertCaelestis<CaelestisList>;
template class VertCaelestis<DSOList>;

Planet::Planet(GLuint texture_id,const char *name_planet){
this->texture_id=texture_id;
this->planet_name=strdup(name_planet);}

GLuint Planet::getTextId() const{
return texture_id;}


float def_vmag_step=0.5;
/**************** VERT Astras ********************/
//-----------------------------------------------вертексно-цветной конструктор
VertAstras::VertAstras(){
color = NULL;
bk_color=NULL;}

VertAstras::VertAstras(const profundus_mundus::AstraList &cl, float vMag_step):
VertCaelestis<AstraList>(cl,vMag_step){
if (!cl.size()) {this->arrays_count=0;
                     this->color=NULL;
                     bk_color=NULL; return;}
unsigned stars_colored=0;
//unsigned vMagRange = ceil(fabs(min_vMag)+fabs(max_vMag));
this->color = new GLfloat*[arrays_count];
this->bk_color = new GLfloat*[arrays_count];
for (unsigned i=0; i<arrays_count; i++){
double i_min = i*step+min_vMag;
double i_max = (i+1)*step+min_vMag;
unsigned hm = profundus_mundus::how_much<AstraList>(cl,i_min,i_max);
if (hm==0) {this->color[i]=NULL; this->bk_color[i]=NULL; continue;}
else {this->color[i] = new GLfloat[hm*3]; /*bk_color[i]=new GLfloat[hm*3];*/}
in_all+=hm;

stars_colored+=create::color_array(cl,i_min ,i_max, color[i]);
//unsigned long c=make_color_array(cl,i_min ,i_max, bk_color[i]);
//if (bk_color[i]!=NULL)
//for (unsigned long j=0; j<c; j++){
//bk_color[i][j]*=0.8;}
}

//color[vMagRange-1] = new GLfloat[cl.size()*3];// оптимизировать размер массива
//stars_colored+=make_color_array(cl, min_vMag+vMagRange-1 ,max_vMag, color[vMagRange-1]);
}
//-------------------
VertAstras::~VertAstras(){
if (color!=NULL){
for (unsigned i=0; i<this->arrays_count; i++)
delete[] this->color[i]; delete[] color;}
if (color!=NULL) {for (unsigned i=0; i<this->arrays_count; i++) delete[] this->bk_color[i];
delete[] bk_color;}}

/**************** VERT Caeestis ********************/
//-----------------------------------------------вертексно-цветной конструктор
template <class T>
VertCaelestis<T>::VertCaelestis(){
arrays_count = 0;
arrays_len = NULL;
vert = NULL;
in_all =0;
step = def_vmag_step;}

template <class T>
VertCaelestis<T>::VertCaelestis(const T &cl, float vMag_step){
vMag_step=fabs(vMag_step);
if (!cl.size()){    // astras?
arrays_count = 0;
arrays_len = NULL;
vert = NULL;
in_all =0;
step = vMag_step;
return;}
in_all=0;
typename T::value_type a;
this->step = vMag_step;
a = *cl.begin();
this->max_vMag = a->vMag();
this->min_vMag = a->vMag();
//была где-то функция дающая мин и макс блеск...
for(typename T::const_iterator it=cl.begin(), end=cl.end(); it!=end; it++){
a=*it;
if (a->vMag()>max_vMag) max_vMag = a->vMag();
else if (a->vMag()<min_vMag) min_vMag = a->vMag();}

//max_vMag = min(max_vMag, h.astra*h.k); //максимум. не более того, что может разглядеть наблюдатель
unsigned vMagRange = ceil(max_vMag - min_vMag);
arrays_count = ceil(vMagRange)/step;
this->vert = new GLdouble*[arrays_count];
arrays_len = new unsigned long[arrays_count];
long hm;
for (unsigned i=0; i<arrays_count; i++){ //+ для вещественного step
double i_min = i*step+min_vMag;
double i_max = (i+1)*step+min_vMag;
hm = profundus_mundus::how_much<T>(cl,i_min,i_max);
if (hm==0) {this->vert[i]=NULL; this->arrays_len[i]=0;continue;}
else this->vert[i] = new double[hm*3];
in_all+=hm;
this->arrays_len[i] = create::vert_array<T>(cl,i_min ,i_max, vert[i]);
}}
//-------------------
template <class T>
VertCaelestis<T>::~VertCaelestis(){
if (arrays_len!=NULL) delete[] this->arrays_len;
if (vert!=NULL)
for (unsigned long i=0; i<this->arrays_count; i++){
delete[] this->vert[i];}}
//-------------

//namespace create{
//double* make_vert_orbit(const CoordList &orbit){
//double* v_orbit=new double[orbit.size()*3];
//unsigned i=0;
//double *v;
//for (CoordList::const_iterator it=orbit.begin(),end=orbit.end(); it!=end;it++){
//coordinates::Coordinate c=*(*it);
////c.Range(c.Range()*AU);
//v=coordinates::converting::SpherToCartesian(c);
//v_orbit[i++]=v[0];
//v_orbit[i++]=v[1];
//v_orbit[i++]=v[2];
//delete[] v;}
//return v_orbit;}}

/****************** Orbit ***************/
vOrbit::vOrbit(const CoordList& orbit, long segments, elementus_mundus::ID_Count_type planet_id_){
this->orbit=create::vert_array(orbit);
this->orb_segments_count=orbit.size()*3;
this->planet_id = planet_id_;}
//-------------
void vOrbit::setOrbit(double *o, long &len){
if (o==NULL || len<1) return;
delete[] orbit;
orbit = new double[len];
for (int i=0; i<len; i++) orbit[i]=o[i];}
//--------------
long vOrbit::getOrbSegCount() const{
return this->orb_segments_count;}
//----------------------
vOrbitMap* make_vOrb_list(const planetary_system::PlanetList& pl,const QDateTime& epoch,const QDateTime& now){
vOrbitMap *orb=new vOrbitMap;
for (planetary_system::PlanetList::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
CoordList* cl=planetary_system::make_orbit(**it,now,epoch,365);
orb->insert(std::pair<elementus_mundus::ID_Count_type,vOrbit*>((*it)->absID(),new vOrbit(*cl,1)));
if ((*it)->getNSats()->size()>0)
for(unsigned i=0;i<(*it)->getNSats()->size();i++){
cl=planetary_system::make_orbitl(*(*it)->getNSats()->at(i),now,epoch,365);
orb->insert(std::pair<elementus_mundus::ID_Count_type,vOrbit*>((*it)->getNSats()->at(i)->absID(),new vOrbit(*cl,1)));}
}
return orb;}

FastSidusis::FastSidusis(){
this->constellation_borders=NULL;
this->first_sid_id=0;
this->sidCenterVector=NULL;}

FastSidusis::FastSidusis(const profundus_mundus::SidusList &sl){
constellation_borders = new VertArrayVector;
this->sidCenterVector = new coordinates::CoordVector;
if (sl.size()){
this->sidCenterVector->resize(sl.size());
unsigned i=0;
profundus_mundus::SidusList::const_iterator it=sl.begin(), end=sl.end();
this->first_sid_id = (*it)->absID();
for (; it!=end; it++, i++){
elementus_mundus::altus_mundus::sidus::Sidus *s=*it;
coordinates::CoordList cl=s->getBorderPoints();
Vert_array *va=new Vert_array;
va->array=create::vert_array(cl);
va->length=cl.size();
constellation_borders->push_back(va);
coordinates::Coordinate* c = new coordinates::Coordinate;
coordinates::mid_value(cl,*c);
this->sidCenterVector->push_back(c);}}
}

FastSidusis::~FastSidusis(){

}

/**********  VERT DEEP SPACE **************/
gProfundusMundus::gProfundusMundus(){
astras=NULL;
caelestis=NULL;
dso=NULL;
this->vMag_step = 0.5;}
//------------------------
gProfundusMundus::gProfundusMundus(const profundus_mundus::ProfundusMundus &c, float vMag_step_){
this->vMag_step = fabs(vMag_step_);
if (astras!=NULL) delete astras; else astras=NULL;
if (caelestis!=NULL) delete caelestis; else caelestis=NULL;
if (dso!=NULL) delete dso; else dso = NULL;
/*setPermeability(w,c);*/}
//-------------------------
void gProfundusMundus::createUniverse(const profundus_mundus::ProfundusMundus &pm){
if (astras!=NULL) delete astras;
astras = new VertAstras(pm.getStars(),this->vMag_step);
if (caelestis!=NULL) delete caelestis;
caelestis = new VertCaelestis<CaelestisList>(pm.getCaelestis(),this->vMag_step);
dso = new VertCaelestis<profundus_mundus::DSOList>(pm.getDSOs(), this->vMag_step);
}

void gProfundusMundus::update(const profundus_mundus::ProfundusMundus &pm){
if (astras!=NULL) delete astras;
astras = new VertAstras(pm.getStars(),this->vMag_step);
if (caelestis!=NULL) delete caelestis;
caelestis = new VertCaelestis<CaelestisList>(pm.getCaelestis(),this->vMag_step);
dso = new VertCaelestis<profundus_mundus::DSOList>(pm.getDSOs(), this->vMag_step);
}

//-----------------------------------
void gProfundusMundus::createConstellations(const profundus_mundus::SidusList &sl){
this->sidusis = FastSidusis(sl);}

//----------------------
gProfundusMundus::~gProfundusMundus(){
delete this->astras;
delete this->caelestis;}

Vert_array::Vert_array(){
this->array=NULL;
this->length=0;}

Vert_array::Vert_array(GLdouble *va, unsigned long length){
this->array=va;
this->length=length;}
namespace create{
//---------------------
GLdouble* vert_array(const coordinates::CoordList &cl){
GLdouble *va = new GLdouble[cl.size()*3];
unsigned long i=0;GLdouble xyz[3];
for (coordinates::CoordList::const_iterator it=cl.begin(), end=cl.end(); it!=end; it++){
coordinates::Coordinate *c=*it;
coordinates::converting::SpherToCartesian(*c,xyz);
va[i++]=xyz[0];
va[i++]=xyz[1];
va[i++]=xyz[2];}
return va;}

//------------------------------------------- создаёт массив вершин звёзд и цветов звёзд
long fstar_array(const AstraList &stars, double minVmag, double maxVmag,GLdouble *s_arr, GLfloat *c_arr){
long stars_collected = 0;
int k=0;
Astrum *a;
double xyz[3];
for(AstraList::const_iterator it=stars.begin(), end=stars.end(); it!=end; it++){
a=*it;
if (a->vMag() < maxVmag && a->vMag() > minVmag){ stars_collected++;
coordinates::converting::SpherToCartesian(a->coordinate, xyz);
float* col = elementus_mundus::altus_mundus::spectral_class::getColorBySpecClass(a->spec_class);
memcpy(c_arr+k,col,3*sizeof(double));
memcpy(s_arr+k,xyz,3*sizeof(double));
k+=3*sizeof(double);
delete[] col;}}
return stars_collected;}
//------------------------------------------- создаёт массив цветов звёзд
long color_array(const AstraList &stars, double minVmag, double maxVmag, GLfloat *c_arr){
long stars_collected = 0;
int k=0;
Astrum *a;
for(AstraList::const_iterator it=stars.begin(), end=stars.end(); it!=end; it++){
a=*it;
if (a->vMag() < maxVmag && a->vMag() >= minVmag){
stars_collected++;
float* col = getColorBySpecClass(a->spec_class);
c_arr[k++] = col[0];
c_arr[k++] = col[1];
c_arr[k++] = col[2];
delete[] col;}}
return stars_collected;}
}

/*********** UNIVERSE ***********/
gMundus::gMundus(){
orbits=new vOrbitMap;
planets = new PlanetMap;}
//---------------
void gMundus::setPlanetMap(g_containers::PlanetMap *pl){this->gplanets=pl;}

void gMundus::updateOrbits(const planetary_system::PlanetList &pl,const QDateTime& epoch,const QDateTime& now){
this->orbits->clear();
this->orbits_lu=now;
//std::list<CoordList*>* ol=sistema_solare::make_orbits(pl,now,epoch);
orbits=make_vOrb_list(pl,epoch,now);
}
//----------------------
void gMundus::updateProfundusMundus(const profundus_mundus::ProfundusMundus &_pm,
                                            electum_necessarius::howIsee &w){
deep_space.update(_pm);
}
//-----------
QDateTime gMundus::getOrbitsLastUpdate() const{
return this->orbits_lu;}
}


