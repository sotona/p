﻿#ifndef VISUALIZATION_H
#define VISUALIZATION_H
#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include <FTGL/ftgl.h>
#include "../e2/e2.h"
#include "g_container.h"
#include "screen.h"
#include "../e2/e/coordinate.h"

/*
note: world coordinate unit is kilometer, but most astro objects use AU and parsec.
*/

bool initExtensions();

namespace visualization{
using observer::perceptionem::Perceptionem;
namespace aux{
//--------------
bool get2Dcoordinate(GLdouble* xy_z_2d,const GLdouble* xyz_3d);
bool get2Dcoordinate(GLdouble &x, GLint &y,const GLdouble* xyz);
//-------------- is this point visible (2d_x+dx and 3d_y+dy). check_z - checking z coordinate ?
bool is_visible(GLint* viewport, const double* xyz3d, int dx=0, int dy=0, bool check_z=0);
// this function also return xyz_2d/ xyz_2d must be double[3]
bool is_visible(GLint *viewport, const double *xyz3d, double *xyz_2d, int dx=0, int dy=0, bool check_z=0);
}

namespace elementus{
void quasiGround();
void Sphere(float r /*float pr, float corner,float color,float style*/);
void vert_array(const g_containers::Vert_array& va, GLenum mode = GL_POINTS);
void vert_arrays(const std::list<g_containers::Vert_array*> &vas, GLenum mode = GL_POINTS);
void vert_arrays(const std::vector<g_containers::Vert_array*> &vas, GLenum mode = GL_POINTS);
//-------------
void markers(const profundus_mundus::CaelestisList& cl);
void marker(const coordinates::Coordinate &c);
void text(FTGLPixmapFont &font,double coord[], char *text);
}

/*****************/
namespace init{
void planet_rotation(const elementus_mundus::proxima_mundus::OrbiterBody& p,
                 const location::geo_pos::GeoPos& gp, const tempus::VivusTempus& life_time);
namespace position{
void geopos(const location::Location& l);
void in_solar_system(const location::Location& location);}
void observer_look(coordinates::Coordinate);                //  установка взгляда наблюдателя
void observerGPHAlook(coordinates::Coordinate look,const location::geo_pos::GeoPos& gp, double ha); // т.с. только с учётом ГП - gp и премени суток - ha
void local_observers_center(const location::Location& location);
}

namespace imaginary_draw{

struct grid_stngs{
double col[3];
double width; 
unsigned parallels, meridians;
unsigned parts;
grid_stngs();};
// grids, etc
void grid(float r, int parallels=9*2, int meridians=24); //spherical grid
void equator(float r, unsigned parts = 24*2);
void meridian(float r, unsigned parts = 24*2);
void grid(float *color, float width, float radius,unsigned parallels,unsigned meridians);
void grid(FTGLPixmapFont &font, float *color, float width, float radius,unsigned parallels,unsigned meridians);
void ground(float r, unsigned parts = 24*2);
void draw_selection(float r, float line_width);
void select_smthng(const coordinates::Coordinate& c, float fov);
void draw_user_mark(const coordinates::Coordinate& c);

void ecliptic_disc(double start_r, double r=35, unsigned segments=36, double r_step=1); // рисует плоскость эклиптики. r - радиус в а.е.
//----------- labels
void ss_labels(FTGLPixmapFont &font,const planetary_system::PlanetList &pl,const electum_necessarius::whatISee &w,
                const elementus_mundus::ID_Count_type& excep, const screen::col_wid& cw);
//void ds_labels(FTGLPixmapFont &font,const profundus_mundus::ProfundusMundus& pm,
//                                            const observer::perceptionem::Perceptionem& p,const screen::col_wid& cw);
void caelestis_list(const profundus_mundus::CaelestisList &cl);
void caelestis(const elementus_mundus::altus_mundus::Caelestis& c);
void selected_mark(const elementus_mundus::BasicAstro& ba, FTGLPixmapFont& font, float* color); // маркерует выбранный объект
//-------------------
//template <class List> // T std::list of something, what inherited from Caelestis
//void caelestis_labels(const List& al, FTGLPixmapFont &font){
//GLint viewport[4]; glGetIntegerv(GL_VIEWPORT,viewport);// каждый раз ?
//double xyz[3], xyz_2d[3];
//for (typename List::const_iterator it = al.begin(), end = al.end(); it!=end; it++){
//const typename List::value_type a = *it;
//coordinates::converting::SpherToCartesian(a->coordinate, xyz);
//if (aux::is_visible(viewport,xyz,xyz_2d,10,0,1))
//font.Render(a->name(),strlen(a->name()), FTPoint(xyz_2d[0], xyz_2d[1]));
//}}

// addxy - добавить к пиксельным координатам

void label(FTGLPixmapFont &font,const elementus_mundus::BasicAstro& a, const screen::col_wid& cw, const int* addxy=NULL);

template <class AstroList>
void labels(FTGLPixmapFont &font,const AstroList& al, const Perceptionem& p,  const screen::col_wid& cw){
typename AstroList::value_type a;
GLint viewport[4]; glGetIntegerv(GL_VIEWPORT,viewport); // каждый раз ?
GLdouble coord_2d[3];
glPixelTransferf(GL_RED_BIAS,   cw.color[0]);
glPixelTransferf(GL_BLUE_BIAS,  cw.color[1]);
glPixelTransferf(GL_GREEN_BIAS, cw.color[2]);
double xyz[3];
for (typename AstroList::const_iterator it=al.begin(), end=al.end(); it!=end; it++){
a=*it;
if (a->vMag() <= p.how_i_see*p.labels_k){
coordinates::converting::SpherToCartesian(a->coordinate, xyz);
if (visualization::aux::get2Dcoordinate(coord_2d, xyz)){
if (coord_2d[2]<=1.)
if (coord_2d[0] >= 0-50 && coord_2d[0] <= viewport[2] && coord_2d[1] >=0 && coord_2d[1] <=viewport[3]){
const char *name = a->name();
if (name!=NULL) if (strlen(name))
font.Render(name, strlen(name),FTPoint(coord_2d[0],coord_2d[1]));}}}}} // wtf!?

template <class AstroList> void labels(FTGLPixmapFont &font,const AstroList& al, const screen::col_wid& cw){
typename AstroList::value_type a;
GLint viewport[4]; glGetIntegerv(GL_VIEWPORT,viewport); // каждый раз ?
GLdouble coord_2d[3];
glPixelTransferf(GL_RED_BIAS,   cw.color[0]);
glPixelTransferf(GL_BLUE_BIAS,  cw.color[1]);
glPixelTransferf(GL_GREEN_BIAS, cw.color[2]);
double xyz[3];
for (typename AstroList::const_iterator it=al.begin(), end=al.end(); it!=end; it++){
a=*it;
coordinates::converting::SpherToCartesian(a->coordinate, xyz);
if (visualization::aux::get2Dcoordinate(coord_2d, xyz)){
if (coord_2d[2]<=1.)
if (coord_2d[0] >= 0-50 && coord_2d[0] <= viewport[2] && coord_2d[1] >=0 && coord_2d[1] <=viewport[3]){
const char *name = a->name();
if (name!=NULL) if (strlen(name))
font.Render(name, strlen(name),FTPoint(coord_2d[0],coord_2d[1]));}}}}


} /* end of imaginary*/

/* рисует астрономические объекты */
namespace universe{
using namespace elementus_mundus;
using namespace elementus_mundus::altus_mundus::spectral_class;
using namespace electum_necessarius;

//-------------- яркость Одного Бритого Англичанина Финики Жевавшего Как Морковь
float getSizeByVisualMag(float vmag);
/********************************************************/

using namespace g_containers;

/*solar system*/
namespace solar_system{
void orbit(double* orbit, unsigned len);
void orbit(const vOrbit& o);
void orbits(const g_containers::vOrbitMap& om, const planetary_system::PlanetList& pl);
void planet(const Planet &p, const g_containers::Planet* graphic_planet);
void planet(const Planet &p);
unsigned planets(const g_containers::PlanetMap&, const planetary_system::PlanetList&, const whatISee&, ID_Count_type except);
unsigned planets(const planetary_system::PlanetList& pl, const whatISee& w, ID_Count_type except);
unsigned planet_points(const planetary_system::PlanetList& pl, ID_Count_type except);


template <class AL>
unsigned orbiter_points(const AL& pl, electum_necessarius::whatISee& w, ID_Count_type except){
glEnable(GL_POINT_SMOOTH);double xyz[3];
for (typename AL::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
if (/*сравнение с предельной видимой величиной*/ 0 || (*it)->absID()==except) continue;
coordinates::Coordinate c=(*it)->coordinate;
coordinates::converting::SpherToCartesian(c,xyz);
glColor3f(0.9,0.9,0.9); // background
glPointSize(10);glEnable(GL_POINT_SMOOTH);
glBegin(GL_POINTS);glVertex3dv(xyz);glEnd();
glPointSize(5);glColor3f(1,1,1); // set true color !!!
glBegin(GL_POINTS);glVertex3dv(xyz);glEnd();
}glDisable(GL_POINT_SMOOTH);
return 0;}

void comet(const proxima_mundus::Cometa &comet);
unsigned comets(const planetary_system::CometList& cl,const whatISee& w);
void asteroid_belt(/*some parameters*/);
void kuiper_belt();

void spacecraft();
void test_star(float polar_r, float equator_r,const universe::altus_mundus::spectral_class::SpectralClass& sc);
void star(float polar_r, float equator_r,const universe::altus_mundus::spectral_class::SpectralClass& sc);

void all(const E2& e2,  g_containers::gMundus &vc, const screen::plan_elements &pe);

}
/*depp sky*/
namespace deep_sky{
void asterizm(const profundus_mundus::Asterizm& A);
void asterism_list(const profundus_mundus::AsterismList& cl);
void draw_asterism_labels(const profundus_mundus::AsterismList& al, FTGLPixmapFont &font);
//int draw_astras(const storage::VertAstras &vc,const whatISee &w, unsigned mode);
int all(const profundus_mundus::ProfundusMundus &pm,
        const g_containers::gProfundusMundus &vc,const observer::perceptionem::Perceptionem &p,const screen::ScreenParameters& sp);
//int gMundus(const g_containers::gMundus &guniverse, const profundus_mundus::ProfundusMundus &universe, const whatISee &w);
void sky_texture(unsigned tex_id);
//---------------------------------------------------
template <class T>
int some_deep_sky(const T &stars,const float max_vmag){
unsigned stars_drawed =0;

glInitNames();glPushName(0);
for(typename T::const_iterator it=stars.begin(), end=stars.end(); it!=end; it++){
 typename  T::value_type /*const*/ a; a= *it;
if (a->vMag()>max_vmag) {continue;}else{ stars_drawed++;
double xyz[3];
coordinates::converting::SpherToCartesian(a->coordinate,xyz);
glLoadName(a->absID());
glBegin(GL_POINTS);
glVertex3dv(xyz);
glEnd();}}
return stars_drawed;}
}

}}
/**************************************************************/
namespace select_astro{
/***********************/
namespace draw{
void stars(const profundus_mundus::AstraList &stars,const electum_necessarius::howIsee &h);
void caelestis(const profundus_mundus::CaelestisList& cl,const electum_necessarius::howIsee &w);
//int profundus_mundus(const profundus_mundus::ProfundusMundus &c,/*const storage::gProfundusMundus &vc,*/const whatISee &w);
void planets(const planetary_system::PlanetList &pl,const electum_necessarius::howIsee &w);
// рисование тел солнечной системы. A должно быть std::list<потомок_OrbiterBody*>
template <class A>
void ss_orbiters(const A& orbiters,const electum_necessarius::howIsee &h, elementus_mundus::ID_Count_type nd=0){
    typename A::value_type p;
    glMatrixMode(GL_MODELVIEW);double xyz[3];
    for (typename A::const_iterator it=orbiters.begin(), end =orbiters.end();it!=end;it++){
    p=*it;
    glPushMatrix();
    coordinates::Coordinate c=p->coordinate;
    c.Range(c.Range());
    coordinates::converting::SpherToCartesian(c,xyz);
    glTranslatef(xyz[0],xyz[1],xyz[2]);

    glLoadName(p->absID());
    glColor3f(1,1,1);
    glPointSize(1.0);
    glBegin(GL_POINTS);
    glVertex3f(0,0,0);
    glEnd();
    visualization::elementus::Sphere(p->sphere.PolarR());
    glPopMatrix();
    }
}
//-------------------------
// p must be const!
void all(const AstroDB &adb, const location::Location& location, const QDateTime &t,
const observer::perceptionem::Perceptionem &p,elementus_mundus::ID_Count_type nd=0);
} /* draw */
} /* select */





#endif // VISUALIZATION_H
