﻿#ifndef G_CONTAINER_H
#define G_CONTAINER_H
#include <math.h>
#include <map>
#include <GL/gl.h>
#include <FTGL/ftgl.h>
#include "../e2/whatIsee.h"
#include "../e2/e/mundus.h"

const unsigned dorbs = 365;

//const float cra2=1./24.*M_PI*2.;
//const float cde2=1./90.*M_PI/2;
//#define cra coordinate.RightAscension()
//#define cde coordinate.Declination()
#define AU coordinates::astronomical_unit

namespace g_containers{

// планета как статичный объект.
class Planet{
private:
char *planet_name;
GLuint texture_id;
GLuint on_ground_tid;
public:
Planet(GLuint texture_id,const char *name_planet);
GLuint getTextId() const;};

typedef std::map<elementus_mundus::ID_Count_type,g_containers::Planet*> PlanetMap;
PlanetMap* initPlanetTextures(char *filename,const planetary_system::PlanetList& );

/**********************************************/
template <class T>
class VertCaelestis{//вертексно-цветастый массив.
public:
float min_vMag, max_vMag;
double step;
unsigned long arrays_count; //число элементов vert и color
unsigned long *arrays_len; //число эл-в в массивах vert[i] и color[i]
unsigned long in_all;
void setDefToMemb();
public:
GLdouble** vert; //массив вершин
VertCaelestis();
VertCaelestis(const T &cl, float vMag_step);
int arraysCount() const {return arrays_count;}
double minVMag() const {return min_vMag;}
double maxVMag() const {return max_vMag;}
double Step() const {return step;}
~VertCaelestis();
template <class X> friend int draw_caelestis(const VertCaelestis<T> &vc,const electum_necessarius::whatISee &w, unsigned mode);
};
/**********************************************/
//class VertAstras;
//int draw_astras(const VertAstras &vc,const whatISee &w, unsigned mode);

class VertAstras:public VertCaelestis<profundus_mundus::AstraList>{//вертексно-цветастый массив.
public:
GLfloat** color; //массив цветов звёзд.
GLfloat** bk_color; // массив свечений звёзд
public:
VertAstras();
VertAstras(const profundus_mundus::AstraList &cl, float vMag_step);
~VertAstras();
/*friend
int draw_astras(const storage::VertAstras &vc,const whatISee &w, unsigned mode);*/
};
/****************************/
class vOrbit{
public: double *orbit; //xyz массив точек орбиты
 long orb_segments_count; //длинна orbit
public:
 elementus_mundus::ID_Count_type planet_id;
 vOrbit(const coordinates::CoordList& orbit, long segments = dorbs, elementus_mundus::ID_Count_type planet_id_=0);
 //double* getOrbit() const;
 void setOrbit(double* o, long &len);
 long getOrbSegCount() const;
 ~vOrbit();
};

typedef std::map<elementus_mundus::ID_Count_type,vOrbit*> vOrbitMap;

double* make_vert_orbit(const coordinates::CoordList& orbit);
double* make_vert_orbit(const planetary_system::PlanetList& pl);
// orb_elmnts - число отрезков из ко-х строится орбита
//vOrbitMap* make_vOrb_list(std::list<elementus_mundus::CoordList*>* ol,unsigned orb_els_k);
vOrbitMap* make_vOrb_list(const planetary_system::PlanetList& pl,const QDateTime& epoch,const QDateTime& now);

struct Vert_array{
 GLdouble* array;
 unsigned long length;
 Vert_array();
 Vert_array(GLdouble*,unsigned long length);};

template <class List>
inline Vert_array make_vert_array(const List &stars){
Vert_array va;
va.length = make_vert_array<List>(stars);
return va;}

typedef std::vector<Vert_array*> VertArrayVector;

struct FastSidusis{
 elementus_mundus::ID_Count_type first_sid_id;                          // идентификатор первого созвездия созвездия
 VertArrayVector* constellation_borders;                                // массив массивов вершин границ созвездий
 coordinates::CoordVector* sidCenterVector;                              // вектор центров созвездий
 FastSidusis();
 FastSidusis(const profundus_mundus::SidusList& sl);
 ~FastSidusis();
};

/**********************************************/
class gProfundusMundus{//вертексно-цветастое небо
private:
 float vMag_step; // шаг блеска, который определяет число массивов вершин с заданым блеском
// electum_necessarius::howIsee his; // для чего ?
 VertAstras *astras;
 VertCaelestis<profundus_mundus::CaelestisList> *caelestis;
 VertCaelestis<profundus_mundus::DSOList> *dso;

 FastSidusis sidusis;  /*данные в массивах расположены в том же порядке, как и созвездия в своём списке*/

 // астеризмы. массив вершин.
public:
 GLuint *sky_texture;   // нужен вектор

 gProfundusMundus();
 gProfundusMundus(const profundus_mundus::ProfundusMundus &c, float vMag_step_);
 ~gProfundusMundus();
 void createUniverse(const profundus_mundus::ProfundusMundus& pm);
 void update(const profundus_mundus::ProfundusMundus& pm);
 void createConstellations(const profundus_mundus::SidusList& sl);
 const electum_necessarius::howIsee& getPermeability() const;
 /*get data*/
 const VertAstras& getAstras() const                      {return *astras;}
 const VertCaelestis<profundus_mundus::CaelestisList>& getCaelestis() const {return *caelestis;}
 const VertCaelestis<profundus_mundus::DSOList>& getODSs() const {return *dso;}
 const FastSidusis& getConstellations() const         {return sidusis;}
};
//-----------------------
namespace create{
GLdouble* vert_array(const coordinates::CoordList& cl);
//------------------------------------------- создаёт массив вершин звёзд и цветов звёзд
template <class T>
long vert_array(const T &stars, double minVmag, double maxVmag,GLdouble *s_arr){
long stars_collected = 0;
int k=0;double xyz[3];
for(typename T::const_iterator it=stars.begin(), end=stars.end(); it!=end; it++){
typename T::value_type c=*it;
if ((*it)->vMag() < maxVmag && (*it)->vMag() >= minVmag){
stars_collected++;
coordinates::converting::SpherToCartesian(c->coordinate,xyz);
s_arr[k++]=xyz[0];
s_arr[k++]=xyz[1];
s_arr[k++]=xyz[2];}}
return stars_collected;}
//----------------------
template <class T>
long vert_array(const T &stars,GLdouble *s_arr){
long stars_collected = 0;
int k=0;
for(typename T::const_iterator it=stars.begin(), end=stars.end(); it!=end; it++){
typename T::value_type c=*it;
stars_collected++;
double *xyz = coordinates::converting::SpherToCartesian(c->coordinate);
s_arr[k++]=xyz[0];
s_arr[k++]=xyz[1];
s_arr[k++]=xyz[2];
delete[] xyz;}
return stars_collected;}
//----------------------
long color_array(const profundus_mundus::AstraList &stars, double minVmag, double maxVmag, GLfloat *c_arr);
//----------------------
}
/*************/
class gMundus{
public:
/*graphic planet*/
gProfundusMundus deep_space;
vOrbitMap *orbits;
PlanetMap *planets; //планеты с текстурами
g_containers::PlanetMap *gplanets;

private:
QDateTime orbits_lu; //lu = last update
QDateTime am_lu;

private:
void update();
public:
gMundus();
void setPlanetMap(g_containers::PlanetMap* pl);
void updateOrbits(const planetary_system::PlanetList &pl,const QDateTime& epoch,const QDateTime& now);
void updateProfundusMundus(const profundus_mundus::ProfundusMundus &_pm,
                                            electum_necessarius::howIsee &w);
QDateTime getOrbitsLastUpdate() const;
};

}

#endif // G_CONTAINER_H
