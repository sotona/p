﻿/*Only what my eyes will see, I will believe!*/
#ifndef WHATISEE_H
#define WHATISEE_H

#include <bitset>

namespace electum_necessarius{
/* вспомогательные элементы графики */
namespace imagination{
namespace grid{ // сетка
const unsigned char az       =      0;
const unsigned char eq       =      1;
const unsigned char ec       =      2;}
const unsigned char asterism      = 3;
const unsigned char orbit         = 4;
const unsigned char orbit_features =5; // доп. графика для орбит - точки апоцентра и перицентра и т.д.
const unsigned char c_borders     = 6; //constellation borders
const unsigned char ecliptic_disc = 7;
const unsigned char planet_axis =   8;    // ось вращения планеты
}
/* что может видеть наблюдатель во вселенной. индексы для whatISee::universe и labels */
namespace universe{
/*deep sky*/
namespace ds{
const unsigned char astra =         17;
const unsigned char OC =            18;
const unsigned char GC =            19;
const unsigned char nebula =        20;
const unsigned char galaxy =        21;
const unsigned char caelestis =     22;
const unsigned char constellation = 23;
const unsigned char dso =           24;
//const unsigned char asterism      = 25;
//const unsigned char c_borders     = 26;

const unsigned char ds_image      = 27;   //optical texture of deepsky
//const unsigned char universe_IR = 10;        //infrared texture of deepsky
//const unsigned char universe_relict = 11;    //relict texture of deepsky

/* остальные числа зарезервированы */}

const unsigned char ground        = 28;

/*solar system*/
namespace ps{
const char planet =                 33;
const char comet =                  34;
const char asteroids =              35;
const char sattelite =              36;
}
}

typedef std::bitset<48> bitset48;

const float what_eye=6.0; //примерная максимальная видимая величина воспринимаемая нормальными 2х6мм

struct whatISee{
    bitset48 things;      // реальные объекты вселенной
    bitset48 labels;        // подписи для real. индесы такие же.
//    bitset48 things;   // вспомогательные абстракции. орбиты, созвездия...
    whatISee();
    whatISee(bitset48 _universe, bitset48 _labels, bitset48 _imagination);

    bool isUvisible(unsigned char) const;       // universe
    bool isLvisible(unsigned char) const;       // labels
    bool isIvisible(unsigned char) const;       // imagination
};

/* предельные видимые величины для астрообъектов и подписей к ним. */
struct howIsee{ //maximum of visual magnitudes
    float k;
    float astra;
    float oc;
    float gc;
    float galaxy;
    float nebula;
    float caelestis;
    float planet;
    float comet;
    howIsee(float _astra = what_eye, float _nebula = what_eye,
        float _galaxy = what_eye, float _oc = what_eye, float _gc = what_eye){
        astra = _astra, nebula = _nebula, galaxy = _galaxy, oc = _oc, gc = _gc;
        caelestis=what_eye; planet=what_eye; k=1;}
};


}

#endif // WHATISEE_H
