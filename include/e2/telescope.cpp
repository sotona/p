﻿#include <string.h>
#include "telescope.h"

optical_system::optical_system(float d_, float f_, const char *xx):d(d_),f(f_){}

optical_system::optical_system(){
d=0; f=0; name=NULL;}

optical_system::~optical_system(){
delete[] name;}

const optical_system& telescope_etc::getOcular() const{
return this->y[this->o_index];}

const optical_system& telescope_etc::getTelescope() const{
return this->t[this->t_index];}

float out_fow(const optical_system &t, const optical_system &y){
return y.d*y.f/t.f;}

namespace info{
namespace param{
namespace telescope{
const std::string telescope_name = "telescope";
const std::string t_focal_length = "tel.focal length";
const std::string aperture_d = "apperture diametr";
const std::string out_fov = "out FoV";
const std::string y_focal_lengt = "yey pice focal_length";
const std::string yey_piece_name = "yey piece";
}}
}
