﻿#include <math.h>
#include <GL/gl.h>
#include "oculus.h"

namespace oculus{
Oculus::Oculus(float azimuth, float height, float fov){
this->views_wideness = fov;
this->where_i_see = Coordinate(azimuth,height,1,4);
focused=0;
this->focus_coord=NULL;}

Oculus::~Oculus(){}
//----------
float Oculus::Zoom(float x){
if (fabs(x)>=zero){
if (views_wideness/x >= d_min_fov && views_wideness/x <= d_max_fov)
this->views_wideness /= x;}
return views_wideness;}
//------------
void Oculus::setFoV(float x){
if (x<120.) this->views_wideness = fabs(x);}
//---------
float Oculus::Up(float grad){
if (fabs(grad)>zero){ focused=0;
grad*=(views_wideness/d_max_fov);
if (fabs(where_i_see.Height()+grad)<90.-zero) where_i_see.setHeight(where_i_see.Height()+grad); /*работает?*/}
else if (focused) return focus_coord->Sigma();
return where_i_see.Height();}
//---------
float Oculus::Left(float grad){
if (fabs(grad)>=zero){ focused=0;
grad*=(views_wideness/d_max_fov);
where_i_see.setAzimuth(where_i_see.Azimuth()+grad);}
else if (focused) return focus_coord->Alpha();
return where_i_see.Azimuth();}
//------
void Oculus::whereIsee(const Coordinate &c){
focused=0;
this->where_i_see=c;}
//-------
Coordinate Oculus::whereIsee() const{
if (!focused) return where_i_see;
else return *focus_coord;}
//-------
void Oculus::setFocusedCoord(const Coordinate *coord){
focus_coord=coord;}
//------
const Coordinate* Oculus::getFocusedCoord() const{
return this->focus_coord;}
//------
bool Oculus::toFocus(const Coordinate *coord){
if (coord!=NULL) {this->focus_coord=coord;
Coordinate c=*coord;
this->where_i_see=c;
focused=1; return 1;} else
if (this->focus_coord!=NULL){
Coordinate c=*this->focus_coord;
this->where_i_see=c;
focused=1; return 1;} else return 0;}
//-------
}



//may be usefull for oculus
/*Oculus::Oculus(float fovy, float aspect, float *_rotation, float znear, float zfar):
                                                fov_y(fovy), aspectRatio(aspect),znear(znear), zfar(zfar){
rotation=new float[3];
if (_rotation==NULL) for (int i=0; i<3; i++, rotation[i]=0);
else for (int i=0; i<3; i++)
                rotation[i]=_rotation[i];

glRotatef(rotation[0],1.0,0,0);
glRotatef(rotation[1],0,1.0,0);
glRotatef(rotation[2],51,0,0,1.0);

max_fov_y=90.; min_fov_y=30.;}
//--------------------------------------------------------------------------
void Oculus::Rotate(float x, float y, float z){
glRotatef(rotation[0],1,0,0);
glRotatef(rotation[1],0,1,0);
glRotatef(rotation[2],0,0,1);

glRotatef(x,0,1,0);
glRotatef(y,1*cos(x/180*M_PI),0,1.0*sin(x/180*M_PI));}
//--------------------------------------------------------------------------
float Oculus::Zoom(float zoom){
if (fov_y/zoom<min_fov_y || fov_y/zoom>max_fov_y) return fov_y;
zoom=abs(zoom);
fov_y/=zoom;glMatrixMode(GL_PROJECTION);glLoadIdentity();
//gluPerspective(fov_y, aspectRatio, znear, zfar);
return fov_y;}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
void Oculus::Rotate0(float x, float y, float z){
 rotation[0]=x;
 rotation[1]=y;
 rotation[2]=z;
}*/
