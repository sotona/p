﻿#ifndef E2_H
#define E2_H

#include "e/pModel.h"
#include "observer.h"
#include "e/astro_db.h"
#include "location.h"
#include "e/catalogues.h"

struct E2{
u_model::uModel * umodel;
catalogues::Catalogues *cat;
AstroDB *adb;
observer::Observer *Galileo_Galilei;
location::Location * location;};

#endif // E2_H
