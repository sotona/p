﻿#ifndef LOCATION_H
#define LOCATION_H

#include "e/elementus_mundus.h"
#include <math.h>

namespace info{
namespace param{
namespace geo_pos{
extern const std::string latitude;
extern const std::string longitude;
extern const std::string height;}}}

namespace location{
using elementus_mundus::proxima_mundus::Planet;
namespace geo_pos{
/*default latitude longicude and height*/
const float d_lat = 51.25;
const float d_lon = 110.25;
const float d_hei = 300.;

/*************************/
class GeoPos{
private:
        float latitude;     // почему бы это не заменить на Coordinate
	float longitude;
	
	float height; //высота над уровнем моря
        char *location; //название
	char *info;
public:
	GeoPos(float lat=d_lat, float lon=d_lon, float hei=d_hei):latitude(lat), longitude(lon), height(hei) {}
        const float Latitude() const {return latitude;}
        const float Longitude() const {return longitude;}
        const float Height() const {return height;}
        float TimeZone() const; //истинный часовой пояс
        void setLatitude(float x)                           {latitude=x;}
        void setLongutude(float x)                          {longitude=x;}
        void setHeight(float x)                             {height=x;}

};/*************************/
}

class Location{
const elementus_mundus::BasicAstro* planet;     //указатель на активную(передвигающиюся) планету
                                        // если NULL то использовать Солнце
public:
geo_pos::GeoPos gp;
coordinates::Coordinate space_pos;
bool on_ground;                     //наблюдатель на планете
Location(const Planet* planet_=NULL);
const elementus_mundus::BasicAstro* getPlanet() const;
void setPlanet(const elementus_mundus::BasicAstro* p);
};

// return distace in km
double distance(const Location& l, const elementus_mundus::BasicAstro* ba);
//disatce in km. return in degree
float arc_diametr(float distance,const elementus_mundus::BasicAstro* ba);

inline float arc_diametr(const Location& l, const elementus_mundus::BasicAstro *ba){
return arc_diametr(distance(l,ba), ba);}
} /*end of location*/

//возвращает наблюдаемое зенитное расстояние вместо z - истинного
float refraction(float z);

//возвращает наблюдаемое прямое восхождение исходя из истинного ra и широты
float RArefraction(float ra,float latitude);

float Declrefration(float decl, float latitude);

//возвращает величину атмосферного ослабления. z - зенитное расстояние. при z>85 возвращает 0
float atmosphericWeaken(float z);



#endif

