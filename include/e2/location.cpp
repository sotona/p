#include "location.h"

namespace info{
namespace param{
namespace geo_pos{
const std::string latitude = "latitude";
const std::string longitude = "longitude";
const std::string height = "height";}
}}

namespace location{
namespace geo_pos{

float GeoPos::TimeZone() const{
return Longitude()/15.;}
}
//------------------------------
Location::Location(const Planet *planet_){
planet=planet_;
on_ground=1;}
//-------------
const elementus_mundus::BasicAstro* Location::getPlanet() const{
//if (planet==NULL) {
////Planet *p =new Planet();
////coordinates::Coordinate c(0,0,0,coordinates::type::ecliptic);       // ���� ������� ��� ��������� ������ �� ���������. ��������� ��
////p->coordinate=c;
////p->setName("Sun");
//return p;}
return this->planet;}
//-----------------
void Location::setPlanet(const elementus_mundus::BasicAstro *p){
this->planet=p;}

//----------------------------------------------
float refraction(float z){
return z;}
//------------------------------------------------
float RArefraction(float ra, float latitude){
return ra;}
//-------------------------------------------------
float Declrefration(float decl, float latitude){
return decl;}
//---------------------------------------------------
float atmosphericWeaken(float z){
if (z<85) return 0.2*cos(z);
else return 0;}

// return distace in km
double distance(const Location& l, const elementus_mundus::BasicAstro* ba){ // :(
using namespace coordinates::converting;
double d;
if (l.on_ground) d = coordinates::distance(l.getPlanet()->coordinate, ba->coordinate);
    // ���� ����� � ������ ���������
else {if (l.getPlanet()->coordinate.Range()<0.001) d = coordinates::distance(l.space_pos, ba->coordinate);
else{double x1[3], x2[3], x3[3];
SpherToCartesian(l.getPlanet()->coordinate,x1);
SpherToCartesian(l.space_pos,x2);
//x1[0]=-x1[0];x1[2]=-x1[2];x1[1]=-x1[1];
coordinates::addition(x1,x2);
SpherToCartesian(ba->coordinate,x3);
d = coordinates::distance(x2,x3);}
}return d;}

//disatce in km. return in degree
float arc_diametr(float distance,const elementus_mundus::BasicAstro* ba){
if (ba->coordinate.Type()==coordinates::type::ecliptic) distance/=coordinates::astronomical_unit;
else distance/=coordinates::parsec;
distance*=coordinates::astronomical_unit;
return 2*asin(elementus_mundus::radius(ba)/distance)*coordinates::converting::toDeg;}

}
