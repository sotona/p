﻿#ifndef TELESCOPE_H
#define TELESCOPE_H
#include <string>
#include "e/coordinate.h"
/*все величины в милиметрах */


struct optical_system{
    float d;        // апертура. D для телескопа. 2w для окуляра.
    float f;        // фокальное растояние
    char* name;     // название

    optical_system();
    optical_system(float d_, float f_, const char* name="");
    ~optical_system();};

inline float magnification(float f_t, float f_y) {return f_t/f_y;}
inline float magnification(const optical_system& t, const optical_system& y) {return t.f/y.f;}
float out_fow(const optical_system& t, const optical_system& y);


struct telescope_etc{
std::vector<optical_system> t;
std::vector<optical_system> y;
unsigned char o_index;          // использовать окуляр номер
unsigned char t_index;          // использовать телескоп номер
const optical_system&  getTelescope() const;
const optical_system&  getOcular() const;
};

namespace info{
namespace param{
namespace telescope{
extern const std::string telescope_name;
extern const std::string t_focal_length;    // фокусное расстояние телескопа
extern const std::string aperture_d;        // диаметр входного отверстия телескопа)
extern const std::string out_fov;           // диаметр вых. зрачок
extern const std::string y_focal_length;    // фокусное расстояние окуляра
extern const std::string yey_piece_name;}}
}


#endif // TELESCOPE_H
