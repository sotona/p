﻿#ifndef OCULUS_H
#define OCULUS_H

#include <stdlib.h>
//#include "whatIsee.h"
#include "e/elementus_mundus.h"

#define zero 0.0001

namespace oculus{
//using namespace electum_necessarius;
const float d_fov = 60.0;
const float d_min_fov = .03; //min fov of my telescope
const float d_max_fov = 120; //average fov of human

using namespace coordinates;
//class eye
class Oculus {
private:
bool focused;
Coordinate up;          //напрввление вверх
const Coordinate *focus_coord; //координаты маркерованого объекта
float views_wideness; //широта взгляда в градусах

public:
Coordinate where_i_see;

Oculus(float azimuth = 0,float height = 0, float fov = d_fov);
~Oculus();
float Zoom(float x = 0.0); //zoom in x times. return fov
void setFoV(float x);
float getFoV() const {return views_wideness;}
float Up(float grad = 0.0); //raise oсulus up on grad degree. return height
float Left(float grad = 0.0); //turn on the left grad degree
void whereIsee(const Coordinate& c);
Coordinate whereIsee() const;

void setFocusedCoord(const Coordinate* coord);
const Coordinate* getFocusedCoord() const;
bool toFocus(const Coordinate* coord=NULL);
bool isFocused() const {return focused;}
};}

#endif // OCULUS_H

/*
NOTE
MAY BE add smthng abt projection type
*/
