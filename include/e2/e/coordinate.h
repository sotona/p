﻿#ifndef COORDINATE_H
#define COORDINATE_H
/*
 все линейные расстояния хранятся в километрах
 все угловые либо в часах либо в градусах

*/

#include <math.h>
#include <string.h>
#include <string>
#include <list>
#include <vector>
namespace info{
namespace param{
namespace coord{
extern const std::string ecl_range_units;
extern const std::string eq_range_units;
extern const std::string range;
extern const std::string ra;
extern const std::string declination;
extern const std::string ecl_long;
extern const std::string ecl_lat;}
}
}

/******************* ALL ABOUT COORDINATES ********************/
namespace coordinates{
const double very_far =1.0E20/7;
const double light_year = 9460730472580.82;     //km
const double parsec = 30.857E12;                //km
const double astronomical_unit = 149597870.66;  //km

//типы кординат
namespace type{
const char equator=1;
const char galactic=2;
const char ecliptic=3;
const char horizontal=4;/*az*/}

namespace converting{
const double toDeg = 180./M_PI;
const double toRad = M_PI/180.;
//прямое восхождение -> галактическая долгота
double RAToGalacticLong(double ra);

//склонение -> галактическая широта
double DeclToGalacticLat(double de);

//галактическая долгота -> прямое восхождение
double GalacticLongitudeToRA(double lon);

//галактическая широта -> склонение
double GalacticLatitudeToDecl(double lat);

double HorMinSecToFloat(double hour, double min=0,double sec=0);
//double HorMinSecToFloat(char* hms);

//double HorMinSecToFloat(char* hms);
double HMStoFloat(const char* hms);

//преобразует часы_и_десятичные_доли_часа в ч:мин:сек:мсек. int[4]
int* floatHourToHMS(double x);

//x[4]=h,m,s,ms. return h.m.s.
//is_hour return xx 'h', xx 'm', xx, 's'
//else return xx° xx' xx"
char* HMStochar(int* x, bool is_hour=1);
char* HMStochar(double x,bool is_hour=1);
//равнопромежуточная проекция
double* inEquidistant(double lat,double lon,double lat0,double lon0);
double* fromEquidistant(double x,double y,double lat0,double lon0);
}

//double EclipAnglEquator(const QDate &time);

/*float RAToGalacticLong(float ra);  //прямое восхождение -> галактическая долгота
float DeclToGalacticLat(float de); //склонение -> галактическая широта

float GalacticLongitudeToRA(float lon);  //галактическая долгота -> прямое восхождение
float GalacticLatitudeToDecl(float lat); //галактическая широта -> склонение*/


/*************************************************/
//сферические координаты
class Coordinate{
private:
double alpha; //первая координата - аналог долготы
double sigma; //вторая координаты - аналог широты
unsigned char type; //тип координат: экваториальные, галактические.
double r; //радиус предпологается что при эклиптических - а.е.
                                        // экваториальных - ..
                                        // галлактических - ..
                                        // азимутальных - ..
                  //остальные - эклиптические и горизонтальные,
                  //реализовать при необходимости
public:
Coordinate(const Coordinate &c);
Coordinate(int type);
Coordinate(double a,double s, double r=0, int type=type::equator);
Coordinate(): alpha(0.0), sigma(0.0), type(type::equator), r(0.0){}

char Type() const {return type;}
void setType(char type_) {if (type_<5) type=type_;} // число типов координат - 4;
//get. экваториальные координаты
double Declination() const; //возвращает склонение в градусах
double RightAscension() const;//возвр. прямое восхождение в часах
//галактические
double GalLongitude() const; //долгота
double GalLatitude() const; //широта
//эклиптические
double EclipticLongitude() const; /*{if (type==type::ecliptic) return}*/
double EclipticLatitude() const;
//азимутальные (горизонтальные)
double Azimuth(double latitude=.0) const;
double Height(double latitude=.0, double LST=.0) const;
//set. экваториальные
void Declination(double d);
void RightAscension(double ra);
//галактические
void GalLongitude(double lon); //долгота
void GalLatitude(double lat); //широта
//эклиптические
void EclipticLongitude(double lon);
void EclipticLatitude(double lat);
//азимутальные (горизонтальные)
void setAzimuth(double az, double latitude=0);
void setHeight(double lat, double latitude=0);
//-----------------
double Alpha() const {return alpha;}        // аналог долготы
double Sigma() const {return sigma;}        // аналог широты

//типа долгота
void Alpha(double a);

//типа широта
void Sigma(double s);

double Range() const {return r;}
void Range(double _r) {this->r=fabs(_r);}

std::string* coordNames() const; // return array[2] coordinat's name.
std::string* toString() const; // return array[3]  = {sigma, alpha, r}
std::string geRangeUnits() const;

};

namespace converting{
//сферические координаты в декартовы. k - коэф-т для длинны радиуса.
//double* SpherToCartesian(const Coordinate &c);
//double* SpherToCartesian(const Coordinate &c, double k, double* xyz);
double* SpherToCartesian(const Coordinate &c, double* xyz);
Coordinate CartesianToSpher(const double* xyz);}

typedef std::list<Coordinate*> CoordList;
typedef std::vector<Coordinate*> CoordVector;

/*return distance between two points*/
double distance(const Coordinate& c1, const Coordinate& c2);
double distance(const double *xyz1, const double *xyz2);
double* addition(const double *xyz, double *res_xyz);
void mid_value(const CoordList& cl, Coordinate& mid_point);
}

#endif // COORDINATE_H
