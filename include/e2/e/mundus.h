﻿#pragma once
#include <math.h>

#include "elementus_mundus.h"
typedef std::list<elementus_mundus::BasicAstro*> BasicAstroList;
/* дальний космос */
namespace profundus_mundus{

using elementus_mundus::altus_mundus::sidus::Sidus;
using elementus_mundus::altus_mundus::Caelestis;
using coordinates::CoordList;
typedef std::list<elementus_mundus::altus_mundus::Caelestis*> CaelestisList;
typedef std::list<elementus_mundus::altus_mundus::Astrum*> AstraList;
typedef std::list<elementus_mundus::altus_mundus::dso::DSO*> DSOList;
typedef std::list<elementus_mundus::altus_mundus::sidus::Sidus*> SidusList;

//--выдаёт самый яркий объект для списка небесных объектов
template <class AL>
const typename AL::value_type maxVmag(const AL& al){
const typename AL::value_type a;
for (typename AL::const_iterator it=al.begin(), end=al.end(); it!=end; it++){;
//if ((*it)->vMag()>)
}
}

Caelestis* get_max_vmag(CaelestisList* lst);
void get_min_mag(double &min, double &max);
//typedef std::vector<BasicAstroList*> BasicAstroListsVector;
typedef std::vector<AstraList*> AstraListsVector;
typedef std::list<CoordList*> ListOfCoordList;
/******************/
class Asterizm: public elementus_mundus::BasicAstro{
private:
float line_width;
float color[3];
coordinates::Coordinate center;
ListOfCoordList lines;
public:
AstraListsVector pattern;
Asterizm();
Asterizm(const char* _name,float _line_width,float r,float g,float b);
~Asterizm();
void Line_width(float _line_width);
void setColor(float r,float g,float b);
void setColor(const float *rgb) {memcpy(color, rgb, 3*sizeof(float));}
void setLines(const std::list<CoordList*>& _lines);
void setCenter(const coordinates::Coordinate& c);
const float Line_width()const{return line_width;}
const float* getColor()const{return color;}
const ListOfCoordList& getLines()const{return lines;}
coordinates::Coordinate getCenter() const;
};

typedef std::list<Asterizm*> AsterismList;
/************************************/
class ProfundusMundus{
public:                                 // public - это временно.
SidusList *sidusis;		//созвездия (имя+граница. без контуров)
AstraList *astras;       		//звёзды
CaelestisList *caelestis;                //астрообъекты без категории
AsterismList *asterisms;
DSOList *dso;                            /*Dubai Silicon Oasis*/
void init_lists();

QDateTime last_update;

public:
 ProfundusMundus(ProfundusMundus &c);
 ProfundusMundus();

 bool update(const QDateTime& d); // update positions of astroobjects

 void setAsterisms(AsterismList *al);
 void setConstellations(SidusList* s);
 void setCaelestis(CaelestisList* cl);
 void setStars(AstraList* al);
 void setDSO(DSOList* dso);

 const AsterismList&    getAsterisms() const;
 const SidusList&       getConstellations() const;
 const Sidus*           getConstellation(const char* sidus_name) const;
 const CaelestisList&   getCaelestis() const;
 const AstraList&       getStars() const;
 const DSOList&         getDSOs() const;
 //template <class T> const T& getSomeCaelestis();
};

/*template <class T>
unsigned how_much(const T &l, double &min_vMag, double &max_vMag);*/
//------------------------------сколько звёзд в диапозоне блеска
template <class T>
unsigned how_much(const T &l, double min_vMag, double max_vMag){
//T *a;
unsigned hm = 0;
for(typename T::const_iterator it=l.begin(), end=l.end(); it!=end; it++){
//a=*it;
if ((*it)->vMag()<max_vMag && (*it)->vMag()>=min_vMag) hm++;}
return hm;}

}
    
/* солнечная сичтема */
namespace planetary_system{
typedef std::list<elementus_mundus::proxima_mundus::Planet*> PlanetList;
typedef std::list<elementus_mundus::proxima_mundus::Cometa*> CometList;
coordinates::CoordList* make_orbit(const elementus_mundus::proxima_mundus::Planet& p,const QDateTime cur,const QDateTime &epoch, unsigned segments);
coordinates::CoordList* make_orbitl(const elementus_mundus::proxima_mundus::Planet& p,const QDateTime cur,const QDateTime &epoch, unsigned segments);
std::list<coordinates::CoordList*>* make_orbits(const PlanetList& pl,const QDateTime &ct,const QDateTime& epoch);

coordinates::CoordList* make_orbit(const elementus_mundus::proxima_mundus::Cometa& comet,const QDateTime cur,const QDateTime &epoch, unsigned segments);
std::list<coordinates::CoordList*>* make_orbits(const CometList& cl,const QDateTime &ct,const QDateTime& epoch);

/* */
class PlanetarySystem{
private:                 // это временно. до того как появится метод update;
    elementus_mundus::altus_mundus::Astrum* star;
    planetary_system::PlanetList *planets;
    planetary_system::CometList *comets;
    planetary_system::PlanetList* asteroid;
public:
    PlanetarySystem(planetary_system::PlanetList *planets_=NULL, planetary_system::CometList *comets_=NULL,elementus_mundus::altus_mundus::Astrum *star_=NULL);
    void setPlanets(planetary_system::PlanetList *p);
    void setComets(planetary_system::CometList *c);
    void setStar(elementus_mundus::altus_mundus::Astrum *star_);
    const planetary_system::PlanetList& getPlanets() const;
    const planetary_system::CometList& getComets() const;
    const elementus_mundus::altus_mundus::Astrum* getStar()const;

    void update(const QDateTime& d);    // пересчёт координат под текушию дату
    void update_planets(const QDateTime& d);
    void update_comets(const QDateTime& d);};

}


