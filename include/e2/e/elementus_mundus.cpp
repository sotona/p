﻿#include "elementus_mundus.h"

#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>

/* объеденить в одиин заголовочник */
//#include "../../calculation/APC_Const.h"
//#include "../../calculation/APC_Kepler.h"
//#include "../../calculation/APC_Math.h"
//#include "../../calculation/APC_Planets.h"
//#include "../../calculation/APC_PrecNut.h"
//#include "../../calculation/APC_Spheric.h"
//#include "../../calculation/APC_Sun.h"
//#include "../../calculation/APC_Time.h"
//#include "../../calculation/APC_VecMat3D.h"
//#include "../../calculation/APC_Moon.h"
//#include "../../calculation/APC_DE.h"


#ifdef __GNUC__  // GNU C++ adaptation
#include <ctype.h>
//#include "../../calculation/GNU_iomanip.h"
#endif

using namespace std;

namespace info{
// не соот-т параметрам в Catalogues
namespace param{
const std::string type = "type";
/*basic astro*/
const std::string name = "name";
const std::string note = "note";
/*caelestis*/
const std::string vmag = "v.mag";
const std::string ids = "ids"; // список, через запятую идентификаторов объекта в каталогах
/*Astrum*/
const std::string spec_class = "spectrall class";
/*Sidus*/
const std::string abvr = "abbreveration"; // abbreveration for sidus
/*Planet*/
const std::string tp = "circulation period";
const std::string polar_radius = "polar radius";
const std::string rotation_period = "rotation period";
/*other*/
const std::string distance = "distance";
const std::string arc_daimetr = "arc daiameter";
}} // end of info

// установить значение строковой переменной
inline char* setCharAttr(char* param,const char *value){
if (param) delete[] param;
if (value!=NULL && strlen(value))param=strdup(value);
else param=strdup(" ");
return param;}

/*basic universe*/
namespace elementus_mundus{
using namespace coordinates;
/*basic astroobject*/
BasicAstro::BasicAstro(unsigned long id, const char *_name, const char *_comment){
this->name_ = NULL;
this->comment_ = NULL;
absolute_id=id;
setName(_name);}
//-----------
BasicAstro::BasicAstro(unsigned long id,Coordinate &c, const char *_name, const char *_comment){
this->name_ = NULL;
this->comment_ = NULL;
absolute_id=id;
coordinate=c;
setName(_name);
setNote(_comment);}
//-----------
BasicAstro::~BasicAstro(){
if (name_) delete[] name_;
if (comment_) delete[] comment_;}
//---------
void BasicAstro::setName(const char *_name){
this->name_=setCharAttr(name_,_name);}
//--------
void BasicAstro::setNote(const char *_info){
setCharAttr(comment_, _info);}
//-------
void BasicAstro::setAbsId(unsigned long id){
absolute_id=id;}
//-----

const char* BasicAstro::name() const{return name_;}
const char* BasicAstro::note() const {return comment_;}
unsigned long BasicAstro::absID() const {return absolute_id;}
//--------
info::ParamValues BasicAstro::getInfo() const{
using namespace coordinates::converting;
info::ParamValues i;
i[info::param::name] = name();
std::string* coordinate = this->coordinate.toString();
std::string* coordinate_name = this->coordinate.coordNames();
for (unsigned short j = 0; j<3; j++) i[coordinate_name[j]] = coordinate[j];
delete[] coordinate;
delete[] coordinate_name;
//i[info::param::note] = note();
return i;}
//--------------
char BasicAstro::getType() const{
return astro_types::basic_astro;}

/*Astro Body*/

Sphere::Sphere(double r, double f){
setPolarR(r);
setFlattering(f);}

double Sphere::PolarR() const{
return this->polar_r;}
void Sphere::setPolarR(double r){
if (r>0) this->polar_r = r;}
double Sphere::Flattering() const{
return flattening;}
void Sphere::setFlattering(double f){
if (f>0) flattening=f;}

float radius(const elementus_mundus::BasicAstro *ba){
const altus_mundus::Astrum* a = dynamic_cast<const altus_mundus::Astrum*>(ba);
if (a) return a->sphere.PolarR();
else {const proxima_mundus::OrbiterBody* ob = dynamic_cast<const proxima_mundus::OrbiterBody*>(ba);
if(ob) return ob->sphere.PolarR();}
return -1;}

/*Deeep space objects*/
const QDateTime d_epoch = QDateTime(QDate(2000,0,0));   // эпоха по умолчанию
/* дальний космос */
namespace altus_mundus{
/******************** CAELESTIS **************************/
//----------------------------------------------------------
Caelestis::Caelestis():BasicAstro(){
vmag=6;id=NULL;proper_motion=NULL;}

Caelestis::Caelestis(Coordinate &c, float v_mag,const char* _name):BasicAstro(0,c,_name){
vMag(v_mag);
this->id=NULL;
proper_motion=NULL;}
//--------------------
Caelestis::Caelestis(ID_Count_type id,Coordinate &c, float v_mag,const char* _name):BasicAstro(id,c,_name){
this->id=NULL; proper_motion=NULL;
vMag(v_mag);}
/********************* SET METHODS  ***********************/
//----------------------------------------------------------
void Caelestis::vMag(float vm){
vmag=vm;}
//-----------------
void Caelestis::setProperMotion(const Coordinate& c){
proper_motion = new Coordinate(c);}
//--------------------
void Caelestis::setCatIDs(Cats_Ids* cid){
if (cid==NULL){if (id!=NULL) delete id; id=NULL;}
this->id=cid;}
/*********************** GET METHODS **********************/
//----------------------------
Coordinate Caelestis::getProperMotion() const{
return *proper_motion;}
//---------------
float Caelestis::vMag() const{
return vmag;}
//--------------
unsigned long Caelestis::getIDin(const char *catalogue) const{
if (id==NULL) return 0;
unsigned long r_id=0;
for (unsigned i=0; i<id->size(); i++){
const Cat_Id cat_id = id->at(i);
 if (!strcmp(cat_id.first,catalogue)){ r_id=cat_id.second; break;}}
return r_id;}
//-------
info::ParamValues Caelestis::getInfo() const{
info::ParamValues i = BasicAstro::getInfo();
char vm[8];
sprintf(vm,"%3.2f",vMag());
i[info::param::vmag]=vm;
char t[16];
i[info::param::ids]="";
if (id)
for (unsigned j=0; j<id->size(); j++){
if (j>0) i[info::param::ids]+=", ";
Cat_Id cid = id->at(j);
sprintf(t," %ld",cid.second);
i[info::param::ids]+= cid.first;
i[info::param::ids]+= t;}
return i;}
//------------
char Caelestis::getType() const{
return astro_types::caelestis;}
//------------
Caelestis::~Caelestis(){
if (this->id) delete id;}

/************************* ASTRUM *************************/
//----------------------------------------------------------
Astrum::Astrum():Caelestis(){
this->alt_name = NULL;
this->spec_class.setSpecClass("");
}

Astrum::Astrum(Coordinate &c, float _bright,const char *specClass,const char *name,const char *altname):
                Caelestis(c, _bright, name){
this->alt_name = NULL;
this->spec_class.setSpecClass(specClass);
setCharAttr(this->alt_name, altname);}
//---------------
Astrum::Astrum(ID_Count_type id, Coordinate &c, float _bright,const char *specClass,const char *name,const char *altname):
                Caelestis(id, c, _bright, name){
alt_name = NULL;
this->spec_class.setSpecClass(specClass);
setCharAttr(this->alt_name, altname);}
//----------------------------------------------------------
const char* Astrum::altName(){
return alt_name;}
//---------
info::ParamValues Astrum::getInfo() const{
info::ParamValues i = Caelestis::getInfo();
i[info::param::spec_class] = this->spec_class.toString();
return i;}
//-----------
char Astrum::getType() const{
    return astro_types::astrum;}
//-----------
Astrum::~Astrum(){
if (this->alt_name) delete[] alt_name;}


namespace dso{
const char* types[] = {"Unverified southern object","Open Cluster", "Globular Cluster", "Diffuse Nebula","Planetary Nebula",
                       "Galaxy", "Cluster associated with nebulosity", "Non existent",
                       "Object in Large Magellanic Cloud", "Object in Small Magellanic Cloud", };

DSO::DSO(ID_Count_type id,Coordinate &c,float _bright,const char *name,unsigned _type_ODS):Caelestis(id, c, _bright, name){
if (_type_ODS<=9) type_ODS=_type_ODS;
else type_ODS = NonExistent;}

DSO::DSO(Coordinate &c,float _bright,const char *name,unsigned _type_ODS):Caelestis(c, _bright, name),type_ODS(_type_ODS){}
void DSO::SetType(unsigned _type_ODS){this->type_ODS=_type_ODS;}
unsigned DSO::Type(){return this->type_ODS;}

info::ParamValues DSO::getInfo(){
info::ParamValues  i = Caelestis::getInfo();
i[info::param::type] = types[type_ODS];
return i;}

char DSO::getType() const{
    return astro_types::dso;}
}

/********************** SPECTRALL CLASS *******************/
namespace spectral_class{
const char def_sc = 'G'; //спектраьный класс по умолчанию
const unsigned short def_ssc = 2;
//spec/ class main letter to rgb
float* rgbBySpecCode(char c){
float *rgb=new float[3];
switch (c){
case 'O': {rgb[0]=170./255; rgb[1]=191./255; rgb[2]=1.; break;}//aabfff
case 'B': {rgb[0]=202./255; rgb[1]=215./255; rgb[2]=1.; break;}//cad7ff
case 'A': {rgb[0]=248./255; rgb[1]=247./255; rgb[2]=1.; break;}//f8f7ff
case 'F': {rgb[0]=248./255; rgb[1]=247./255; rgb[2]=1.; break;}//f8f7ff
case 'G': {rgb[0]=255./255; rgb[1]=242./255; rgb[2]=161./255; break;}//fff2a1
case 'K': {rgb[0]=255./255; rgb[1]=196./255; rgb[2]=111./255; break;}//ffc46f
case 'M': {rgb[0]=255./255; rgb[1]=96./255; rgb[2]=96./255; break;}//ff6060
default:     {rgb[0]=1.0;rgb[1]=1.0;rgb[2]=1.0;}}
return rgb;}

float* getColorBySpecClass(const char* spec_class){//возвращает массив из r,g,b
float *rgb=NULL;
if (spec_class!=NULL)  rgb=rgbBySpecCode(spec_class[0]);
return rgb;}
//----------------------------------------------------------
float* getColorBySpecClass(const SpectralClass& sc){//возвращает массив из r,g,b
float *rgb=rgbBySpecCode(sc.getLetterOfSpClass());
return rgb;}
//----------------def constructor
spectral_class::SpectralClass::SpectralClass(){
/*this->setSpClassLetter(spectral_class::def_sc);
this->setSubClass(spectral_class::def_ssc);*/}
//---------------
spectral_class::SpectralClass::SpectralClass(const char *sc){
this->setSpClassLetter(sc[0]);
this->setSubClass(sc[1]-49);}
//---------------get full spectral class
char* spectral_class::SpectralClass::toString() const{
char *tmp=new char[16];
tmp[2]=0;
tmp[0]=this->_class;
tmp[1]=49+this->subclass;
return tmp;}
//--------------get letter of spectrall class
char SpectralClass::getLetterOfSpClass() const{
return this->_class;}
//--------------get subcluss
unsigned short SpectralClass::getSubClass() const{
return this->subclass;}
//--------------set spectral class
void SpectralClass::setSpecClass(const char *sp){
if (sp!=NULL){
if (strlen(sp)>0) this->setSpClassLetter(sp[0]);
if (strlen(sp)>1) this->setSubClass(atoi(sp+1));}}
//--------------set letter of spectral class
void SpectralClass::setSpClassLetter(char l){
this->_class=l;}
//--------------
void SpectralClass::setSubClass(unsigned short x){
this->subclass=x;}
//------------
void SpectralClass::toRGB(float *rgb) const{

}
}
//-------------------
const float* getColorBySpecClass(const char* spec_class){//возвращает массив из r,g,b
float *rgb=new float[3];
if (spec_class!=NULL){
switch (spec_class[0]){
        case 'O': {rgb[0]=0.6666666666666666666666666666667; rgb[1]=0.74901960784313725490196078431373; rgb[2]=1.; break;}//aabfff
        case 'B': {rgb[0]=0.7921568627450980392156862745098; rgb[1]=0.84313725490196078431372549019608; rgb[2]=1.; break;}//cad7ff
        case 'A': {rgb[0]=0.97254901960784313725490196078431; rgb[1]=247./255; rgb[2]=1.; break;}//f8f7ff
        case 'F': {rgb[0]=0.97254901960784313725490196078431; rgb[1]=247./255; rgb[2]=1.; break;}//f8f7ff
        case 'G': {rgb[0]=1; rgb[1]=242./255; rgb[2]=161./255; break;}//fff2a1
        case 'K': {rgb[0]=1; rgb[1]=196./255; rgb[2]=111./255; break;}//ffc46f
        case 'M': {rgb[0]=1.; rgb[1]=96./255; rgb[2]=96./255; break;}//ff6060
}}else {rgb[0]=1.0;	rgb[1]=1.0;		rgb[2]=1.0;}
return rgb;}
//-------------------------------------------------------------------


namespace sidus{
Sidus::Sidus(const char* name, const char *abvr):BasicAstro(0,name){
this->abbreviation_=NULL;
setCharAttr(this->abbreviation_, abvr);}
//---------
void Sidus::add_point(Coordinate *c){
border.push_back(c);}
//--------
const char* Sidus::abbreviation()const{
return abbreviation_;}
//------
const CoordList Sidus::getBorderPoints()const{
return border;}
//--------
info::ParamValues Sidus::getInfo() const{
info::ParamValues i = BasicAstro::getInfo();
i[info::param::abvr] = this->abbreviation();
return i;}
}

}
/* ближний космос. солнечная система */
namespace proxima_mundus{

KeplerOrbit::KeplerOrbit(){
this->epoch=d_epoch;
ep=0; wp=0; e=1; a=1; i=0; w=0;}

KeplerOrbit::KeplerOrbit(double _ep, double _wp, double _e, double _a, double _i, double _w):
                             ep(_ep),    wp(_wp),     e(_e),     a(_a),     i(_i),     w(_w){
this->epoch=d_epoch;}
QDateTime KeplerOrbit::getEpoch() const {return epoch;}
double KeplerOrbit::EP()const {return ep;}
double KeplerOrbit::WP()const {return wp;}
double KeplerOrbit::E()const {return e;}
double KeplerOrbit::A()const {return a;}
double KeplerOrbit::I()const {return i;}
double KeplerOrbit::W()const{return w;}

void KeplerOrbit::setEpoch(const QDateTime &_epoch){
epoch=_epoch;}
void KeplerOrbit::EP(double _ep){ep=_ep;}
void KeplerOrbit::WP(double _wp){wp=_wp;}
void KeplerOrbit::E(double _e){e=_e;}
void KeplerOrbit::A(double _a){a=_a;}
void KeplerOrbit::I(double _i){i=_i;}
void KeplerOrbit::W(double _w){w=_w;}
/*orbiter body*/
OrbiterBody::OrbiterBody(const char *_name, const char* _comment):BasicAstro(0,_name, _comment){
this->coordinate.setType(coordinates::type::ecliptic);}
OrbiterBody::OrbiterBody(ID_Count_type id, const char *_name, double polar_rad, double _m, double _Tp):BasicAstro(id,_name){
sphere.setPolarR(polar_rad);
m=_m;Tp=_Tp;
this->coordinate.setType(coordinates::type::ecliptic);}
OrbiterBody::~OrbiterBody(){;}
void OrbiterBody::setMass(double _m){m=_m;}
void OrbiterBody::setTP(double _Tp){Tp=_Tp;}
void OrbiterBody::setDayLength(double r) {this->day_length=r;}
void OrbiterBody::setDayPhase(double p) {this->day_phase=p;}
void OrbiterBody::setEqToOrbit(double eto) {this->eq_orbeq_orb_angle=fmod(eto,360);}
void OrbiterBody::setKepOrbit(const KeplerOrbit &ko){this->kep_orbit=ko;}
double OrbiterBody::Mass()const {return m;}
double OrbiterBody::TP()const {return Tp;}
double OrbiterBody::DayLength() const{return this->day_length;}
double OrbiterBody::DayPhase() const {return this->day_phase;}
double OrbiterBody::EqToOrbit() const {return eq_orbeq_orb_angle;}
const KeplerOrbit& OrbiterBody::getKepOrbit() const {return kep_orbit;}

info::ParamValues OrbiterBody::getInfo() const{
info::ParamValues i = BasicAstro::getInfo();
unsigned year = floor(this->Tp);
float month = (this->Tp-year)*12.;
char t[24];
if (year>0){
const char *y[]={"year","years"};short yi=1;
if (year==1) yi=0;
sprintf(t,"%d %s %2.3f months",year,y[yi],month);}
else sprintf(t,"%2.3f months",month);
i[info::param::tp] = t;
sprintf(t,"%8.3f km",this->sphere.PolarR());
i[info::param::polar_radius] = t;
sprintf(t,"%5.2f day's",this->day_length);
i[info::param::rotation_period] = t;
return i;}
//--------------
char OrbiterBody::getType() const{
return astro_types::orbiter;}
/*******    Planet  ************/
Planet::Planet(const char* name):OrbiterBody(name){
rings=NULL;
n_satellites=new PlanetVector;
star_name=NULL;}
Planet::Planet(ID_Count_type id, const char *name, double polar_rad):OrbiterBody(id, name,polar_rad){
rings=NULL;
n_satellites=new PlanetVector;
star_name=NULL;}
const char* Planet::getMajorStar() const  {return this->star_name;}
const PlanetVector* Planet::getNSats() const  {return this->n_satellites;}
void Planet::setRings(RingList *ring){
this->rings=ring;}
void Planet::addNSat(Planet *nsat){
this->n_satellites->push_back(nsat);
}
//------------------
Planet::~Planet(){
delete []star_name;
for(PlanetVector::iterator it=n_satellites->begin(), end = n_satellites->end(); it!=end; it++) delete (*it);
delete n_satellites;
delete rings;}
//-------------
info::ParamValues Planet::getInfo() const{
info::ParamValues i = OrbiterBody::getInfo();
return i;}
char Planet::getType() const{
    return astro_types::orbiter;}
/********* COMET ***********/
Cometa::Cometa(const char* _name):OrbiterBody(_name){
}
//-------------------------
Cometa::Cometa(ID_Count_type id, const float _color_rgba[], float _evaporation):OrbiterBody(id){
 memcpy(this->color_rgba, _color_rgba, 4*sizeof(float));
 evaporation=_evaporation;}
//--------
info::ParamValues Cometa::getInfo() const{
info::ParamValues i = OrbiterBody::getInfo();
return i;}

char Cometa::getType() const{
return astro_types::orbiter;}
/**/
//------------------------------------Kepler's equation. eps = accuracy
double E_Sub_e_Mult_sin_E_eqvl_M(double e, double M, double epsl){
double b;double E=M;int i=0;
do{
if (i==1000)break;
b=E-e*sin(E)-M; E=E-(b/(1-e*cos(E)));
i++;}while (fabs(b)>epsl);
return E;}
//-----------------------------------
double N_0_360(double N){
if(N>0 && N>360)N=N-360*(1+(int)N/360);
if(N<0)N=N+360*(1+(int)fabs(N)/360);
return N;}
//-------------------------------------
void upd_position_planet(const QDateTime &stdt,const QDateTime &currentdt,Planet &p){
double D=stdt.daysTo(currentdt)+
converting::HorMinSecToFloat(currentdt.time().hour(), currentdt.time().minute(), currentdt.time().second())/24;
double Mp=N_0_360((D*360)/(365.2422*p.TP()))+p.kep_orbit.EP()-p.kep_orbit.WP(); //365.2422 - как константа. долгота в эпоху - долгота перигелия. средняя аномалия
double E=E_Sub_e_Mult_sin_E_eqvl_M(p.kep_orbit.E(),Mp*M_PI/180 ,0E-1);
double vp=N_0_360(2*atan(pow((1+p.kep_orbit.E())/(1-p.kep_orbit.E()),0.5)*tan(E/2))*180/M_PI);
double l=vp+p.kep_orbit.WP();  //гелиоцентрическая долгота
double r=p.kep_orbit.A()*(1-pow(p.kep_orbit.E(),2))/(1+p.kep_orbit.E()*cos(vp*M_PI/180));
double W=asin(sin((l-p.kep_orbit.W())*M_PI/180)*sin(p.kep_orbit.I()*M_PI/180.));
//double ll= atan2(sin((l-pl.W())*M_PI/180)*cos(pl.I()*M_PI/180),cos((l-pl.W())*M_PI/180))*180/M_PI;
double rr=r*cos(W);
p.coordinate=Coordinate(l, W/M_PI*180.,rr, type::ecliptic);}
//------------------
Coordinate get_position_planet(const QDateTime &stdt,const QDateTime &currentdt,const Planet& p){
double D=stdt.daysTo(currentdt)+
converting::HorMinSecToFloat(currentdt.time().hour(), currentdt.time().minute(), currentdt.time().second())/24;
double Mp=N_0_360((-D*360)/(365.2422*p.TP()))+p.kep_orbit.EP()+p.kep_orbit.WP(); //365.2422 - как константа. долгота в эпоху - долгота перигелия. средняя аномалия
double E=E_Sub_e_Mult_sin_E_eqvl_M(p.kep_orbit.E(),Mp*M_PI/180 ,0E-1);
double vp=N_0_360(2*atan(pow((1+p.kep_orbit.E())/(1-p.kep_orbit.E()),0.5)*tan(E/2))*180/M_PI);
double l=vp+p.kep_orbit.WP();  //гелиоцентрическая долгота
double r=p.kep_orbit.A()*(1-pow(p.kep_orbit.E(),2))/(1+p.kep_orbit.E()*cos(vp*M_PI/180));
double W=asin(sin((l-p.kep_orbit.W())*M_PI/180)*sin(p.kep_orbit.I()*M_PI/180.));
//double ll= atan2(sin((l-pl.W())*M_PI/180)*cos(pl.I()*M_PI/180),cos((l-pl.W())*M_PI/180))*180/M_PI;
double rr=r*cos(W);
return Coordinate(l, W/M_PI*180.,rr, type::ecliptic);}
}


//void get_position_planet(Coordinate& c, const QDateTime &curdt,const char* Name_p, char mode/*или эпоху*/){
//double MJD, T;
//double dist, fac;
//int iPlanet;
//Vec3D R_Sun, r_helioc, r_geoc, r_equ;
//Mat3D P;
//const char* Name[] =
//{ "Sun", "Mercury", "Venus", "Earth", "Mars","Jupiter", "Saturn", "Uranus", "Neptune", "Pluto"};
//MJD = Mjd(curdt.date().year(),curdt.date().month(),curdt.date().day())+
//converting::HorMinSecToFloat(curdt.time().hour(), curdt.time().minute(), curdt.time().second())/24;
//T   = ( MJD - MJD_J2000 ) / 36525.0;
//R_Sun = SunPos(T);
//for(iPlanet=0;iPlanet<10; iPlanet++)
//if(!strcmp(Name_p,Name[iPlanet]))break;
//PlanetType Pt = (PlanetType) iPlanet;
//r_helioc = PertPosition(Pt, T);
//r_geoc = r_helioc + R_Sun;
//dist = Norm(r_geoc);
//fac = dist/c_light;
//if (mode=='a')r_geoc -= fac*(KepVelocity(Pt,T)-KepVelocity(Earth,T));
//else r_geoc -= fac*KepVelocity(Pt,T);
//switch (mode){
//case 'a': r_equ= NutMatrix(T) * Ecl2EquMatrix(T) * r_geoc;break;
//case 'j': P= PrecMatrix_Ecl(T,T_J2000);
//r_helioc = P * r_helioc;
//r_geoc= P * r_geoc;
//r_equ= Ecl2EquMatrix(T_J2000) * r_geoc;break;
//case 'b': P= PrecMatrix_Ecl(T,T_B1950);
//r_helioc = P * r_helioc;
//r_geoc   = P * r_geoc;
//r_equ    = Ecl2EquMatrix(T_B1950) * r_geoc;}
//c.Alpha(Deg*r_helioc[phi]);
//c.Sigma(Deg*r_helioc[theta]);
//c.Range(r_helioc[r]);}

//-----------------------------
//Vec3D Accel (double Mjd, const Vec3D& r){
//static const double  GM[10] = {
//GM_Sun,                 // Sun
//GM_Sun / 6023600.0,     // Mercury
//GM_Sun /  408523.5,     // Venus
//GM_Sun /  328900.5,     // Earth
//GM_Sun / 3098710.0,     // Mars
//GM_Sun /    1047.355,   // Jupiter
//GM_Sun /    3498.5,     // Saturn
//GM_Sun /   22869.0,     // Uranus
//GM_Sun /   19314.0,     // Neptune
//GM_Sun / 3000000.0      // Pluto
//};
//  int         iPlanet;
//  PlanetType  Planet;
//  Vec3D       a, r_p, d;
//  Mat3D       P;
//  double      T, D;

//  // Solar attraction
//  D = Norm(r);
//  a = - GM_Sun * r / (D*D*D);

//  // Planetary perturbation
//  T = ( Mjd - MJD_J2000 ) / 36525.0;
//  P = PrecMatrix_Ecl ( T, T_J2000 );    // Precession

//for (iPlanet=Mercury; iPlanet<=Neptune; iPlanet++) {
//Planet = (PlanetType) iPlanet;
//// Planetary position (ecliptic and equinox of J2000)
////  r_p = P * KepPosition(Planet,T);    // Fast but low precison
//    r_p = P * PertPosition(Planet,T);   // Accurate but slow
//    d   = r - r_p;
//    // Direct acceleration
//    D = Norm(d);
//    a += - GM[iPlanet] * d / (D*D*D);
//    // Indirect acceleration
//    D = Norm(r_p);
//    a += - GM[iPlanet] * r_p / (D*D*D);}
//return a;}

//void F (double X, double Y[], double dYdX[]){
//  Vec3D a, r;
//  r = Vec3D(Y[1], Y[2], Y[3]);
//  a = Accel(X, r);
//  dYdX[0] = 0.0;
//  dYdX[1] = Y[4]; dYdX[2] = Y[5];  dYdX[3] = Y[6];  // velocity
//  dYdX[4] = a[x]; dYdX[5] = a[y];  dYdX[6] = a[z];  // acceleration
//}

//Coordinate* get_position_comet(const QDateTime &curdt,
//            const Cometa& cometa,double eps/*точность*/,int  Neqn/*количество уравнений*/,double Year){
//    //
//    // Constants
//    //
//    const pol_index R    = pol_index(2);   // Index for vector length
//    //const int       Neqn = 6;              // Number of differential eqns.
//   //const double    eps  = 1.0e-10;        // sRelative accuracy
//    //
//    // Variables
//    //
//   // int       n_line;
//    double    MjdStart, /*Step, MjdEnd,*/ T_eqx;
//    double    MjdEpoch, a, e, M, T_eqx0;
//    double    Omega, i, omega;
//    Mat3D     PQR, P;
//    double    Mjd_, /*MjdStep,*/ T;
//    SolverDE  Orbit(F, Neqn);
//    double    Y[Neqn+1];
//    double    relerr,abserr;
//    DE_STATE  State;
//    Vec3D     r, v;
//    Vec3D     R_Sun, r_helioc, v_helioc/*, r_geoc, r_equ*/;

//  a=cometa.kep_orbit.A();
//  e=cometa.kep_orbit.E();
//  i=cometa.kep_orbit.I()*Rad;
//  Omega=cometa.kep_orbit.W()*Rad;
//  omega=cometa.kep_orbit.WP()*Rad;
//  M=cometa.Mass()*Rad;


//  MjdEpoch = Mjd(curdt.date().year(),curdt.date().month(),curdt.date().day())+
//  converting::HorMinSecToFloat(curdt.time().hour(), curdt.time().minute(), curdt.time().second())/24;
//  T_eqx0   = (Year-2000.0)/100.0;
//PQR      = GaussVec(Rad*Omega,Rad*i,Rad*omega);

//T_eqx = (Year-2000.0) /100.0;

//    PQR = PrecMatrix_Ecl(T_eqx0,T_J2000) * PQR;


//    Mjd_ = MjdEpoch;

//    MjdStart = Mjd(cometa.kep_orbit.getEpoch().date().year(),cometa.kep_orbit.getEpoch().date().month(),cometa.kep_orbit.getEpoch().date().day())+
//    converting::HorMinSecToFloat(cometa.kep_orbit.getEpoch().time().hour(), cometa.kep_orbit.getEpoch().time().minute(), cometa.kep_orbit.getEpoch().time().second())/24;
//  Kepler (GM_Sun, Mjd_, MjdStart, cometa.kep_orbit.A(), e, PQR, r_helioc, v_helioc);

//Coordinate* c=new Coordinate(Deg*r_helioc[phi],Deg*r_helioc[theta],r_helioc[R],type::ecliptic);
//return c;}


//void get_position_nsats(Coordinate& c,const QDateTime &curdt,Planet& nsat,char mode){
//double MJD, T;
//Mat3D P;
//Vec3D R_Sun, r_helioc;
//MJD = Mjd(curdt.date().year(),curdt.date().month(),curdt.date().day())+
//converting::HorMinSecToFloat(curdt.time().hour(), curdt.time().minute(), curdt.time().second())/24;
//T   = ( MJD - MJD_J2000 ) / 36525.0;
//R_Sun = SunPos(T);
//if (!strcmp("Moon",nsat.name()))
//r_helioc=MoonPos(T)-R_Sun;
//switch (mode){
//case 'j': P= PrecMatrix_Ecl(T,T_J2000);
//r_helioc = P * r_helioc;
//break;
//case 'b': P= PrecMatrix_Ecl(T,T_B1950);
//r_helioc = P * r_helioc;}
//c= Coordinate(Deg*r_helioc[phi],Deg*r_helioc[theta],r_helioc[r]/AU,type::ecliptic);}

//Coordinate get_position_nsats(const QDateTime &curdt,const Planet& nsat,char mode){
//double MJD, T;
//Mat3D P;
//Vec3D R_Sun, r_helioc;
//MJD = Mjd(curdt.date().year(),curdt.date().month(),curdt.date().day())+
//converting::HorMinSecToFloat(curdt.time().hour(), curdt.time().minute(), curdt.time().second())/24;
//T   = ( MJD - MJD_J2000 ) / 36525.0;
//R_Sun = SunPos(T);
//if (!strcmp("Moon",nsat.name()))
//r_helioc=MoonPos(T)-R_Sun;
//switch (mode){
//case 'j': P= PrecMatrix_Ecl(T,T_J2000);
//r_helioc = P * r_helioc;
//break;
//case 'b': P= PrecMatrix_Ecl(T,T_B1950);
//r_helioc = P * r_helioc;}
//return Coordinate(Deg*r_helioc[phi],Deg*r_helioc[theta],r_helioc[r]/AU,type::ecliptic);}

float VMag(float Abs_mag, double distance_sun, double distance_obs, float phase){
return Abs_mag+5*log10(distance_sun*distance_obs)/*+ f(phase)*/;}

float AbsMag(float diameter, float g_albedo, float c){
return c-2.5*log(g_albedo*diameter*diameter)/log(2);}
}


