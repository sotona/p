﻿#include <algorithm>
#include "../../straux.h"
#include "astro_db.h"
#include "cat_definitions.h"
/*AstroData*/
AstroData::AstroData(){
planet_systems = new std::vector<planetary_system::PlanetarySystem*>;
this->global_space=new profundus_mundus::ProfundusMundus;}
AstroData::AstroData(const AstroData &ad){
this->global_space = ad.global_space;
this->planet_systems = ad.planet_systems;}

planetary_system::PlanetarySystem* AstroData::nativeSS(){
if (this->planet_systems->size()) return planet_systems->at(0);
else return NULL;}

const planetary_system::PlanetarySystem& AstroData::getNativeSS() const{
return *planet_systems->at(0);}

template <class A>
inline elementus_mundus::ID_Count_type getFirstId(const A& a){
if (a.size()){
typename A::const_iterator it = a.begin();
typename A::value_type x = *it;
return x->absID();}
else return 0;}

IndArray initPS_indexes(const planetary_system::PlanetarySystem& ps){
IndArray i;

return i;}

inline bool is_there(const FirUnt& firunt, elementus_mundus::ID_Count_type& id){
return (firunt.first <= id && id<= firunt.first+firunt.second);}

template <class Vector> // vector - some std::vector of std::pair of elementus_mundus::ID_Count_type
short where_is(const Vector& v, elementus_mundus::ID_Count_type id){
elementus_mundus::ID_Count_type a,b;
for (unsigned short i=0; i<v.size(); i++){
a = v[i].first, b = v[i].first + v[i].second;
if (id >= a && id <= b) return i;} return -1;}
//--------------
template <class AL>
inline const elementus_mundus::BasicAstro* getObjectByID(const AL& al, elementus_mundus::ID_Count_type id){
typename AL::const_iterator it=al.begin();
elementus_mundus::BasicAstro *a;
for (unsigned long i=0; i<id; i++, it++);
a = (*it);
return a;}

/****** AstroDB ******/
void AstroDB::initIDsArray(){

{/*инициализация индексов глубокого космоса*/
    b_id.deep_sky.array.resize(straux::strarrlen(cat_types::ds_cats)); /*инициализация размеров всех типов астросписков ОДК*/
    b_id.deep_sky.firunt.second = b_id.deep_sky.array[cat_types::ds::asterism].second = data->global_space->asterisms->size();
    b_id.deep_sky.firunt.second+= b_id.deep_sky.array[cat_types::ds::caelestis].second = data->global_space->getCaelestis().size();
    b_id.deep_sky.firunt.second+= b_id.deep_sky.array[cat_types::ds::constellation].second = data->global_space->getConstellations().size();
    b_id.deep_sky.firunt.second+= b_id.deep_sky.array[cat_types::ds::star].second = data->global_space->getStars().size();
    b_id.deep_sky.firunt.second+= b_id.deep_sky.array[cat_types::ds::DSO].second = data->global_space->getDSOs().size();
    /*получение первых идентификаторов каждого астросписка ОДК*/
    b_id.deep_sky.array[cat_types::ds::asterism].first = getFirstId<profundus_mundus::AsterismList>(data->global_space->getAsterisms());
    b_id.deep_sky.array[cat_types::ds::caelestis].first = getFirstId<profundus_mundus::CaelestisList>(data->global_space->getCaelestis());
    b_id.deep_sky.array[cat_types::ds::constellation].first = getFirstId<profundus_mundus::SidusList>(data->global_space->getConstellations());
    b_id.deep_sky.array[cat_types::ds::star].first = getFirstId<profundus_mundus::AstraList>(data->global_space->getStars());
    b_id.deep_sky.firunt.first =
     b_id.deep_sky.array[cat_types::ds::DSO].first = getFirstId<profundus_mundus::DSOList>(data->global_space->getDSOs());

    for (unsigned i=0; i<b_id.deep_sky.array.size(); i++)
    b_id.deep_sky.firunt.first = std::min(b_id.deep_sky.firunt.first,
             elementus_mundus::ID_Count_type((b_id.deep_sky.array[i].first==0)?-1:b_id.deep_sky.array[i].first));
    }

unsigned ps_cats = straux::strarrlen(cat_types::ps_cats);
b_id.plnet_systems.resize(this->data->planet_systems->size());
for (unsigned i=0; this->data->planet_systems->size()>i; i++){
b_id.plnet_systems[i].array.resize(ps_cats);
/*getting count of objects in this ps*/
b_id.plnet_systems[i].firunt.second=1;                                  // central star
b_id.plnet_systems[i].firunt.second+=b_id.plnet_systems[i].array[cat_types::ps::asteroid].second = 0;
b_id.plnet_systems[i].firunt.second+=b_id.plnet_systems[i].array[cat_types::ps::comet].second = data->planet_systems->at(i)->getComets().size();
b_id.plnet_systems[i].firunt.second+=b_id.plnet_systems[i].array[cat_types::ps::planet].second = data->planet_systems->at(i)->getPlanets().size();
// первый идентификатор всегда звезда. см load::planet_system
b_id.plnet_systems[i].firunt.first= (data->planet_systems->at(i)->getStar()!=NULL)?
    data->planet_systems->at(i)->getStar()->absID():0;
b_id.plnet_systems[i].array[cat_types::ps::cental_star].first = data->planet_systems->at(i)->getStar()->absID();
b_id.plnet_systems[i].array[cat_types::ps::asteroid].first = 0;
b_id.plnet_systems[i].array[cat_types::ps::asteroid].first = 0;
b_id.plnet_systems[i].array[cat_types::ps::comet].first = getFirstId<planetary_system::CometList>(data->planet_systems->at(i)->getComets());
b_id.plnet_systems[i].array[cat_types::ps::planet].first = getFirstId<planetary_system::PlanetList>(data->planet_systems->at(i)->getPlanets());
for (unsigned j=0; j<b_id.plnet_systems[i].array.size(); j++)
b_id.plnet_systems[i].firunt.first = std::min(b_id.plnet_systems[i].firunt.first,
 elementus_mundus::ID_Count_type(b_id.plnet_systems[i].array[j].first==0)?-1:b_id.plnet_systems[i].array[j].first);}
}
/* ID data base */
AstroDB::AstroDB(const AstroData &astro_data){
data = new AstroData(astro_data);
initIDsArray();
sel_id = 0;
selected_info.clear();
selected = NULL;}

//----------------
AstroDB::~AstroDB(){

}

//-----------------
short AstroDB::typeOfID(elementus_mundus::ID_Count_type id) const{
//if (!ds_ids_array.size()) return -1;
//short t=-1;
if (is_there(b_id.deep_sky.firunt,id)) return where_is(b_id.deep_sky.array,id);
else for (unsigned i=0; i<b_id.plnet_systems.size(); i++)
if (is_there(b_id.plnet_systems[i].firunt,id)) return -1;
return -1;}
//--------------
void AstroDB::activateID(elementus_mundus::ID_Count_type id){
// = typeOfID(id); if (t<0) {selected=NULL; selected_info.clear(); return;}
this->sel_id = id;
short t;
using namespace cat_types::ds;
if (is_there(b_id.deep_sky.firunt,id)){ t= where_is(b_id.deep_sky.array,id);
elementus_mundus::ID_Count_type tid = id - b_id.deep_sky.array[t].first;
switch(t){
case asterism:      {selected = getObjectByID<profundus_mundus::AsterismList> (data->global_space->getAsterisms(), tid); break;}
case caelestis:     {selected = getObjectByID<profundus_mundus::CaelestisList>(data->global_space->getCaelestis(), tid); break;}
case constellation: {selected = getObjectByID<profundus_mundus::SidusList>    (data->global_space->getConstellations(), tid); break;}
case star:          {selected = getObjectByID<profundus_mundus::AstraList>    (data->global_space->getStars(),tid); break;}
case DSO:           {selected = getObjectByID<profundus_mundus::DSOList>      (data->global_space->getDSOs(),tid); break;}
default: selected=NULL;}}
else{ unsigned ps_i = 0; // номер планетной системы
for (unsigned i=0; i<b_id.plnet_systems.size(); i++)if (is_there(b_id.plnet_systems[i].firunt,id)) ps_i=i;
t = where_is(b_id.plnet_systems[ps_i].array,id);
if (data->planet_systems->at(ps_i)->getStar()->absID()==id) selected=data->planet_systems->at(ps_i)->getStar();
else{
elementus_mundus::ID_Count_type  tid = id - b_id.plnet_systems[ps_i].array[t].first;
using namespace cat_types::ps;
switch(t){
case asteroid:      {/*selected = getObjectByID<profundus_mundus::AsterismList> (data->planet_systems[ps_i]->getAsteriod(), tid);*/ break;}
case comet:         {selected = getObjectByID<planetary_system::CometList>  (data->planet_systems->at(ps_i)->getComets(), tid); break;}
case planet:        {selected = getObjectByID<planetary_system::PlanetList> (data->planet_systems->at(ps_i)->getPlanets(), tid); break;}
case satellite:     {selected = getObjectByID<planetary_system::PlanetList> (data->planet_systems->at(ps_i)->getPlanets(),tid); break;}
default: selected=NULL;}}
}
if (selected!=NULL)
selected_info = selected->getInfo();}
//---------------------------
void AstroDB::setNoAsctiveId(){
this->sel_id = 0;
this->selected = NULL;
this->selected_info.clear();}
//-------------
elementus_mundus::ID_Count_type AstroDB::getActiveID(){
return this->sel_id;}
//-------------------------------------------------------------
const std::list<elementus_mundus::BasicAstro*>& AstroDB::findAstras(const char *find_line){
std::list<elementus_mundus::BasicAstro*>* find_list=new std::list<elementus_mundus::BasicAstro*>;
if (!strlen(find_line))return *find_list;
for(profundus_mundus::AstraList::const_iterator it=data->global_space->getStars().begin(),
                                                end=data->global_space->getStars().end(); it!=end; it++){
char *buff_n=strdup((*it)->name());
char *buff_f=strdup(find_line);
if (strstr(strupr(buff_n),strupr(buff_f)))
find_list->push_back(*it);
delete[] buff_f;
delete[] buff_n;}
return *find_list;}

const std::list<elementus_mundus::BasicAstro*>& AstroDB::findAsterizm(const char *find_line){
std::list<elementus_mundus::BasicAstro*>* find_list=new std::list<elementus_mundus::BasicAstro*>;
if (!strlen(find_line))return *find_list;
for(profundus_mundus::AsterismList::const_iterator it=data->global_space->getAsterisms().begin(),
                                                end=data->global_space->getAsterisms().end(); it!=end; it++){
char *buff_n=strdup((*it)->name());
char *buff_f=strdup(find_line);
if (strstr(strupr(buff_n),strupr(buff_f)))
find_list->push_back(*it);
delete[] buff_f;
delete[] buff_n;}
return *find_list;}

const std::list<elementus_mundus::BasicAstro*>& AstroDB::findPlanet(const char *find_line){
std::list<elementus_mundus::BasicAstro*>* find_list=new std::list<elementus_mundus::BasicAstro*>;
if (!strlen(find_line))return *find_list;
for(planetary_system::PlanetList::const_iterator it=data->getNativeSS().getPlanets().begin(),
                                                end=data->getNativeSS().getPlanets().end(); it!=end; it++){
char *buff_n=strdup((*it)->name());
char *buff_f=strdup(find_line);
if (strstr(strupr(buff_n),strupr(buff_f)))
find_list->push_back(*it);
delete[] buff_f;
delete[] buff_n;

for(unsigned i=0; i<(*it)->getNSats()->size();i++){
buff_n=strdup((*it)->getNSats()->at(i)->name());
buff_f=strdup(find_line);
if (strstr(strupr(buff_n),strupr(buff_f)))
find_list->push_back((*it)->getNSats()->at(i));
delete[] buff_f;
delete[] buff_n;}}
return *find_list;}

const std::list<elementus_mundus::BasicAstro*>& AstroDB::findComet(const char *find_line){
std::list<elementus_mundus::BasicAstro*>* find_list=new std::list<elementus_mundus::BasicAstro*>;
if (!strlen(find_line))return *find_list;
for(planetary_system::CometList::const_iterator it=data->getNativeSS().getComets().begin(),
                                                end=data->getNativeSS().getComets().end(); it!=end; it++){
char *buff_n=strdup((*it)->name());
char *buff_f=strdup(find_line);
if (strstr(strupr(buff_n),strupr(buff_f)))
find_list->push_back(*it);
delete[] buff_f;
delete[] buff_n;}
return *find_list;}

const std::list<elementus_mundus::BasicAstro*>& AstroDB::findDSO(const char *find_line){
std::list<elementus_mundus::BasicAstro*>* find_list=new std::list<elementus_mundus::BasicAstro*>;
if (!strlen(find_line))return *find_list;
for(profundus_mundus::DSOList::const_iterator it=data->global_space->getDSOs().begin(),
                                                end=data->global_space->getDSOs().end(); it!=end; it++){
char *buff_n=strdup((*it)->name());
char *buff_f=strdup(find_line);
if (strstr(strupr(buff_n),strupr(buff_f)))
find_list->push_back(*it);
delete[] buff_f;
delete[] buff_n;}
return *find_list;}

//--------------------------------------------------------------
//const elementus_mundus::proxima_mundus::Planet* AstroDB::getPlanet(elementus_mundus::ID_Count_type id){
//const sistema_solare::PlanetList &pl = this->data->getNativeSS().getPlanets();
//elementus_mundus::proxima_mundus::Planet *p=NULL;
//for (sistema_solare::PlanetList::const_iterator it=pl.begin(), end = pl.end(); it!=end; it++){
//p = (*it);
//if (p->absId()==id) {p=*it; break;} else p=NULL;
//}
//return p;}
void AstroDB::updateInfo(){
this->selected_info = selected->getInfo();
}
