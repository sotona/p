﻿#include "cat_definitions.h"

namespace file_fields{
const char *star        =   "star";
const char *name =          "name";
const char *alt_name =      "alt_name";
const char *type=           "type";
/*coordinates*/
const char *RA =            "ra";
const char *declination =   "decl";
const char *r = "r";
const char *parallax =      "parallax";
namespace phis_characteristic{
const char *polarR = "polar_r";
const char *flattering = "flattering";
const char *spec_class = "spec_class";
const char *abs_vmag = "abs_vmag";}
/*rotation*/
const char *day_length = "day_length";
const char* day_phase = "day_phase";
namespace proper_motion{
const char *RA =            "pmRA";
const char *declination =   "pmDE";}
/*orbital period*/
const char *tp =            "tp"; // годы.
const char *tp_d        =   "tp_d";                // дни
const char *eqtec        =   "eqtec";               // наклон экватора к орбите
namespace orbital_elements{
const char *semi_major_axis =   "a";
const char *semi_major_axis_km ="a_km";
const char *eccentricity =      "e";
const char *inclination =       "i";
const char *per_longitude =     "wp";   // долгота перицентра
const char *an_longitude =      "w";    // долгота восходящего узла
const char *ep_longitude =      "ep";   // долгота в эпоху
const char *mean_anomaly =      "ma";
}
/**/
const char *vmag =          "vmag";
namespace cat_name{
const char *hd =     "HD";
const char *hr =     "HR";
const char *sao =    "SAO";
const char *ngc =    "NGC";
const char *ic  =    "IC";}

namespace asterism{
const char* name = "ast";
}

namespace graph{
const char *factor = "factor";
const char *width = "wodth";
const char *color = "color";
const char *pattern = "pattern";}
}
