﻿#include "tempus.h"

namespace tempus{
namespace converting{
double datetoMJD(struct tm t){
long    MjdMidnight;
double  FracOfDay;
int     b;

//if (t.tm_mon<=2) { Month+=12; --Year;}

  if ( (10000L*t.tm_year+100L*t.tm_mon+t.tm_mday) <= 15821004L )
    b = -2 + ((t.tm_year+4716)/4) - 1179;     // Julian calendar
  else  b = (t.tm_year/400)-(t.tm_year/100)+(t.tm_year/4);  // Gregorian calendar

  MjdMidnight = 365L*t.tm_year - 679004L + b + int(30.6001*(t.tm_mon+1)) + t.tm_mday;
  FracOfDay   = (t.tm_hour+t.tm_min/60.,t.tm_sec/3600.) / 24.0;
return MjdMidnight + FracOfDay;}
}
}
