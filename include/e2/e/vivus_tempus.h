﻿#ifndef TEMPUS_H
#define TEMPUS_H
#include "elementus_mundus.h"
#include <QDateTime>
#include <QTimer>
#include <math.h>

namespace info{
namespace param{
namespace tempus{
extern const std::string time;
extern const std::string timespeed;
extern const std::string is_stopped;
}}}

namespace tempus{
using namespace coordinates;
const double d_delta = 10.; //seconds per second
const short my_timezone=9;

class VivusTempus:public QObject{
Q_OBJECT
public:
unsigned short timezone;
QDateTime Time;
QTimer timer;
float delta;  //seconds per second
 
VivusTempus(short _timezone = my_timezone);
void X(float x);  // множитель времени
float X() const;        // множитель времени
//------
float toHourf() const; //перевод в десятичные доли часа
float toGMTf() const;  //перевод в десятичные доли часа GMT(всемирного гринвеческого времени)
//------------
void stort(bool stort); //start = 1.stop = 0
void stort();
bool is_stort() const; //start = 1.stop = 0
//---------
void setCurrentTime();

public slots:
void tick();

 

};
}


#endif // TIME_H




















//// время не в смысле "который час?", а время вообще. 
//namespace tempus{
//
//struct Tempus{
//public:
//unsigned annus;
//unsigned mensis;
//unsigned dies;
//
//unsigned hora;
//unsigned minutus;
//unsigned secundus;
//float milisecundus;
//
//Tempus(unsigned _annus = 0, unsigned _mensis = 0, unsigned _dies = 0,
//             unsigned _hora = 0, unsigned _minutus = 0, unsigned _secundus = 0,
//             float _milisecundus = 0);
//Tempus operator +=(Tempus _t);
//void plus_msec(unsigned msec);
//void plus_sec(unsigned msec);
//void plus_min(unsigned msec);
//void plus_hora(unsigned msec);
//void plus_dies(unsigned msec);
//void plus_mensis(unsigned msec);
//void plus_annus(unsigned msec);
//};
//
//class VivusTempus{
//private:
//Tempus vivus;
// bool s;
// void fluctus();
//public:
// Tempus t;
// VivusTempus(Tempus _t, Tempus _vivus=Tempus(0,0,0,0,0,0,1));
// void start();
// void stop();
//};
//}
