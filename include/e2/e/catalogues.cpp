﻿/*
в функциях загрузки каталогов, куда передаётся начальный id
используется ++id.
*/

#include "cat_definitions.h"
#include "catalogues.h"
#include "../../straux.h"
#include "mundus.h"


const char* cat_log = "cat.log";

/* работа (загрузка) с астрономическими каталогами */
namespace catalogues{
using namespace coordinates::converting;
using elementus_mundus::ID_Count_type;
using elementus_mundus::proxima_mundus::Planet;
using deserialization::Dbuff_len;
using std::ifstream;

/*initializing catalogues formats and files*/
namespace init{
const std::string files="files";
const std::string format="format";
const std::string planet_system = "planet systems";

// загрузка форматов и имён файлов для данного типа астрообъектов.
// cat_node - узел одного определённого типа каталога, состоящий из мписка ворматов, и соот-х ему файлов.
CatDescriptors* cat(const YAML::Node& cat_node){
CatDescriptors* cadear = new CatDescriptors;
for (unsigned i=0; i<cat_node.size(); i++){                     // for list of catalogue components
const YAML::Node& rec = cat_node[i];
if (rec.FindValue(files)){
CatDescriptor ff;
const YAML::Node& files_node = rec[files];
ff.filename.resize(files_node.size());
for (unsigned j=0; j<files_node.size(); j++) files_node[j] >> ff.filename[j];
deserialization::formats::FormatOfRecord rformat;
if (rec.FindValue(init::format)){
std::string raw_format;
rec[init::format] >> raw_format;
rformat = deserialization::formats::recognize_format(raw_format,
            catalogues::mask::separator, catalogues::mask::param_mask);
ff.format.assign(rformat.begin(), rformat.end());}
cadear->cat_descriptor.push_back(ff);}
}return cadear;}
//--- load planet system. ps_cats - раздел в главном файле описывающий файлы описания планетных систем
// предпологается, что ps_cats уже имеет размер равный числу планетных систем, и имена соот-х файлов
// с описанием каталогов этой системы.
void planet_systems_cats(std::vector<planet_systemFF>& ps_cats){
std::ifstream cat_file;
YAML::Node node;
for (unsigned i =0 ;i<ps_cats.size(); i++){             // для каждого файла с описанием планетной системы
cat_file.open(ps_cats[i].filename.c_str(), ifstream::in);
if (cat_file.is_open()){
YAML::Parser parser(cat_file);
node.Clear();
try{parser.GetNextDocument(node);} catch(YAML::Exception &e) {return;}
ps_cats[i].cats = cat_array(node,cat_types::ps_cats);
//for (unsigned j=0; cat_types::ps_cats[j].size(); j++){
//const YAML::Node * n_rec = node.FindValue(cat_types::ps_cats[j]);
//if (n_rec) ps_cats[i].cats.push_back(init::cat(*n_rec));}
}cat_file.close();}
}
// получить файлы и соот-е им форматы для всех типов каталогов cat_types, присутствующих в cat_node
std::vector<CatDescriptors*> cat_array(const YAML::Node& cat_node,const std::string *cat_types){
std::vector<CatDescriptors*> cats;
for (unsigned i=0; cat_types[i].size(); i++){                      // for cat types
const YAML::Node * n_rec = cat_node.FindValue(cat_types[i]);
if (n_rec) {CatDescriptors *ffa = catalogues::init::cat(*n_rec);  // getting catalogue's files and their formats
cats.push_back(ffa);}else cats.push_back(NULL);}
return cats;}
}


/*loading catalogues*/
/* тут дохуя одинакового кода!!!
*/
namespace load{
using namespace profundus_mundus;
using namespace planetary_system;
AstraList* Astra(CatDescriptors* caders, elementus_mundus::ID_Count_type& records_loaded){
using elementus_mundus::altus_mundus::Astrum;
using namespace straux;
AstraList* al = new AstraList;
if (caders){
caders->total_records=0;
elementus_mundus::ID_Count_type rl=0; // records loaded in last step
elementus_mundus::ID_Count_type all_rl=records_loaded; // totall records loaded before this function
for (unsigned i=0; i<caders->cat_descriptor.size(); i++){
CatDescriptor &ff = caders->cat_descriptor.at(i);
ff.file_records.resize(ff.filename.size());
ff.total_records=0;
for (unsigned j=0; j<ff.filename.size(); j++){
rl=records_loaded;
deserialization::file::stars(ff.filename[j].c_str(),ff.format, al, records_loaded);
ff.file_records[j]=records_loaded-rl;
ff.total_records+=ff.file_records[j];}
}caders->total_records=records_loaded-all_rl;
}return al;}
//------------
PlanetList* Planets(const CatDescriptors* caders, elementus_mundus::ID_Count_type& records_loaded) {
using namespace deserialization;
PlanetList* pl = new PlanetList;
if (caders) for (unsigned i=0; i<caders->cat_descriptor.size(); i++){
const CatDescriptor &ff = caders->cat_descriptor.at(i);
for (unsigned j=0; j<ff.filename.size(); j++)
deserialization::file::planets(ff.filename[j].c_str(), ff.format, pl, records_loaded);}
return pl;}

CometList* Comets(const CatDescriptors* caders, elementus_mundus::ID_Count_type& records_loaded) {
using namespace deserialization;
CometList* cl = new CometList;
if (caders) for (unsigned i=0; i<caders->cat_descriptor.size(); i++){
const CatDescriptor &cad = caders->cat_descriptor.at(i);
for (unsigned j=0; j<cad.filename.size(); j++)
deserialization::file::comets(cad.filename[i].c_str(), cad.format, cl, records_loaded);
}
return cl;}
SidusList *Constellations(const CatDescriptors* caders, elementus_mundus::ID_Count_type& records_loaded) {
using namespace deserialization;
SidusList *sl=new SidusList;
char buff[Dbuff_len];
Sidus* s=NULL;
char last_name[128];
if (caders)
for (unsigned i=0; i<caders->cat_descriptor.size(); i++){
const CatDescriptor &ff = caders->cat_descriptor.at(i);
for (unsigned j=0; j<ff.filename.size(); j++){
FILE* f=fopen(ff.filename[i].c_str(),"r");
if(f!=NULL){
while(!feof(f)){
fgets(buff,Dbuff_len,f);
straux::trim::before_char(buff,'#');
//string_aux::trim::begin(buff,' ');
formats::ValueMap dt=record::get_values(buff, ff.format);
if(strlen(buff)>0){
if (strcmp(dt[file_fields::name],last_name)){
if (s!=NULL){
coordinates::mid_value(s->getBorderPoints(),s->coordinate);
s->coordinate.Range(1); sl->push_back(s);}

if (!strcmp(dt[file_fields::name],"XXX")) break;
s = new Sidus(dt[file_fields::name]); s->setAbsId(++records_loaded);
memset(last_name,0,128);
strcpy(last_name,dt[file_fields::name]);}
coordinates::Coordinate *c=new coordinates::Coordinate(atof(dt[file_fields::RA]), atof(dt[file_fields::declination]),
                                                         coordinates::very_far, coordinates::type::equator);
s->add_point(c);
delete[] dt[file_fields::name];
delete[] dt[file_fields::RA];
delete[] dt[file_fields::declination];
dt.clear();}
}
fclose(f);}}}
return sl;}
AsterismList* Asterizms(const CatDescriptors* caders, elementus_mundus::ID_Count_type& records_loaded, const AstraList& al) {
using namespace straux;
YAML::Parser parser;                                                // добавть в класс?
YAML::Node yaml_asts;
AsterismList* asl = new AsterismList();                             // ASterizms List
std::ifstream ast_file; // file contains asterizms
FILE* f_log = fopen(cat_log,"a"); fprintf(f_log,"loading asterisms:\n");
//for (FrDF_array::const_iterator it=asterizmdata.begin(), end =asterizmdata.end();it!=end;it++) // for all astrerizm files
if (caders)
for (unsigned i=0; i<caders->cat_descriptor.size(); i++){                 // for asterizm's files
const CatDescriptor &ff = caders->cat_descriptor.at(i);
for (unsigned j=0; j<ff.filename.size(); j++){
ast_file.open(ff.filename[i].c_str());
if (ast_file.is_open()){
parser.Load(ast_file);
float d_width;
float d_color[3];
try{parser.GetNextDocument(yaml_asts);}catch(YAML::Exception &e) {fprintf(f_log,"%s\n", e.what()); fclose(f_log); return asl;}
if (deserialization::record::asterizm_default(yaml_asts,d_width, d_color)) // если есть секция с параметрами по умолчанию
try{parser.GetNextDocument(yaml_asts);}catch(YAML::Exception &e) {fprintf(f_log,"%s\n", e.what()); fclose(f_log);return asl;}
for (unsigned i=0; i<yaml_asts.size(); i++){                    //for each asterizm in file
Asterizm *ast = deserialization::record::makeAsterizm(yaml_asts[i], al, d_width, d_color);
ast->setAbsId(++records_loaded);
asl->push_back(ast);}
}}}return asl;}
//--- loading deep sky objects
DSOList* DSO(const CatDescriptors* caders,elementus_mundus::ID_Count_type& records_loaded){
using namespace deserialization;
DSOList* odsl=new DSOList;
if (caders) for (unsigned i=0; i<caders->cat_descriptor.size(); i++){
CatDescriptor ff=caders->cat_descriptor.at(i);
for (unsigned j=0; j<ff.filename.size(); j++)
deserialization::file::odss(ff.filename[j].c_str(), ff.format, odsl, records_loaded);}
return odsl;}
//----------------------
unsigned Sattelites(const CatDescriptors *caders, planetary_system::PlanetList* pl, elementus_mundus::ID_Count_type& rec_loaded){
using namespace deserialization;
if (caders) for (unsigned i=0; i<caders->cat_descriptor.size(); i++){
const CatDescriptor &ff = caders->cat_descriptor.at(i);
for (unsigned j=0; j<ff.filename.size(); j++){
deserialization::file::satellites(ff.filename[j].c_str(),ff.format,pl, rec_loaded);}
}return 1;}
//------------
planetary_system::PlanetarySystem *Planetary_System(const planet_systemFF& ps_cats, elementus_mundus::ID_Count_type& rec_loaded){
planetary_system::PlanetarySystem* ps = new planetary_system::PlanetarySystem;
if (ps_cats.filename.size()){
YAML::Parser parser;                                                // добавть в класс?
YAML::Node plnanet_system;
std::ifstream ast_file; // file contains asterizms
ast_file.open(ps_cats.filename.c_str());
if (ast_file.is_open()){
parser.Load(ast_file);
try{parser.GetNextDocument(plnanet_system);}catch(YAML::Exception &e) { return ps;}
if (plnanet_system.FindValue(file_fields::name)){;/*добавить имя в панетную систему*/}
if (plnanet_system.FindValue(file_fields::star)){
elementus_mundus::altus_mundus::Astrum* a = deserialization::record::getAstrum(plnanet_system[file_fields::star]);
a->coordinate.setType(coordinates::type::ecliptic);
a->setAbsId(++rec_loaded);
ps->setStar(a);
}

else ps->setStar(new elementus_mundus::altus_mundus::Astrum);
PlanetList *pl =  load::Planets(ps_cats.cats[cat_types::ps::planet],rec_loaded);
load::Sattelites(ps_cats.cats[cat_types::ps::satellite],pl,rec_loaded);
ps->setPlanets(pl);
ps->setComets(load::Comets(ps_cats.cats[cat_types::ps::comet],rec_loaded));
}}
elementus_mundus::altus_mundus::Astrum *star = new elementus_mundus::altus_mundus::Astrum;
star->absID();
ps->setStar(star);
return ps;}
}

unsigned get_cat_index(const std::string& cat_name){
unsigned i=0;
while (cat_types::ds_cats[i].length() && cat_types::ds_cats[i]!=cat_name) i++;
return i;}

/*********** CATALOGUES **************/
//-------------
Catalogues::Catalogues(const char *patchfile,const std::string *ds_cats,const std::string ps_cats[]){
loaded=0;
straux::copy(ds_cat_types,ds_cats);
straux::copy(ps_cat_types,ps_cats);
is_open=open(patchfile);}

Catalogues::Catalogues(){
loaded=0;
is_open=0;
loaded=0;}

// обработка файла-описателя астрономических каталогов
bool Catalogues::open(const char* filename){
std::ifstream cat_file(filename);
if (cat_file.is_open()){
YAML::Parser parser(cat_file);
YAML::Node main_doc;
/*initializing ds_catalogues*/
try{parser.GetNextDocument(main_doc);} catch(YAML::Exception &e) {is_open=0; return 0;}
this->ds_cats = init::cat_array(main_doc,cat_types::ds_cats);
/*initialize planet systems catalogues*/
try{parser.GetNextDocument(main_doc);} catch(YAML::Exception &e) {is_open=0; return 0;}
if (main_doc.FindValue(init::planet_system)){
const YAML::Node &ps = main_doc[init::planet_system];
for (unsigned i=0; i<ps.size(); i++){
planet_systemFF psfile; ps[i] >> psfile.filename;
this->ps_cats.push_back(psfile);}
init::planet_systems_cats(ps_cats);}
cat_file.close(); is_open=1; return 1;}
else{ /*если файл каталогов не найден*/
for  (unsigned i=0; i<ds_cat_types.size(); i++) this->ds_cats.push_back(new CatDescriptors);
this->ps_cats.resize(/*ps_cat_types.size()*/1); // какого хрена ps_cats

}
is_open=0; return 0;}

bool Catalogues::isOpen() const {return is_open;}

void Catalogues::free_catalogues(){
    /*удаление астрокаталлогов*/}

const DSCatsDescriptor Catalogues::getDSCatsInfo() const{
    return ds_cats;}

const PSCatsDescriptor Catalogues::getPSCatsInfo() const{
    return ps_cats;}


void Catalogues::del_cat_info(){
for (unsigned i=0; i<ds_cats.size(); i++)
if (ds_cats[i]!=NULL) delete ds_cats[i];}

Catalogues::~Catalogues(){
void del_cat_info();
 /*delete data*/
}

AstroData Catalogues::load_data(){
using namespace cat_types::ds;
using namespace cat_types::ps;
std::ofstream log_file(cat_log);
AstroData data;
loaded = 0;
data.global_space->last_update = QDateTime(QDate(2000,0,0));
data.planet_systems->clear();
for (unsigned i=0; this->ps_cats.size()>i; i++){
planetary_system::PlanetarySystem* ps = load::Planetary_System(this->ps_cats[i],loaded); //это здесь пока не появились каталоги дрругих СС
data.planet_systems->push_back(ps);}


try{
data.global_space->setStars(load::Astra(ds_cats[star], loaded));
data.global_space->setConstellations(load::Constellations(ds_cats[constellation],loaded));
data.global_space->setDSO(load::DSO(ds_cats[cat_types::ds::DSO],loaded));

log_file << "asterisms: "; log_file.flush();
data.global_space->setAsterisms(load::Asterizms(ds_cats[asterism], loaded, data.global_space->getStars()));
log_file << data.global_space->getAsterisms().size()<<"\n"; log_file.flush();
data.global_space->setCaelestis(new profundus_mundus::CaelestisList);
}catch(std::string er){log_file << er; log_file.close();}


log_file.close();
return data;}

}

