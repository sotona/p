﻿#include "pModel.h"
#include <stdio.h>
namespace u_model{
//возвращает угол наклона эклиптики к небесному экватору на данный момент
double EclipAnglEquator(const QDate &date){
double t = double(abs(date.daysTo(QDate(2000,1,1))) - 0.5/*полдень 1 января*/)/365.25/100.; //число столетий прошедших с 2000 года
return coordinates::converting::HorMinSecToFloat(23,26,21.448-46.815*t-0.0059*t*t+
0.00181*t*t*t);}
//-------------------------------constructor
uModel::uModel(){}
//-------------------------------destructor
uModel::~uModel(){}
//------------
void uModel::setUniverse(AstroData& u){
 universe=u;}
//-------------------------
void uModel::updateSS(){
planetary_system::PlanetarySystem* s;
for (unsigned i=0; i<universe.planet_systems->size(); i++) {
s = this->universe.planet_systems->at(i);
s->update(lifeTime.Time);}}
//-------------
bool uModel::updatePM(){
return universe.global_space->update(this->lifeTime.Time);
}

}
