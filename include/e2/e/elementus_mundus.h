﻿#ifndef UNIVERSE_H
#define UNIVERSE_H

//there is discription of universe by viewpoint of Earthling.

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <list>
#include <string>
#include <vector>
#include <map>
#include <stdio.h>
#include <QDateTime> //
#include "coordinate.h"
#include "astro_types.h"

/* see more info at eof */
namespace info{
typedef std::map<std::string,std::string> ParamValues; // [параметр] = значение.
}

/***********************   UNIVERSE   ************************/
namespace elementus_mundus{
typedef unsigned long ID_Count_type;            // тип для абсолютных индексов астрообъектов.
/* базовый астрокласс. предок всех без исключения астрообъектных классов. */
class BasicAstro{
protected:
    ID_Count_type absolute_id; // id start from 1. id=0 mean that object has no id;
                               // это абсолютный id который выдаётся классом загрузчиком.
                               // уникален у _всех_ загруженных астрообъектов.
    char *name_;
    char *comment_;             // комментарий. содержит произвольную информацию.

public:
    coordinates::Coordinate coordinate;
    BasicAstro(ID_Count_type id=0, const char* _name="", const char* comment_="");
    BasicAstro(ID_Count_type id, coordinates::Coordinate& c, const char* _name="", const char* comment_="");

    void setName(const char* _name);
    void setNote(const char* _info);
    void setAbsId(ID_Count_type);

    const char* name() const;
    const char* note() const;
    ID_Count_type absID() const;

    virtual info::ParamValues getInfo() const;      // информация в ассоциативном массиве.
    virtual char getType() const;

    ~BasicAstro();
};
/* Basic Astro with spherical body size characteristics*/
struct Sphere{
private:
 double flattening;  // сплюснутость тела. экв.радиус - полярный_радиус/экваториальный-радиус
 double polar_r;     // полярный радиус, но тут другого нет
public:
 Sphere(double r=1., double f=0.00001);
 void setPolarR(double r);          // полярный радиус
 void setFlattering(double f);      // Приплюснутость. полярный радиус / экваториальный

 double PolarR() const;
 double Flattering() const;
};

// return -1 if class ba have no sphere parameter
float radius(const elementus_mundus::BasicAstro* ba);

/*Deep space*/
namespace altus_mundus{
typedef std::pair<const char*,ID_Count_type> Cat_Id;
typedef std::vector<Cat_Id> Cats_Ids;
namespace spectral_class{
    class SpectralClass;
//    extern const char def_sc[]; //спектраьный класс по умолчанию
	// Один Бритый Англичанин Финики Жевал Как Морковь + подкласс  .например G2
        float* getColorBySpecClass(const char* spec_class);
        float* getColorBySpecClass(const SpectralClass& sc);
//----------------------------------
class SpectralClass{
   private:
        char _class; // Один Бритый Англичанин Финики Жевал Как Морковь
        unsigned short subclass; //0..9
        //more information
   public:
        SpectralClass();
        SpectralClass(const char *sc); //српектр	letter + subclass +..
        //---get
        char* toString() const;
        char getLetterOfSpClass() const;
        unsigned short getSubClass() const;
        //---set
        void setSpecClass(const char* sp); //српектр	letter + subclass +..
        void setSpClassLetter(char l);
        void setSubClass(unsigned short x);
        //more methods about additional	sp. classes and etc
        void toRGB(float *rgb) const;
};
}
const float no_vmag=65535.;
/********************************************/
class Caelestis:public BasicAstro{//lat. caelestis - celestial (небожитель, китаец :)).
protected:
Cats_Ids *id; // массив пар (ИМЯ_КАТАЛОГА, id в этом каталоге)
float vmag; //видимая велечина
coordinates::Coordinate *proper_motion; // NULL, если нет.
public:
//ctype - тип координат, галактические или экваториальные,или может быть какие ещё,
//но экваториальные 
Caelestis();
Caelestis(coordinates::Coordinate &c, float v_mag,const char* _name="");
Caelestis(ID_Count_type id, coordinates::Coordinate &c, float v_mag,const char* _name="");
~Caelestis();

//set
void setProperMotion(const coordinates::Coordinate& c);
void vMag(float vm);
void setCatIDs(Cats_Ids* cid);
//get
coordinates::Coordinate getProperMotion() const;
float vMag() const;
// получить id в каталоге catalogue
unsigned long getIDin(const char* catalogue) const; // return 0 if no in that's catalogue
info::ParamValues getInfo() const;
char getType() const;
};

/*So all the stars
Will guide us on our way*/
//----------------------------Astrum
class Astrum:public Caelestis{
private:
	char* alt_name;				//альтернативное имя звезды
	//переменнность
	//кратность
	//может быть ещё id какой добавить
public:
        Sphere sphere;
        spectral_class::SpectralClass spec_class;
        Astrum();
        Astrum(coordinates::Coordinate& c, float _bright=no_vmag,const char* specClass=NULL,const char *name_="",const char *altname="");
        Astrum(ID_Count_type id, coordinates::Coordinate& c, float _bright=no_vmag,const char* specClass=NULL,const char *name_="",const char *altname="");
        ~Astrum();
        const char* altName();
        info::ParamValues getInfo() const;
        char getType() const;
};
namespace dso{
const unsigned OpenCluster=1;
const unsigned GlobularCluster =2;
const unsigned DiffuseNebula =3;
const unsigned PlanetaryNebula=4;
const unsigned Galaxy=5;
const unsigned ClusterNebula=6;
const unsigned NonExistent=7;
const unsigned LMCobject=8;
const unsigned SMCobject=9;
const unsigned unknown=0;
class DSO:public Caelestis{
private:
unsigned type_ODS;
public:
DSO(ID_Count_type id,coordinates::Coordinate &c,float _bright=no_vmag,const char *_name="",unsigned _type_ODS=dso::NonExistent);
DSO(coordinates::Coordinate &c,float _bright=no_vmag,const char *name="",unsigned _type_ODS=dso::NonExistent);
void SetType(unsigned _type_ODS);
info::ParamValues getInfo();
char getType() const;
unsigned Type();
~DSO(){}
};}

//----------------------------
namespace sidus{
using coordinates::CoordList;
/*************/
class Sidus:public BasicAstro{
private:
 char *abbreviation_;
 coordinates::CoordList border; //список пограничных точек точек
public:
 Sidus(const char* _name="", const char* _abbreviation="");
 void add_point(coordinates::Coordinate* c);
 const char* abbreviation() const;
 const coordinates::CoordList getBorderPoints() const;
 info::ParamValues getInfo() const;
};
/*площадь созвездия.
определение попадания астрообъкта в то или иное созвездие.
сокращеное имя хранить в созвездии?*/}

}

/*Near Space*/
namespace proxima_mundus{
/*кеплеровы элементы орбиты*/
class KeplerOrbit{
QDateTime epoch;        //для этой эпохи действительно все нижеследующее. но в каком календаре?
double ep;              //Долгота в эпоху
double wp;              //Долгота перигелия
double e;               //Эксцентриситет орбиты
double a;               //Большая полуось орбиты (a.e.)
double i;               //Наклонение орбиты (градусы)
double w;               //Долгота восходящего узла(градусы)
public:
KeplerOrbit();
KeplerOrbit(double _ep, double _wp, double _e, double _a, double _i, double _w);

QDateTime getEpoch() const;
void EP(double _ep);
void WP(double _wp);
void E(double _e);
void A(double _a);
void I(double _i);
void W(double _w);

void setEpoch(const QDateTime& _epoch);
double EP() const;
double WP() const;
double E() const;
double A() const;
double I() const;
double W() const;
};

/*Base class for solar system bodys, except Sun*/
class OrbiterBody:public BasicAstro{
private:
double day_phase;   // настоящий угол поворота планеты вокруг своей оси. время местного дня в градусах.
double day_length;  // угол поворота за одни сутки
double albedo;
double eq_orbeq_orb_angle; //наклон экватора к орбите. относительно чего определяется знак?
double m; // kg.
double Tp;          // Перид обрашения(тропические годы)

public:
    Sphere sphere;
    KeplerOrbit kep_orbit;
OrbiterBody(const char *_name="", const char* note = "");
OrbiterBody(ID_Count_type id, const char *_name="", double polar_rad=1.0, double _m=0.0, double _Tp=1.0);

//set
void setKepOrbit(const KeplerOrbit& ko);
void setMass(double m);
void setTP(double _Tp);
void setDayLength(double r);
void setDayPhase(double p);
void setEqToOrbit(double eto);

//get
const KeplerOrbit& getKepOrbit() const;
double Mass() const;
double TP() const;
double DayLength() const;
double DayPhase() const;
double EqToOrbit() const;
~OrbiterBody();

info::ParamValues getInfo() const;
char getType() const;
};

class Planet;       // планета

/* кольцо для планеты.*/
struct PlanetRing{
double radius_inner;     // меньший радиус. отсчмтывается от центра планеты в километрах
double radius_outer;     //
double width;            // толщина кольца.
float  color_rgba[4];    // цвет кольца
PlanetRing();
PlanetRing(double _rad1,double _rad2,double _width,const float  _color_rgba[]):
radius_inner(_rad1),radius_outer(_rad2), width(_width){memcpy(this->color_rgba,_color_rgba, 4*sizeof(float));}
~PlanetRing(){}
};

//typedef std::vector<NSatellite*> NSatList;
typedef std::vector<PlanetRing> RingList;
typedef std::vector<Planet*> PlanetVector;

class Planet: public OrbiterBody{
private:
    RingList *rings;                 // колец список
    PlanetVector *n_satellites;          // естественные спутники
    char * star_name;               // имя звезды вокруг которой обращается планета.
public:
    // конструктор с именем и т.п.
    Planet(const char* name="");
    Planet(ID_Count_type id, const char* name="", double polar_rad=1.0);
    const char* getMajorStar() const;
    const PlanetVector* getNSats() const;
    const RingList* getRings()const{return this->rings;}
    void addNSat(Planet *nsat);
    void setRings(RingList *_Rings);
    // + сколько спутников
    ~Planet();

    info::ParamValues getInfo() const;
    char getType() const;
};

class Cometa:public OrbiterBody{
private:
    unsigned long periodic_comet_number;
    float color_rgba[4]; // цвет хвоста
    float evaporation; // безразменая величина характеризующая испарение кометы на расстоянии 1 а.е.
    QDate perihelion_passage;   // дата прохождения перигелия.
public:
    Cometa(const char *name="");
    Cometa(ID_Count_type id, const float _color_rgba[],float _evaporation);
    void setPeriodicComNumber(unsigned long id) {periodic_comet_number=id;}
    unsigned long getPeriodicComNumber() const {return periodic_comet_number;}
    void setPerihelionPassageDate(const QDate& d) {perihelion_passage=d;}
    QDate getPerihelionPassageDate() const {return perihelion_passage;}
    float getEvaporation() const {return this->evaporation;}
    void setEvaporation(float e) {this->evaporation=e;}
    //+ получение\установска цвета
    info::ParamValues getInfo() const;
    char getType() const;
};

void upd_position_planet(const QDateTime &stdt,const QDateTime &Currentdt, Planet &p);                                      //< ОБНОВИТЬ координаты планеты
coordinates::Coordinate get_position_planet(const QDateTime &stdt,const QDateTime &Currentdt,const Planet &p);                  //< просто ПОЛУЧИТЬ координаты планеты.
void get_position_nsats(coordinates::Coordinate& c,const QDateTime &curdt,Planet& nsat,char mode);
coordinates::Coordinate get_position_nsats(const QDateTime &curdt,const Planet& nsat,char mode);

/**/
float plnaetVmag(float sp_r, float op_r, float f, float a);
}}



/* сбор информации из астрономических классов */
// see namespace info alsos on the top
namespace info{
/***params***/
namespace param{
extern const std::string type;
/*basic astro*/
extern const std::string name;
extern const std::string note;
/*caelestis*/
extern const std::string vmag;
extern const std::string ids; // список, через запятую идентификаторов объекта в каталогах
/*Astrum*/
extern const std::string spec_class;
/*Sidus*/
extern const std::string abvr; // abbreveration for sidus
/*Planet*/
extern const std::string tp;
extern const std::string polar_radius;
extern const std::string rotation_period; // длительность суток
/*other*/
extern const std::string distance;
extern const std::string arc_daimetr;
}}


/*
Note on Multiple:
A = Astrometric binary
D = Duplicity discovered by occultation;
I = Innes, Southern Double Star Catalogue (1927)
R = Rossiter, Michigan Publ. 9, 1955
S = Duplicity discovered by speckle interferometry.
W = Worley (1978) update of the IDS;
*/


/*So all the stars
Will guide us on our way
The Sextant as a leader
Has duration for all days

Look at the amazing sky
In long and profund discoveries
With a strong and a clear mind he`s encryping
Secrets of astronomy

In endless nights
He observes the sky
This publications will change the world

Galileo Galilei

Only what my eyes will see, I will believe!
Day and night - separated by the light

In Pisa he`s required
To teach the theory
That the stars and all the planets
Revolve around the earth
But he believed
In a different truth
The heliocentric one
Proposed by Kopernikus
A new age had begun

The stolen sun
Made their fear rise
And man had sacrificed
A shadow was the reason why

And all the servants of the cross - they will deny
Will deny the starlight

In Pisa he`s required
To teach the theory
That the stars and all the planets
Revolve around the earth
But he believed
In a different truth
The helocentric one
Proposed by Kopernikus
A new age had begun*/

#endif
