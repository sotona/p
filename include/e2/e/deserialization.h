﻿/*
десериализация данных, хранящихся в астрономических каталогах
создание астроклассов из записей в астрокаталогах.
*/
#ifndef CAT_AUXILIARY_H
#define CAT_AUXILIARY_H
#include <string.h>
#include <string>
#include "../../../3rdparty/yaml/include/yaml.h"
#include "mundus.h"
#include "include/v/screen.h"

/* десериализация данных */
namespace deserialization{// имена параметров астрообъектов.

const unsigned Dbuff_len = 1024; // размер буфера чтения из файла [число символов]
typedef std::vector<unsigned long> ul_vector;   // для массива id звёзд
typedef std::vector<ul_vector> ul_vect_2d;
/* работа только с форматами данных */
namespace formats{
struct UnitMask {
std::string param;
unsigned position;
unsigned length;};
typedef std::vector<UnitMask>                         FormatOfRecord;        //формат записи в файле. вектор <имя_поля <позиция, длинна>>.
typedef std::map<std::string,char*>                        ValueMap;             //[имя_параметра] , значение_параметра
// распознование маски данных. mask - неразобраная маска. main_dif - знак разделения параметров.
// param_mask - символы разделения имени_параметра[0]начал_данных[1]длинны_данных. например ":."
// маска в этом случае выглядит так: имя_звезды.0:16
FormatOfRecord recognize_format(const std::string& raw_format,char main_dif,const char* param_mask);
}
/* разбор записей в файле в содержимое класса */
namespace record{
formats::ValueMap get_values(const char *raw_string,const formats::FormatOfRecord& format); //создаёт ассоциативный массив("параметр" - значение)
using elementus_mundus::altus_mundus::Astrum;
using elementus_mundus::altus_mundus::dso::DSO;
using profundus_mundus::Asterizm;
using elementus_mundus::proxima_mundus::Planet;
using elementus_mundus::proxima_mundus::Cometa;
using elementus_mundus::altus_mundus::sidus::Sidus;
using coordinates::converting::HMStoFloat;

// d_width - толщина по умолчанию, d_color - rgb цвет по умолчанию
Asterizm* makeAsterizm(const YAML::Node& node, const profundus_mundus::AstraList& al, float d_width, float *d_color);

Astrum* getAstrum(const YAML::Node& s);
Astrum* getAstrum(const char* str_astrum,const formats::FormatOfRecord& record_format);
Planet* getPlanet(const formats::ValueMap& values);                   // эта функция принемает правильные параметры. остальные нет
Cometa* getComet(const char* raw_data,const formats::FormatOfRecord& record_format);
Sidus* getConstellation(const char* raw_data,const formats::FormatOfRecord& record_format);
DSO* getDSO(const char* raw_data,const formats::FormatOfRecord& record_format);
// получаем парметры по умолчанию для астеризмов.
bool asterizm_default(const YAML::Node &node, float &d_width, float *d_color);

// прочие парсеры
bool getColWidth(screen::col_wid& cw,const YAML::Node& n);
bool getColWidthStr(screen::col_wid_str& cws, const YAML::Node& n);
}

namespace file{
using profundus_mundus::AstraList;
using profundus_mundus::DSOList;
using planetary_system::PlanetList;
using planetary_system::CometList;
using profundus_mundus::SidusList;
using elementus_mundus::ID_Count_type;
//------- загрузка звёзд. из filename в al. al!=NULL
bool stars(const char* filename, const formats::FormatOfRecord& record_format, AstraList* al, ID_Count_type &id);
bool planets(const char* filename, const formats::FormatOfRecord& record_format, PlanetList* pl, ID_Count_type &id);
bool comets(const char* filename, const formats::FormatOfRecord& record_format, CometList* cl, ID_Count_type &id);
bool constellation(const char* filename, const formats::FormatOfRecord& record_format, SidusList* sl, ID_Count_type &id);
bool odss(const char* filename, const formats::FormatOfRecord& record_format, DSOList* odsl, ID_Count_type &id);
bool satellites(const char* filename, const formats::FormatOfRecord& record_format, PlanetList* pl, ID_Count_type &id);}
}

namespace cat_aux{
using namespace coordinates;
Coordinate* coordinateById(const profundus_mundus::AstraList &al, unsigned long id);
// список_цепочек_из_имён_звёзд + список_звёзд = список_цепочек_из_координат_звёзд.
std::list<CoordList*> makeLinesList(const deserialization::ul_vect_2d &ll,const profundus_mundus::AstraList &al);
profundus_mundus::AstraListsVector makeAstroPattern(const deserialization::ul_vect_2d &ll,const profundus_mundus::AstraList &al);
char** getShitList(char* line, char c); //разделяет строку на массив строк. разделяющий символ - с.
std::list<char**> getLineList(char *s);
}

#endif // CAT_AUXILIARY_H
