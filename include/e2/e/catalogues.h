﻿/*
работа с астрономическими каталогами.
астрономический каталог - список файлов, и их форматов для отпределённого типа астрообъектов.
загрузка данных в списки_астроклассов.

*/
#ifndef CATALOGUES_H
#define CATALOGUES_H
#include <vector>
#include <map>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "yaml.h"
#include "mundus.h"
#include "deserialization.h"
#include "astro_db.h"
#include "cat_definitions.h"

/* работа с каталогами*/
namespace catalogues{
/*маска для описания формата*/
namespace mask{
const char separator = ' ';         // разделитель для разных параметров маски
const char param_mask[] = ".:";      // параметр[0]позиция[1]длинна
}

typedef std::pair<std::string,std::string>                 File_rawDataFormat;      // файл - маска_данных.
typedef std::vector <File_rawDataFormat>         FrDF_array;              // вектор <путь_к_файлу, маска> File-rawDataMask
struct CatDescriptor {std::string cat_name;                                       // имя каталога
                    elementus_mundus::ID_Count_type total_records;              // общее число записей
                    deserialization::formats::FormatOfRecord format;            // формат каталога
                    std::vector<std::string> filename;                          // файлы каталога
                    std::vector<elementus_mundus::ID_Count_type> file_records;  // число записей по файлам каталога
                   }; // формат - список файлов
struct CatDescriptors{        // вектор [формат_записи_в_файле - имя файла]
std::vector<CatDescriptor> cat_descriptor;
elementus_mundus::ID_Count_type total_records;
};

/*файл - количество записей*/
struct file_records{    
std::string filename;
elementus_mundus::ID_Count_type records;};
/*каталог для одного типа астрообъектов - (число записей, список файлов и количества записей)*/
struct cat_records{
elementus_mundus::ID_Count_type total;      // чило записей в нём
std::vector<file_records> files;            // список (имя файла -  число записей)
};
/*планетная система - записей в каталогах*/
struct ps_cat_records{
std::string name;                           // имя планетной системы
std::vector<cat_records> cats;              // описание описывающих его каталогов
};
/*информация о каталогах планетной системы*/
struct planet_systemFF{
std::string filename;
std::vector<CatDescriptors*> cats;};

typedef std::vector<CatDescriptors*>      DSCatsDescriptor;
typedef std::vector<planet_systemFF>        PSCatsDescriptor;

/*инициализация данных о каталогах*/
namespace init{
CatDescriptors* cat(const YAML::Node& cat_node);            // get catalogues files and formats
/*создаёт массив описателей каталогов, node - узел с описанием каталогов
имя_каталога1: ...
имя_каталога2: ...
cat_types - заканчивающийся пустой строкой массив имен обрабатываемых каталогов. */
std::vector<CatDescriptors*>  cat_array(const YAML::Node& cat_node,const std::string *cat_types);
std::vector<planet_systemFF> planet_systems_cats(const YAML::Node &cat);
}

namespace errors{
const unsigned NO_ERRR=0;
//const enum error {NO_ERROR, FILE_NOT_FOUND, READING_FILE_ERROR};
}

namespace load{
planetary_system::PlanetarySystem *Planetary_System(elementus_mundus::ID_Count_type& rec_loaded);
profundus_mundus::AstraList* Astra(CatDescriptors* ff, elementus_mundus::ID_Count_type& rec_loaded);
profundus_mundus::AsterismList* Asterizms(const CatDescriptors* ff, elementus_mundus::ID_Count_type& rec_loaded, const profundus_mundus::AstraList& al) ;              // загрузка астеризмов
planetary_system::PlanetList* Planets(const CatDescriptors* ff, elementus_mundus::ID_Count_type& rec_loaded);
profundus_mundus::SidusList* Constellations(const CatDescriptors* ff, elementus_mundus::ID_Count_type& rec_loaded);                               // загрузка созвездий
planetary_system::CometList* Comets(const CatDescriptors* ff, elementus_mundus::ID_Count_type& rec_loaded);
profundus_mundus::DSOList* DSO(const CatDescriptors* ff,elementus_mundus::ID_Count_type& rec_loaded);
}

/*отдельные каталоги должны загружатся сразу целиком (даже если сосстоят из нескольких файлов).
но допускается непоследовательная загрузка частей одного каталога или солнечной системы. главное чтобы индексы внутри отдельного каталога не прерывались.*/
class Catalogues{
private:
    DSCatsDescriptor ds_cats; // где лежат файлы каталогов и какой имеют формат. индексы - cat_types::dsa_cats
    PSCatsDescriptor ps_cats; // массив планетных систем

    //std::vector<cat_records> ds_cat_info;       // информация о каталогах дальнего космоса. индексы - cat_types::dsa_cats
    //std::vector<ps_cat_records> ps_cat_info;    // информация о каталогах планетных систем

    std::vector<std::string> ds_cat_types;  // ??
    std::vector<std::string> ps_cat_types;

    elementus_mundus::ID_Count_type loaded;             //REcord loadED. всего загруженных записей

    bool is_open;

    void del_cat_info();                                        // очищает список файлов каталогов и форматов.
    void free_catalogues();

    void init_planet_systems_cats(const YAML::Node * n_rec);

public:
    Catalogues();
    /*путь к файлу описателю каталогов,
    ds_cats - массив типов каталого глубокого космоса и звёзд
    ps_cats - массив
    */
    Catalogues(const char *patchfile,const std::string *ds_cats=cat_types::ds_cats,const std::string ps_cats[]=cat_types::ps_cats);
    ~Catalogues();
    bool open(const char* filename); // open main cat's file and return true if all right.
    AstroData load_data();

    bool isOpen() const;

    const DSCatsDescriptor getDSCatsInfo() const;
    const PSCatsDescriptor getPSCatsInfo() const;
};


}

#endif // CATALOGUES_H
