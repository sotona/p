﻿#ifndef ASTRO_DB_H
#define ASTRO_DB_H
#include "elementus_mundus.h"
#include "mundus.h"

/*  */
struct AstroData{
    std::vector<planetary_system::PlanetarySystem*> *planet_systems;
    profundus_mundus::ProfundusMundus *global_space;
    AstroData();
    AstroData(const AstroData& ad);
    const planetary_system::PlanetarySystem& getNativeSS() const;   // родная СС
    planetary_system::PlanetarySystem* nativeSS();
};
typedef std::pair<elementus_mundus::ID_Count_type,elementus_mundus::ID_Count_type> FirUnt;                  // FIRst,coUNT
typedef std::vector<std::pair<elementus_mundus::ID_Count_type, elementus_mundus::ID_Count_type> > IndArray; // first index, count
struct ps_indexes{
FirUnt firunt;      // first id, count
IndArray array;
};

/* индексы начинаются с 1. */
class AstroDB{
private:
    struct {
    struct {
    FirUnt firunt;
    IndArray array;             /*массив <first index, count> для глубокого космоса*/} deep_sky;
    std::vector<ps_indexes> plnet_systems;    // т.с. но для поанетных систем
    } b_id; /*boundaries if of different lists of astroobjects*/

    void initIDsArray(); // инициализация массива индексов
    elementus_mundus::ID_Count_type sel_id;
    short typeOfID(elementus_mundus::ID_Count_type id) const;                            // return -1 if not in db
public:
    AstroDB(const AstroData& data_);
    ~AstroDB();

    const AstroData *data;
    const elementus_mundus::BasicAstro* selected;
    info::ParamValues selected_info;
    const std::list<elementus_mundus::BasicAstro*>& findAstras(const char *find_line);
    const std::list<elementus_mundus::BasicAstro*>& findAsterizm(const char *find_line);
    const std::list<elementus_mundus::BasicAstro*>& findPlanet(const char *find_line);
    const std::list<elementus_mundus::BasicAstro*>& findDSO(const char *find_line);
    const std::list<elementus_mundus::BasicAstro*>& findComet(const char *find_line);
    //const std::list<elementus_mundus::BasicAstro*>& findAsteroids(const char *find_line);
   // std::list<elementus_mundus::BasicAstro*>* find_astro(char *find_line);
  //  std::list<elementus_mundus::BasicAstro*>* find_global_space(char *find_line);

    elementus_mundus::ID_Count_type getActiveID();
    void activateID(elementus_mundus::ID_Count_type id);
    void setNoAsctiveId();

    const char *nameOfID(elementus_mundus::ID_Count_type) const;
    void updateInfo();

 };

#endif // ASTRO_DB_H
