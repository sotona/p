﻿#include "vivus_tempus.h"
#include <QObject>



namespace info{
namespace param{
namespace tempus{
const std::string time = "time";
const std::string timespeed = "timespeed";
const std::string is_stopped = "is stopped";
}}}

namespace tempus{
const int min_year = -15000;
const int max_year =  15000;
const float max_X    =  1E10;


VivusTempus::VivusTempus(short _timezone){
timezone=_timezone;
Time = QDateTime::currentDateTime();
bool x =connect(&timer, SIGNAL(timeout()),SLOT(tick()));
timer.start(1000);
delta=d_delta;}
//-----------------------------------
void VivusTempus::tick(){
QDateTime test = Time.addMSecs(delta);
if (test.date().year()<max_year && test.date().year()>min_year)Time=test;
timer.start(100);}
//-----------------------------------
void VivusTempus::X(float x){
if (fabs(x)<max_X)
this->delta=timer.interval()*x;}
//-----------------------------------
float VivusTempus::X() const{
return delta/timer.interval();}
//-----------------------------------
void VivusTempus::stort(bool stort){
if (stort) timer.start();
else timer.stop();}
//-------------
void VivusTempus::stort(){
if (timer.isActive()) timer.stop();
else timer.start();}

//-----------------------------------
bool VivusTempus::is_stort() const{
return timer.isActive();}
//-----------------------------------
float VivusTempus::toHourf() const{
return converting::HorMinSecToFloat(Time.time().hour(),
 Time.time().minute(), Time.time().second());}
//-----------------------------------
float VivusTempus::toGMTf() const{
//+учёт летнего времени
return toHourf()-timezone;}
//-----------------------------------
void VivusTempus::setCurrentTime(){
Time = QDateTime::currentDateTime();
}
}
















//namespace tempus{
//
//    Tempus::Tempus(unsigned _annus, unsigned _mensis, unsigned _dies,
//                    unsigned _hora, unsigned _minutus, unsigned _secundus,  float _milisecundus){
//        annus=_annus;
//        mensis=_mensis;
//        dies=_dies;
//
//        hora=_hora;
//        minutus=_minutus;
//        secundus=_secundus;
//        milisecundus=_milisecundus;}
////------------------------------------------
//    Tempus Tempus::operator += (Tempus t){
//    milisecundus+=t.milisecundus;
//    secundus+=t.secundus;
//    minutus+=t.minutus;
//    hora+=t.hora;
//    dies+=t.dies;
//    mensis+=t.mensis;
//    annus+=t.annus; }
////------------------------------------------
//void Tempus::plus_msec(unsigned msec){
//if (milisecundus+msec>1000) plus_msec(msec/1000);
//milisecundus+=msec%1000;}
////------------------------------------------
////------------------------------------------
////------------------------------------------
//    VivusTempus::VivusTempus(Tempus _t, Tempus _vivus){
//        t=_t; vivus=_vivus;}
////------------------------------------------
//    void VivusTempus::start(){
//        s=1;
//        fluctus();}
////------------------------------------------
//    void VivusTempus::stop(){
//        s=0;}
////------------------------------------------
//    void VivusTempus::fluctus(){
////        while(s){
////        this->t.milisecundus+=vivus.hora;
////        this->t.hora+=vivus.hora;
////        this->t.hora+=vivus.hora;
////        this->t.hora+=vivus.hora;
////        this->t.hora+=vivus.hora;
////        this->t.hora+=vivus.hora;
////        this->t.hora+=vivus.hora;
////        this->t.hora+=vivus.hora;
////        this->t.hora+=vivus.hora;}
//    }
//}
