﻿/*
объявления имён параметров, использующихся в каталогах
и т.п.
*/

#ifndef CAT_DEFINITIONS_H
#define CAT_DEFINITIONS_H
#include <string>

namespace cat_types{
// все константы и строки в массиве в алфавитном порядке. что, сука, облегчает добаление, но зато их можно сравнивать strcmp.
// NULL terminated массив - всевозможные типы каталогов. нужен для класса Catalogues, который будет искать только эти типы каталогов
// в файле описания каталогов.

const std::string ds_cats[] = {"asterism", "caelestis",  "constellation","star","dso",""};// Deep Sky, Astra catalogues types
const std::string ps_cats[] = {"central_star","asteroid", "comet","planet","satellite",""};// planet system cats. "central star" is fiction catalogue

// this consants MUST be correspond indexes of cat_types array;
// что-то типовдохуя
// эти типы должны использоватся в методе GetType/
// вероятно нужно связать чвойства чисел (чётность, число еденичных бит) астрономический смысл
namespace ds{
const unsigned short asterism =         0;
const unsigned short caelestis =        1;
const unsigned short constellation =    2;
const unsigned short star =             3;
const unsigned short DSO =              4;
}
namespace ps{
const unsigned short cental_star =      0;
const unsigned short asteroid =         1;
const unsigned short comet =            2;
const unsigned short planet =           3;
const unsigned short satellite=         4;}}

namespace file_fields{
extern const char *star;
extern const char *name;
extern const char *alt_name;
extern const char *type;
/*coordinates*/
extern const char *RA;
extern const char *declination;
extern const char *parallax;
extern const char *r;
namespace phis_characteristic{
extern const char *polarR;
extern const char *flattering;
extern const char *spec_class;
extern const char* abs_vmag;
}

extern const char *day_length;
extern const char* day_phase;

namespace proper_motion{
extern const char *RA;
extern const char *declination;}

extern const char *tp;                  // период обращения. годы
extern const char *tp_d;                // дни
extern const char *eqtec;               // наклон экватора к орбите
namespace orbital_elements{
extern const char *semi_major_axis;
extern const char *semi_major_axis_km;
extern const char *eccentricity;
extern const char *inclination;
extern const char *per_longitude;   // долгота перицентра
extern const char *an_longitude;    // долгота восходящего узла
extern const char *ep_longitude;   // долгота в эпоху
extern const char *mean_anomaly;
}
/**/
extern const char *vmag;
namespace cat_name{
extern const char *hd;
extern const char *hr;
extern const char *sao;
extern const char *ngc;
extern const char *ic;}

namespace asterism{
extern const char* name;
}

namespace graph{
extern const char *factor;
extern const char *width;
extern const char *color;
extern const char *pattern;}
}

#endif // CAT_FIELDS_H
