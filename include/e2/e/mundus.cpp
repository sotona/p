﻿#include "mundus.h"

namespace profundus_mundus{
Caelestis* get_max_vmag(CaelestisList* lst){
if (lst->empty()) return NULL;
Caelestis *c=lst->front();
for (CaelestisList::iterator it=lst->begin(), end=lst->end(); it!=end; it++)
if ((*it)->vMag()<c->vMag()) c=*it;
return c;}
//---------------------------------------------------------------ProfundusMundus
ProfundusMundus::ProfundusMundus(){
init_lists();}
//---------------
void ProfundusMundus::init_lists(){
    sidusis = new SidusList;
    astras = new AstraList;
    caelestis = new CaelestisList;
    asterisms = new AsterismList;
}
//----------------
bool ProfundusMundus::update(const QDateTime &d){
AstraList::iterator ait = this->astras->begin();
AstraList::iterator end = this->astras->end();
elementus_mundus::altus_mundus::Astrum *a;
int y = fabs(d.date().year() - last_update.date().year());
if (y>33){                                                     // 33 должно быть вычисляемым параметром
for (;ait!=end; ait++){
a = *ait;
coordinates::Coordinate proper_motion = a->getProperMotion();
a->coordinate.Alpha(a->coordinate.Alpha()+proper_motion.Alpha()*y);
a->coordinate.Sigma(a->coordinate.Sigma()+proper_motion.Sigma()*y);}
last_update = d;
return 1;} else return 0;}

void ProfundusMundus::setAsterisms(AsterismList *al){
this->asterisms = al;}
void ProfundusMundus::setConstellations(SidusList* s){
if (s!=NULL) sidusis=s;}
//--------------------------------------------------------------
void ProfundusMundus::setCaelestis(CaelestisList* cl){
if (cl!=NULL) caelestis=cl;}
void ProfundusMundus::setDSO(DSOList *ods){
this->dso = ods;}


//--------------------------------------------------------------
void ProfundusMundus::setStars(AstraList *al){
if (al!=NULL) astras=al;}

/*//--------------------------------------------------------------
AstraList* Sidus::getStars(){
return astras;}*/

//---------------------------------------------------------------Show Astrum 
const AstraList& ProfundusMundus::getStars() const{
return *astras;}
const DSOList& ProfundusMundus::getDSOs() const{
return *dso;}
//---------------------------------------------------------------
const CaelestisList& ProfundusMundus::getCaelestis() const{
return *caelestis;}
//---------------------------------------------------------------
const SidusList& profundus_mundus::ProfundusMundus::getConstellations() const{
return *sidusis;}
//----------------------------------
const Sidus* profundus_mundus::ProfundusMundus::getConstellation(const char* sidus_name) const{
elementus_mundus::altus_mundus::sidus::Sidus *s;
for (SidusList::const_iterator it=this->sidusis->begin(), end=sidusis->end(); it!=end; it++){
s=*it;
if (!strcmp(s->name(),sidus_name)) return s;}
return NULL;}
//------------------
const AsterismList& ProfundusMundus::getAsterisms() const {return *asterisms;}

/****** Asterism *******/
Asterizm::Asterizm(){
memset(color,0,3*sizeof(float));
line_width=1;}

Asterizm::~Asterizm(){}

//проверку данных, Сергей Владмирович говорит "забей", и я "забиl", но потом нужно как то сделать
Asterizm::Asterizm(const char *_name, float _line_width,float r,float g,float b):BasicAstro(0,_name){
    line_width=_line_width; color[0]=r;color[1]=g;color[2]=b;}

void Asterizm::Line_width(const float _line_width){
if(_line_width>=0)line_width=_line_width; else line_width=0;}

void Asterizm::setColor(float r,float g,float b){
if (r>=0&&b>=0&&g>=0){color[0]=r;color[1]=g;color[2]=b;}
else{memset(color,0,3*sizeof(float));}}

void Asterizm::setLines(const std::list<CoordList*>& _lines){lines=_lines;}

void Asterizm::setCenter(const coordinates::Coordinate &c){
center = c;}

coordinates::Coordinate Asterizm::getCenter() const{
return center;}
} /*end of profundus mundus*/
/***************************************/
namespace planetary_system{
using coordinates::CoordList;
using elementus_mundus::proxima_mundus::Planet;
using elementus_mundus::proxima_mundus::Cometa;
CoordList* make_orbit(const Planet& p,const QDateTime cur,const QDateTime &epoch, unsigned segments){
const long days=24*60*60;
CoordList *orbit = new CoordList;
QDateTime time = cur;
//if (segments>365 || segments==0) segments=365;
unsigned seg=segments*p.TP();
for (unsigned i=0; i<seg; i++){
//coordinates::Coordinate* c=elementus_mundus::get_position_planet(epoch,time,p);
coordinates::Coordinate* c = new coordinates::Coordinate(get_position_planet(QDateTime(QDate(1999,12,31)), time,p));
orbit->push_back(c);
time=time.addSecs(365./segments*days);}
return orbit;}
//-------------------
CoordList* make_orbitl(const Planet& p,const QDateTime cur,const QDateTime &epoch, unsigned segments){
const long days=24*60*60;
CoordList *orbit = new CoordList;
QDateTime time = cur;
//if (segments>365 || segments==0) segments=365;
unsigned seg=segments*p.TP();
for (unsigned i=0; i<seg; i++){
//coordinates::Coordinate* c=elementus_mundus::get_position_planet(epoch,time,p);
orbit->push_back(new coordinates::Coordinate(elementus_mundus::proxima_mundus::get_position_planet(epoch, time,p)));
time=time.addSecs(365./segments*days);}
return orbit;}
//-----------------------------
std::list<CoordList*>* make_orbits(const planetary_system::PlanetList& pl,const QDateTime &ct,const QDateTime& epoch){
std::list<CoordList*> *orbits = new std::list<CoordList*>();
for (planetary_system::PlanetList::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
const elementus_mundus::proxima_mundus::Planet *p=*it;
CoordList *cl=make_orbit(*p, ct, epoch,365);
orbits->push_front(cl);}
return orbits;}

//*******************************************************************
//CoordList* make_orbit(const Cometa& comet,const QDateTime cur,const QDateTime &epoch, unsigned segments){
//const long days=24*60*60;
//CoordList *orbit = new CoordList;
//QDateTime time = cur;
////if (segments>365 || segments==0) segments=365;
//unsigned seg=segments*comet.TP();
//for (unsigned i=0; i<seg; i++){
////coordinates::Coordinate* c=elementus_mundus::get_position_planet(epoch,time,p);
//coordinates::Coordinate* c=elementus_mundus::proxima_mundus::get_position_comet(time,comet,1E-10,6,1950.);
//orbit->push_back(c);
//time=time.addSecs(365./segments*days);}
//return orbit;}
//-----------------------------------------------------
//std::list<CoordList*>* make_orbits(const CometList& cl,const QDateTime &ct,const QDateTime& epoch){
//std::list<CoordList*> *orbits = new std::list<CoordList*>();
//for (planetary_system::CometList::const_iterator it=cl.begin(), end=cl.end(); it!=end; it++){
//elementus_mundus::proxima_mundus::Cometa *comet=*it;
//CoordList *coorl=make_orbit(*comet, ct, epoch,365);
//orbits->push_front(coorl);}
//return orbits;}

/* класс солнечная система */
PlanetarySystem::PlanetarySystem(PlanetList *p, CometList *c,elementus_mundus::altus_mundus::Astrum *star_){
planets=NULL; setPlanets(p);
comets=NULL; setComets(c);
// а если звезды нет?
/*star=NULL;*/ setStar(star_);
if (planets==NULL) planets = new PlanetList;
if (comets==NULL) comets = new CometList;}
//-----------
void PlanetarySystem::setComets(CometList *c){
    if (c!=NULL) comets=c;}
void PlanetarySystem::setPlanets(PlanetList *p){
    if (p!=NULL) {planets->clear();
        planets->insert(planets->begin(),p->begin(), p->end());
    }}
void PlanetarySystem::setStar(elementus_mundus::altus_mundus::Astrum *star_){
    if (star_!=NULL) star=star_;}
//---------------
const PlanetList& PlanetarySystem::getPlanets() const{
    return *planets;}
const CometList& PlanetarySystem::getComets() const{
    return *comets;}
const elementus_mundus::altus_mundus::Astrum* PlanetarySystem::getStar() const{
    return star;}
//-------------
void PlanetarySystem::update(const QDateTime &d){
update_planets(d);
//update_comets(d);
}
void PlanetarySystem::update_comets(const QDateTime &d){
//QDateTime j(QDate(2000,1,1),QTime(12,0,0));
//for (CometList::iterator it=comets->begin(), end = comets->end();it!=end;it++)
//(*it)->coordinate=*elementus_mundus::proxima_mundus::get_position_comet(d,**it,0.000001,6,(*it)->getPerihelionPassageDate().year());
}

void PlanetarySystem::update_planets(const QDateTime &d){
    elementus_mundus::proxima_mundus::Planet *p;
    QDateTime j(QDate(2000,1,1),QTime(12,0,0));
    for (PlanetList::iterator it=planets->begin(), end =planets->end();it!=end;it++){
    p=*it;
    //Coordinate *c=elementus_mundus::get_position_planet(j,lifeTime.Time,*p);
    elementus_mundus::proxima_mundus::upd_position_planet(QDateTime(QDate(1999,12,32)),d,*p);
    double days_left=j.secsTo(d)/86400.;
    double df=fmod(days_left,p->DayLength()) /*+начальная фазза*/;
    df=df/p->DayLength()*360.;
    p->setDayPhase(df);
    for(int i=0;i<p->getNSats()->size();i++){

   p->getNSats()->at(i)->coordinate=elementus_mundus::proxima_mundus::get_position_planet(j,d,*p->getNSats()->at(i));}
    }}
}
