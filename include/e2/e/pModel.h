﻿#ifndef PMODEL_H
#define PMODEL_H

#include "tempus.h"
//#include "whatIsee.h"
#include "vivus_tempus.h"
#include "mundus.h"
#include "astro_db.h"

namespace u_model{
double EclipAnglEquator(const QDate &date); //возвращает угол наклона эклиптики к небесному экватору на данный момент

using coordinates::Coordinate;
using coordinates::CoordList;
using profundus_mundus::AstraList;
using profundus_mundus::ProfundusMundus;
using planetary_system::PlanetList;
using planetary_system::CometList;

Coordinate* coordinateByName(const AstraList &al, char*name);

std::list<CoordList*>* makeLinesList(const std::list<char**> &ll,const AstraList &al);


/*класс планетарий*/
class uModel{
private:
    AstroData universe;
    QDate ds_lu; //date of deep sky coordinates last update
    QDateTime ss_lu; // date of solar system(S) last updete
public:
    tempus::VivusTempus lifeTime;

    uModel();
    ~uModel();

    void setUniverse(AstroData &u);

    void updateSS(); //пересчитывает положение объектов солнечной системы.
    bool updatePM(); //пересчитывает положение дипская (если необходимо). возвращает 1 если координаты пересчитаны.
};
}
#endif // PMODEL_H


/*


*/
