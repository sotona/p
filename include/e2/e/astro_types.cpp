#include "astro_types.h"

namespace astro_types{
const char basic_astro = 0;
const char caelestis = 0x80;    // 1000 0000
const char astrum =    0xC0;    // 1100 0000
const char dso =       0xA0;    // 1010 0000 (like cluster and nebula)
const char orbiter =   0x90;    // 0001 0000
}
