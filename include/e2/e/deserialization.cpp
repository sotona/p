﻿#include "deserialization.h"
#include "cat_definitions.h"
#include "../../straux.h"

/* разбор данных из каталогов. десериализация.*/
namespace deserialization{
// имена параметров астрообъектов.

/* работа с форматами записей в файлах  */
namespace formats{
using std::string;
UnitMask parseMaskElement(const string& s, const char* mask){ //exmpl. param_mask = ".:" .
UnitMask em;
unsigned m1p=s.find_first_of(mask[0],0);
unsigned m2p = s.find_first_of(mask[1],0);
em.param = s.substr(0,m1p);
string s1=s.substr(m1p+1,m2p-m1p-1);
string s2=s.substr(m2p+1,s.size()-m2p-1);
em.position = atoi(s1.c_str());
em.length = atoi(s2.c_str());
return em;}
//-------
FormatOfRecord recognize_format(const string& raw_format,char main_dif,const char* param_mask){ //ex. param_mask = ".:"
using namespace straux;
FormatOfRecord difmask;
string mask=raw_format;
mask+=" ";
while (mask.length()){
string s=mask.substr(0,mask.find_first_of(main_dif));
string tmp=mask.substr(mask.find_first_of(main_dif)+1,mask.size());
mask=trim::begin(tmp,' ');
UnitMask em = parseMaskElement(s,param_mask);
difmask.push_back(em);
}return  difmask;}
//--------
}
/* разбор записей в файле в содержимое класса */
namespace record{
/*не обязательно передавать формат. логичнее разбить строку на поля до вызова функции*/
formats::ValueMap get_values(const char *raw_string,const formats::FormatOfRecord& format){
formats::ValueMap param_value;
char *tmp;
for(unsigned i=0;i<format.size();i++){
    tmp=new char[format[i].length+1];
    memset(tmp, 0, format[i].length+1);
    memcpy(tmp,raw_string+format[i].position,format[i].length);
    straux::trim::beginend(tmp,' ');
    param_value[format[i].param]=tmp;}
return param_value;}

Astrum* getAstrum(const YAML::Node &s){
using namespace file_fields;
using namespace file_fields::phis_characteristic;
Astrum *a = new Astrum();
std::string buff="";
double x;           /*self params*/
if (s.FindValue(name))          {s[name] >> buff;       a->setName(buff.c_str());}
if (s.FindValue(flattering))    {s[flattering] >> x;    a->sphere.setFlattering(x);}

if (s.FindValue(polarR))        {s[polarR] >> x;        a->sphere.setPolarR(x);/**/}

if (s.FindValue(spec_class))    {s[spec_class] >> buff; a->spec_class.setSpecClass(buff.c_str());}
if (s.FindValue(abs_vmag))      {s[abs_vmag] >> x;      a->vMag(x);}
/*coordinates*/
if (s.FindValue(RA))            {s[RA] >> x;            a->coordinate.Alpha(x);}
if (s.FindValue(declination))   {s[declination] >> x;   a->coordinate.Sigma(x);}
if (s.FindValue(r))             {s[r] >> x;             a->coordinate.Range(x);}
return a;}
//------------------
Asterizm* makeAsterizm(const YAML::Node& node, const profundus_mundus::AstraList& al, float d_width, float *d_color){
using namespace straux;
using namespace profundus_mundus;
std::string name;
float width;
float color[3];
node[file_fields::asterism::name] >> name;
if (node.FindValue(file_fields::graph::width)!=NULL) node[file_fields::graph::width] >> width;
else width = d_width;
if (node.FindValue(file_fields::graph::color)!=NULL){
const YAML::Node& col_n=node[file_fields::graph::color];
for (unsigned short i=0; i<3; i++) col_n[i]>>color[i];}
else memcpy(color, d_color, 3*sizeof(float));
profundus_mundus::Asterizm *ast = new profundus_mundus::Asterizm(name.c_str(),width,color[0], color[1], color[2]);
const YAML::Node& lines_n=node["lines"];
ul_vect_2d id_lines; // массив линий. линия = список id. на данный момент HR.
for (unsigned i=0; i<lines_n.size(); i++){
ul_vector ids_line; // список id звёзд т.е. ломаная линия на небосводе
const YAML::Node& line_n=lines_n[i];
for (unsigned int j=0; j<line_n.size(); j++){
unsigned long id =  line_n[j];
ids_line.push_back(id);}
id_lines.push_back(ids_line);}
ListOfCoordList coord_lines = cat_aux::makeLinesList(id_lines,al);
ast->pattern = cat_aux::makeAstroPattern(id_lines,al);
ast->setLines(coord_lines);

/*getting center of asterism*/
double alpha=0, sigma=0;
unsigned count=0;
for (ListOfCoordList::const_iterator l_it = ast->getLines().begin(), l_end = ast->getLines().end(); l_it!=l_end; l_it++){
coordinates::CoordList *cl = *l_it;
    for (coordinates::CoordList::const_iterator it = cl->begin(), end = cl->end(); it!=end; it++){
    alpha+=(*it)->Alpha();
    sigma+=(*it)->Sigma(); count++;}}
ast->setCenter(coordinates::Coordinate(alpha/count, sigma/count,1));
return ast;}

elementus_mundus::altus_mundus::dso::DSO* getDSO(const char* raw_data,const formats::FormatOfRecord& record_format){
using namespace straux; using namespace coordinates::converting;
using namespace elementus_mundus::altus_mundus;
formats::ValueMap param_value=record::get_values(raw_data,record_format);
coordinates::Coordinate c=coordinates::Coordinate(HMStoFloat(param_value[file_fields::RA])
        ,HMStoFloat(param_value[file_fields::declination]),1,coordinates::type::equator);
Cats_Ids *cid = new Cats_Ids;
Cat_Id ci(file_fields::cat_name::ngc, atol(param_value[file_fields::cat_name::ngc]));
cid->push_back(ci);
float vmag = atof(param_value[file_fields::vmag]);
if (vmag<=1) vmag = 9+0.1*(rand()%60);
DSO* ods = new DSO(c,vmag,"",atoi(param_value[file_fields::type]));
ods->setCatIDs(cid);
return ods;}
//--------------------
Astrum* getAstrum(const char* str_astrum,const formats::FormatOfRecord& record_format){
using namespace straux;
using namespace elementus_mundus::altus_mundus;
Astrum* a=NULL;
int state=0;
formats::ValueMap param_value=record::get_values(str_astrum,record_format);
if (!param_value.size()) return a;
double r = 8;
char name[32];
if (param_value.count(file_fields::parallax)){
strcpy(name,param_value[file_fields::parallax]);
if (strlen(param_value[file_fields::parallax])){
r = atof(param_value[file_fields::parallax])/3600.;
r = sin(r*coordinates::converting::toRad/2);
if (r>0.0000000000001){
r = coordinates::astronomical_unit/r;
r/=coordinates::parsec;} else r = (rand()%800+400.)/100;}}
try{
state =1;
coordinates::Coordinate c=coordinates::Coordinate(coordinates::converting::HMStoFloat(param_value[file_fields::RA]),
coordinates::converting::HMStoFloat(param_value[file_fields::declination]),fabs(r), coordinates::type::equator);
state=2;
memset(name, 0, 32);
Cats_Ids *cid = new Cats_Ids;
Cat_Id ci(file_fields::cat_name::hr, atol(param_value[file_fields::cat_name::hr])); // HR is BSC star number
cid->push_back(ci);
ci=Cat_Id(file_fields::cat_name::hd, atol(param_value[file_fields::cat_name::hd]));
cid->push_back(ci);
ci=Cat_Id("SAO", atol(param_value["SAO"]));
cid->push_back(ci);
state=3;
if (param_value.count(file_fields::name)) {strcpy(name, param_value[file_fields::name]);/* trim::beginend(name ,' ');*/}
float vmag=6.0;
if (param_value.count(file_fields::vmag)) vmag = atof(param_value[file_fields::vmag]);
std::string sc;
if (param_value.count(file_fields::phis_characteristic::spec_class)) sc = trim::beginend(param_value[file_fields::phis_characteristic::spec_class],' ');
state=4;
a = new Astrum(c,vmag,sc.c_str(),name,param_value["altname"]);
a->setProperMotion(coordinates::Coordinate(/*atof(param_value["pmRA"])/3600., atof(param_value["pmDE"])/3600.*/));
a->setCatIDs(cid);}
catch (...) {FILE*f=fopen("cat.log","a"); fprintf(f, "error on state %d\n",state); fclose (f);}
return a;}
//--------------------
Astrum* getAstrum(const formats::ValueMap& param_value,const formats::FormatOfRecord& record_format){
using namespace straux;
using namespace elementus_mundus::altus_mundus;
Astrum* a=NULL;
int state=0;
if (!param_value.size()) return a;
double r = 8;
char name[32];
if (param_value.count(file_fields::parallax)){
strcpy(name,param_value.at(file_fields::parallax));
if (strlen(name)){
r = atof(name)/3600.;
r = sin(r*coordinates::converting::toRad/2);
if (r>0.0000000000001){
r = coordinates::astronomical_unit/r;
r/=coordinates::parsec;} else r = (rand()%800+400.)/100;}}
try{
state =1;
coordinates::Coordinate c=coordinates::Coordinate(coordinates::converting::HMStoFloat(param_value.at(file_fields::RA)),
coordinates::converting::HMStoFloat(param_value.at(file_fields::declination)),fabs(r), coordinates::type::equator);
state=2;
memset(name, 0, 32);
Cats_Ids *cid = new Cats_Ids;
Cat_Id ci(file_fields::cat_name::hr, atol(param_value.at(file_fields::cat_name::hr))); // HR is BSC star number
cid->push_back(ci);
ci=Cat_Id(file_fields::cat_name::hd, atol(param_value.at(file_fields::cat_name::hd)));
cid->push_back(ci);
ci=Cat_Id("SAO", atol(param_value.at("SAO")));
cid->push_back(ci);
state=3;
if (param_value.count(file_fields::name)) {strcpy(name, param_value.at(file_fields::name));/* trim::beginend(name ,' ');*/}
float vmag=6.0;
if (param_value.count(file_fields::vmag)) vmag = atof(param_value.at(file_fields::vmag));
std::string sc;
if (param_value.count(file_fields::phis_characteristic::spec_class)) sc = trim::beginend(param_value.at(file_fields::phis_characteristic::spec_class),' ');
state=4;
a = new Astrum(c,vmag,sc.c_str(),name,param_value.at(file_fields::alt_name));
a->setProperMotion(coordinates::Coordinate(/*atof(param_value["pmRA"])/3600., atof(param_value["pmDE"])/3600.*/));
a->setCatIDs(cid);}
catch (...) {FILE*f=fopen("cat.log","a"); fprintf(f, "error on state %d\n",state); fclose (f);}
return a;}
//----------------- not yet needed
Sidus* getConstellation(const char *raw_data, const formats::FormatOfRecord &record_format){
Sidus *s=NULL;/* = new Sidus();*/

return s;}
//--------------
Planet* getPlanet(const formats::ValueMap& values){
using namespace file_fields::orbital_elements;
using namespace file_fields::phis_characteristic;
/* предупреждать о отсутствующих параметрах */
Planet *p=new Planet(values.at(file_fields::name));
/* orbital elements */
if (values.count(file_fields::tp)) p->setTP(atof(values.at(file_fields::tp)));
else if (values.count(file_fields::tp_d)) p->setTP(atof(values.at(file_fields::tp_d))/365.25);
if (values.count(ep_longitude)) p->kep_orbit.EP(atof(values.at(ep_longitude)));
if (values.count(per_longitude)) p->kep_orbit.WP(atof(values.at(per_longitude)));
if (values.count(eccentricity)) p->kep_orbit.E(atof(values.at(eccentricity)));
if (values.count(semi_major_axis)) p->kep_orbit.A(atof(values.at(semi_major_axis))); else
if (values.count(semi_major_axis_km)) p->kep_orbit.A(atof(values.at(semi_major_axis_km))/coordinates::astronomical_unit);
if (values.count(inclination)) p->kep_orbit.I(atof(values.at(inclination)));
if (values.count(an_longitude)) p->kep_orbit.W(atof(values.at(an_longitude)));
if (values.count(polarR)) p->sphere.setPolarR(atof(values.at(polarR))*2);
if (values.count(file_fields::day_length)) p->setDayLength(atof(values.at(file_fields::day_length)));else p->setDayLength(0);
if (values.count(file_fields::day_phase)) p->setDayPhase(atof(values.at(file_fields::day_phase)));else p->setDayPhase(0);
if (values.count(file_fields::eqtec)) p->setEqToOrbit(atof(values.at(file_fields::eqtec)));else p->setEqToOrbit(0);
return p;}

Cometa* getComet(const char* raw_data,const formats::FormatOfRecord& record_format){
formats::ValueMap param_value=record::get_values(raw_data,record_format);
Cometa* c = new Cometa();
c->setName(param_value["name"]);
c->kep_orbit.A(atof(param_value["a"]));
c->kep_orbit.E(atof(param_value["e"]));
c->kep_orbit.I(atof(param_value["i"]));
c->kep_orbit.W(atof(param_value["w"]));
c->kep_orbit.WP(atof(param_value["wp"]));
// но на самом деле тут есть ещё десятичные доли дня,
// что в очередной раз новидит на мысль отказатся от класса QDateTime
QDate ppd(atoi(param_value["perPasY"]), atoi(param_value["perPasM"]), atoi(param_value["perPasD"]));
c->setPerihelionPassageDate(ppd);

return c;}
// getting default parameters for asterzm; node - is def param secton node; d_color mist be not NULL;
// return 1 if parameters found;
bool asterizm_default(const YAML::Node &node, float &d_width, float *d_color){
d_width=1.0; // толщина линии по умолчанию
d_color[0] = 1.0;
d_color[1] = 1.0;
d_color[2] = 0.0;
const YAML::Node * def_param=node.FindValue("default");
if (def_param!=NULL){
const YAML::Node * tmp = def_param->FindValue("width");
if (tmp!=NULL) *tmp >> d_width;
tmp = def_param->FindValue("color");
if (tmp!=NULL) {
for (unsigned short i=0; i<3; i++) (*tmp)[i] >> d_color[i];}}
else return 0;
return 1;}
//------------------ parse struct col_width. (create color)
bool getColWidth(screen::col_wid& cw,const YAML::Node& n){
try{
n["width"] >> cw.width;
const YAML::Node &col = n["color"];
cw.color = new float[4];
for (unsigned short i =0; i<4; i++) col[i] >> cw.color[i];}
catch (...) {return 0;}
return 1;}
//-----------------------
bool getColWidthStr(screen::col_wid_str& cws, const YAML::Node& n){
getColWidth(cws,n);
try{
n[file_fields::graph::factor] >> cws.factor;
n[file_fields::graph::pattern] >> cws.pattern;}
catch(...) {return 0;} return 1;}
//----------
elementus_mundus::proxima_mundus::Planet* plnet(const YAML::Node& node){
elementus_mundus::proxima_mundus::Planet* p = new elementus_mundus::proxima_mundus::Planet();

return p;}
}

namespace file{
using profundus_mundus::AstraList;
using planetary_system::PlanetList;
using planetary_system::CometList;
using profundus_mundus::SidusList;
// здесь и далее id - число загруженых на данный момент астрообъектов. используется для раздачи уникальных id астрообъектам.
//------- загрузка звёзд. из filename в al. al!=NULL
bool stars(const char* filename, const formats::FormatOfRecord& record_format, AstraList* al, ID_Count_type &id){
using elementus_mundus::altus_mundus::Astrum;
using namespace straux;
char buff[Dbuff_len];
FILE* f=fopen(filename,"r");
if(f!=NULL){
while(!feof(f)){
memset(buff, 0, Dbuff_len);
fgets(buff,Dbuff_len,f);
trim::before_char(buff,'#'); // убераем комментарии из файла
if(strlen(buff)>0){
Astrum *a = record::getAstrum(buff,record_format);
if (a){a->setAbsId(++id);
al->push_back(a);}
}}
fclose(f); return 1;}
 else return 0;}

bool planets(const char* filename, const formats::FormatOfRecord& record_format, PlanetList* pl, ID_Count_type &id){
using elementus_mundus::proxima_mundus::Planet;
FILE* f=fopen(filename,"r");
if(f!=NULL){
char buff[Dbuff_len];
formats::ValueMap values;
while(!feof(f)){
fgets(buff,Dbuff_len,f);
straux::trim::beginend(straux::trim::before_char(buff,'#'),' ');
if(strlen(buff)>0){
values=record::get_values(buff,record_format);
Planet* p = record::getPlanet(values);
p->setAbsId(++id);
pl->push_back(p); values.clear();}}
memset(buff,0,Dbuff_len);
fclose(f); return 1;}else return 0;}
//--------------
bool satellites(const char* filename, const formats::FormatOfRecord& record_format, PlanetList* pl, ID_Count_type &id){
using elementus_mundus::proxima_mundus::Planet;
FILE *f=fopen(filename,"r");
if (f){
char buf[Dbuff_len];
elementus_mundus::proxima_mundus::Planet* p=NULL;
while (!feof(f)){
fgets(buf,Dbuff_len,f);
straux::trim::before_char(buf,'#');
straux::trim::begin(buf,' ');
if (strlen(buf)){
if (buf[0]==':'){p=NULL;strlwr(buf+1);
char name[128];
for (PlanetList::iterator it=pl->begin(), end=pl->end(); it!=end; it++){
memset(name,0,128);
strcpy(name,(*it)->name()); strlwr(name);
if (!strcmp(name, buf+1)){
p=*it; break;}} continue;}
else if (p){
Planet* sat = deserialization::record::getPlanet(record::get_values(buf,record_format));
sat->setAbsId(++id);
p->addNSat(sat);}}

}fclose(f);}
return 1;}
//-----------------------
bool comets(const char *filename, const formats::FormatOfRecord &record_format, CometList *cl, ID_Count_type &id){
using namespace straux;
using elementus_mundus::proxima_mundus::Cometa;
char buff[Dbuff_len];
FILE* f=fopen(filename,"r");
if(f!=NULL){
while(!feof(f)){
memset(buff, 0, Dbuff_len);
fgets(buff,Dbuff_len,f);
trim::before_char(buff,'#'); // убераем комментарии из файла
if(strlen(buff)>0){
Cometa *a = record::getComet(buff,record_format);
a->setAbsId(++id);
cl->push_back(a);
}}fclose(f); return 1;} else return 0;}
//---------------------------------------
bool constellation(const char* filename, const formats::FormatOfRecord& record_format, SidusList* sl, ID_Count_type &id){
using namespace straux;
using elementus_mundus::altus_mundus::sidus::Sidus;
char buff[Dbuff_len];
FILE* f=fopen(filename,"r");
if(f!=NULL){
 while(!feof(f)){
memset(buff, 0, Dbuff_len);
fgets(buff,Dbuff_len,f);
trim::before_char(buff,'#'); // убераем комментарии из файла
if(strlen(buff)>0){
Sidus *a = record::getConstellation(buff,record_format);
a->setAbsId(++id);
sl->push_back(a);
}}fclose(f); return 1;} else return 0;}

bool odss(const char* filename, const formats::FormatOfRecord& record_format, DSOList* odsl, ID_Count_type &id){
using namespace straux;
using elementus_mundus::altus_mundus::dso::DSO;
char buff[Dbuff_len];
FILE* f=fopen(filename,"r");
if(f!=NULL){
while(!feof(f)){
memset(buff, 0, Dbuff_len);
fgets(buff,Dbuff_len,f);
trim::before_char(buff,'#'); // убераем комментарии из файла
if(strlen(buff)>0){
DSO* ods=record::getDSO(buff,record_format);
ods->setAbsId(++id);
odsl->push_back(ods);}}
fclose(f); return 1;}else return 0;}}

}

/************/
namespace cat_aux{
using namespace profundus_mundus;
Coordinate* coordinateById(const profundus_mundus::AstraList &al, unsigned long id){
Coordinate *c=new Coordinate();
elementus_mundus::altus_mundus::Astrum *a;
//profundus_mundus::AstraList::const_iterator it=al.begin();
unsigned i=0;
for (profundus_mundus::AstraList::const_iterator it=al.begin(), end=al.end(); it!=end; it++){
a=*it; i++;
if (a->getIDin("HR")==id){
return &(*c = a->coordinate);}}
delete c;
return NULL;}

elementus_mundus::altus_mundus::Astrum* astraById(const profundus_mundus::AstraList &al, unsigned long id){
elementus_mundus::altus_mundus::Astrum *a;
//profundus_mundus::AstraList::const_iterator it=al.begin();
unsigned i=0;
for (profundus_mundus::AstraList::const_iterator it=al.begin(), end=al.end(); it!=end; it++){
a=*it; i++;
if (a->getIDin("HR")==id){return a;}}
return NULL;
}

ListOfCoordList makeLinesList(const deserialization::ul_vect_2d &ll,const profundus_mundus::AstraList &al){
ListOfCoordList res;
deserialization::ul_vector line;
for (deserialization::ul_vect_2d::const_iterator it=ll.begin(), end=ll.end(); it!=end; it++){
line=*it;
CoordList *cl = new CoordList;
for (unsigned i=0; i<line.size(); i++){
Coordinate *c = coordinateById(al,line[i]);
if (c!=NULL) cl->push_back(c);}
res.push_back(cl);}
return res;}
//--------------- возвращает астеризм, состоящий из указателей на BasicAstro
AstraListsVector makeAstroPattern(const deserialization::ul_vect_2d &ll,const profundus_mundus::AstraList &al){
AstraListsVector res;
deserialization::ul_vector line;
for (deserialization::ul_vect_2d::const_iterator it=ll.begin(), end=ll.end(); it!=end; it++){
line=*it;
profundus_mundus::AstraList *cl = new profundus_mundus::AstraList;
for (unsigned i=0; i<line.size(); i++){
elementus_mundus::altus_mundus::Astrum *a = astraById(al,line[i]);
if (a!=NULL) cl->push_back(a);}
res.push_back(cl);}
return res;}

//тут память не пропадает?
char** getShitList(char* line, char c){
if (line==NULL || strlen(line)<2) return NULL;
    int rowsc=0;
    for (unsigned i=0; i<strlen(line); i++)
      if (line[i]==c) rowsc++;
    rowsc++;
    char *_line=strdup(line);
    char *s_line=_line;

    char **rows; //запись разбитая по полям
    rows=new char*[rowsc+1];
    for (int i=0;i<rowsc-1; i++){
    char *tmp=strchr(_line, c);
    tmp[0]=0;
    rows[i]=strdup(_line);
    strcpy(rows[i],_line);
    _line=tmp+1;}

    rows[rowsc-1]=strdup(_line);
    rows[rowsc]=NULL;
    delete[] s_line;
return rows;}
//-----------
std::list<char**> getLineList(char *s){
using namespace straux;
std::list<char**> tmpl;unsigned i=0,ii=0;
char* sc=strstr(s,"[");
sc=trim::before_char(sc,']');
sc=trim::after_char(sc,'[');
unsigned size_sc=strlen(sc);
while(size_sc>=ii){i=0;
while(sc[0]=='\n'){sc++;ii++;}
while(sc[i]!='\n' && sc[i]!=0){i++;ii++;}
sc[i]=0;
tmpl.push_back(cat_aux::getShitList(sc,','));
sc=sc+i+1;
ii++;
}return tmpl;
}}
