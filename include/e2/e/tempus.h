﻿#ifndef ELEMENTUS_TEMPUS_H
#define ELEMENTUS_TEMPUS_H

#include <time.h>

namespace tempus{
const double J2000 = 2451545.;
namespace converting{
double datetoMJD(struct tm t);
}}

#endif // ELEMENTUS_TEMPUS_H
