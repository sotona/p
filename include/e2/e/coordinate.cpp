﻿#include "coordinate.h"
#include <stdlib.h>
#include <stdio.h>
#include <QMessageBox>


namespace info{
namespace param{
namespace coord{
const std::string ecl_range_units = "au";
const std::string eq_range_units = "parsec";
const std::string range  = "range";
const std::string ra = "RA";
const std::string declination = "declination";
const std::string ecl_long = "ecl.longitude";
const std::string ecl_lat = "ecl.latitude";}
}}

/*all about coordinats*/
namespace coordinates{
namespace converting{
const char *hours[]={"h ","m ","s"};
const char *degrees[]={" ", "\' ","\""};
/*********************************************************/
double RAToGalacticLong(double ra){
return ra;}

//склонение -> галактическая широта
double DeclToGalacticLat(double de){
return de;}

//галактическая долгота -> прямое восхождение
double GalacticLongitudeToRA(double lon){
return lon;}

//галактическая широта -> склонение
double GalacticLatitudeToDecl(double lat){
return lat;}

double HorMinSecToFloat(double hour, double min,double sec){ //чч мм сc.cc -> ч.ч
double h=hour+min/60.+sec/3600.;
return h;}
//----------------- delete all spaces and return new string
char* del_spaces(const char* s){
if (s==NULL) return NULL;
int sp=0;
for (unsigned i=0; i<strlen(s); i++) if (s[i]==' ') sp++;
char *ns=new char[strlen(s)-sp];
sp=0;
for (unsigned i=0; i<strlen(s); i++) if (s[i]==' ') {sp++;continue;}
else {ns[i-sp]=s[i];}
ns[strlen(s)-sp]=0;
return ns;}

//----- get gloat valur from string "hhmmss[.s]" or "+[or -]gg mm ss[.s]"
double HMStoFloat(const char* hms){
//FILE* f = fopen("qwerty","w");
//fprintf(f,hms); fclose(f);
if(hms==NULL || !strlen(hms)) return 0.123456789;
double hours=0;
short sign=1;
char *h=new char[3];memset(h,0,3);
char *nhms=NULL;
if (hms[0]=='-' || hms[0]=='+'){
if (hms[0]=='-') sign=-1;
nhms=del_spaces(hms+1);}
else nhms=del_spaces(hms);
if (nhms!=NULL){
if (strlen(nhms)>1){
strncpy(h,nhms,2);h[2]=0;
hours+=atof(h);
if (strlen(nhms)>3){
strncpy(h,nhms+2,2);h[2]=0;
hours+=atof(h)/60.;
if (strlen(nhms)>5){
delete[]h;
h=strdup(nhms+4);h[2]=0;
hours+=atof(h)/3600.;
delete[]h;}}}
/*delete[] nhms;*/}
if(h)delete[]h;
return hours*sign;}

//------------------------------------------------
double HorMinSecToFloat(char* hms){ //строка(чч мм сc.cc) -> в число(ч.ч)
double h;
while (hms[0]==' ') hms++;
char *tmp=strchr(hms,' ');
if (strlen(hms)==0) {return 0;}
//если не разделено пробелами
if (tmp==NULL){
char *_hms=strdup(hms);
//delete[] hms;
hms = new char[strlen(_hms)+2];
for (unsigned i=0; i<strlen(_hms)+2; i++)
hms[i]=0;
if (_hms[0]=='+' || _hms[0]=='-') {strncpy(hms, _hms,3);_hms+=3;}
else {strncpy(hms, _hms,2); _hms+=2;}
hms[strlen(hms)]=' ';
strncat(hms, _hms,2);
hms[strlen(hms)]=' ';
_hms+=2;
strcat(hms, _hms);
/*delete[] _hms;*/}

tmp=strchr(hms,' ');
tmp[0]=0;
h=atof(hms);
hms+=3; tmp[0]=' ';

if (hms[0]==' ') hms++;
tmp=strchr(hms, ' ');
tmp[0]=0;
h+=atof(hms)/60.; tmp[0]=' ';

hms+=3;
tmp=strchr(hms,' ');
h+=atof(hms)/3600.; hms-=6;
return h;}

//---------------------------------------------
int*  floatHourToHMS(double x){
int* hmsms=new int[4];
if (x>0){
hmsms[0]=floor(x);x-=hmsms[0]; x*=60.;
hmsms[1]=floor(x);x-=hmsms[1]; x*=60.;
hmsms[2]=floor(x);x-=hmsms[2];
hmsms[3]=x*1000;}
else{
hmsms[0]=ceil(x);x-=hmsms[0];x*=60.;
hmsms[1]=-ceil(x);x+=hmsms[1];x*=60.;
hmsms[2]=-ceil(x);x+=hmsms[2];
hmsms[3]=-x*1000;}
return hmsms;}
//---------------------------------------------
char* HMStochar(int *x,bool is_hour){
char *t=new char[8];
const char **what;
if (is_hour) what=hours; else what=degrees;
char* hms = new char[9];hms[0]=0;
for (short i=0; i<3; i++){
strcat(hms,itoa(x[i],t,10));
strcat(hms,what[i]);}
delete []t;
return hms;}
//-------------------------------
char* HMStochar(double x,bool is_hour){
char t[8];
const char **what;
if (is_hour) what=hours; else what=degrees;
char* hms = new char[16];hms[0]=0;
strcat(hms,itoa((int)(x),t,10));
strcat(hms,what[0]);
double min =fabs(((int)(x)-x)*60);
strcat(hms,itoa(min,t,10));
strcat(hms,what[1]);
unsigned short sec = abs((floor(min)-min)*60);
strcat(hms,itoa(sec,t,10));
strcat(hms,what[2]);
return hms;}

//сферические координаты в декартовы.
//----- xyz must be double[3]
double* SpherToCartesian(const Coordinate &c, double* xyz){
double k;
switch (c.Type()){
case coordinates::type::ecliptic: {k=astronomical_unit; break;}
case coordinates::type::equator: {k = coordinates::parsec; break;}
default: {k = astronomical_unit; break;} /* временно*/}
double alpha=c.Alpha()*M_PI;
if (c.Type()==type::equator) alpha/=12.;
else alpha/=180.;
double cskr = cos(c.Sigma()*toRad)*k*c.Range();
xyz[0] = cos(alpha) * cskr;
xyz[1] = c.Range()*sin(c.Sigma()*toRad)*k;
xyz[2] = -sin(alpha) * cskr;
return xyz;}
//-------------преобразование декартовых координат в сферические
Coordinate CartesianToSpher(const double *xyz){
Coordinate c;
c.Range(pow(xyz[0]*xyz[0]+xyz[1]*xyz[1]+xyz[2]*xyz[2],2));
c.Alpha(atan(xyz[1]/xyz[0]));
c.Sigma(acos(xyz[2]/c.Range()));
return c;}
//равнопромежуточная проекция
double* inEquidistant(
                    double lat,
                    double lon,
                    double lat0,
                    double lon0){
double *xy=new double[2];
xy[0]=cos(lat0*converting::toRad)*(lon-lon0+180)/360;
xy[1]=(lat-lat0+90)/180;
return xy;}
double* fromEquidistant(
                    double x,
                    double y,
                    double lat0,
                    double lon0){
double *latlon=new double[2];
latlon[1]=360*x/cos(lat0*converting::toRad)+lon0-180;
latlon[0]=y*180+lat0-90;
return latlon;}
}
Coordinate::Coordinate(const Coordinate &c){
this->alpha=c.alpha;
this->sigma=c.sigma;
this->r=c.r;
this->type=c.type;}
//------------------------------------------------
Coordinate::Coordinate(int t){
type=t;
alpha=0;
sigma=0;
r=very_far;}
//------------------------------------------------
Coordinate::Coordinate(double a,double s,double r, int t){
type=t; this->r=r;
alpha=a; sigma=s;}
//-------------------
void Coordinate::Alpha(double a){
double m = 360;
if (type==type::equator) m = 24;
this->alpha = fmod(a,m);
if (alpha<0) alpha+=m*ceil(fabs(alpha)/m);}
//--------------------
void Coordinate::Sigma(double s){
sigma=(fabs(s)>90.)?90.*(s)/fabs(s):s;/*fmod(s,90.)*/;
}

//---------------------------------------------------------
double Coordinate::RightAscension() const{
return alpha;}
//---------------------------------------------------------
double Coordinate::Declination() const{
        if (type==coordinates::type::equator) return this->sigma;
        else return converting::GalacticLatitudeToDecl(this->sigma);
}
double Coordinate::Azimuth(double lat) const{
if (type==type::horizontal) return this->alpha;}

double Coordinate::Height(double lat, double LST)const{
if (type==type::horizontal) return this->sigma;}

void Coordinate::setAzimuth(double az, double latitude){
if (type==type::horizontal) {this->alpha = fmod(az,360.);
if (alpha<0) alpha+=360.;}}

void Coordinate::setHeight(double h, double latitude){
if (type==type::horizontal) this->sigma = fmod(h,90.);
/* if (h>=90) {this->sigma = 90.-this->sigma;
             this->alpha+=180.;}*/}

double Coordinate::EclipticLatitude() const {return this->sigma;}
double Coordinate::EclipticLongitude() const {return this->alpha;}

std::string* Coordinate::toString() const{
unsigned short size = 32;
char rc[size]; memset(rc,0,size); sprintf(rc,"%5.3f", this->r);
std::string* crd = new std::string[3];
crd[2] =  rc;
crd[2]+=" ";
switch (this->type){
case coordinates::type::equator: {crd[0] = converting::HMStochar(this->alpha,1);
                                  crd[2] += info::param::coord::eq_range_units; break;}
default:                         {crd[0] = converting::HMStochar(this->alpha,0);
                                  crd[2] += info::param::coord::ecl_range_units; break;}
}
crd[1] = converting::HMStochar(this->sigma,0);
return crd;}

std::string* Coordinate::coordNames() const{
std::string* c = new std::string[3];
switch (this->type){
case coordinates::type::ecliptic: {c[0] = info::param::coord::ecl_long;
                                   c[1] = info::param::coord::ecl_lat; break;}
case coordinates::type::equator:  {c[0] = info::param::coord::declination;
                                   c[1] = info::param::coord::ra; break;}}
c[2] = info::param::coord::range;
return c;}

std::string Coordinate::geRangeUnits() const{
switch (type){
case coordinates::type::ecliptic: return "a.u";
case coordinates::type::equator: return "parsec";
default: return "parsec";}
}
/*****************/
double distance(const Coordinate &c1, const Coordinate &c2){
double xyz1[3]; converting::SpherToCartesian(c1,xyz1);
double xyz2[3]; converting::SpherToCartesian(c2,xyz2);
double d = fabs(sqrt(pow(xyz1[0]-xyz2[0],2)+pow(xyz1[1]-xyz2[1],2)+pow(xyz1[2]-xyz2[2],2)));
return d;}

double distance(const double *xyz1, const double *xyz2){
return fabs(sqrt(pow(xyz1[0]-xyz2[0],2)+pow(xyz1[1]-xyz2[1],2)+pow(xyz1[2]-xyz2[2],2)));}

double* addition(const double *xyz, double *res_xyz){
res_xyz[0]+=res_xyz[0];res_xyz[1]+=res_xyz[1];res_xyz[2]+=res_xyz[2];
return res_xyz;}

/********************/
void mid_value(const CoordList& cl, Coordinate& mid_point){
double alpha=0, sigma=0;
for (CoordList::const_iterator it = cl.begin(), end = cl.end(); it!=end; it++){
alpha+=(*it)->Alpha();
sigma+=(*it)->Sigma();}
alpha/=cl.size();sigma/=cl.size();
mid_point.Alpha(alpha);
mid_point.Sigma(sigma);}


}
