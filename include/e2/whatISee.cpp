﻿//#ifndef WHATISEE_CPP
//#define WHATISEE_CPP

#include "whatIsee.h"

namespace electum_necessarius{

inline bool isVisible(const bitset48& b, unsigned char i){
    if (i>b.size()) return 0;
    else return b.test(i);}

/************** what I see? ************/
whatISee::whatISee(){
things.reset();
things[universe::ds::astra]=1;
things[universe::ps::planet]=1;
labels.reset();
labels[universe::ps::planet]=1;
/*imagination.reset();*/}
//-----------------------
whatISee::whatISee(bitset48 _universe, bitset48 _labels, bitset48 _imagination){
things=_universe;
labels = _labels;
things = _imagination;}
//------------------------
bool whatISee::isUvisible(unsigned char i) const{return isVisible(things,i);}
bool whatISee::isLvisible(unsigned char i) const{return isVisible(labels,i);}
//bool whatISee::isIvisible(unsigned char i) const{return isVisible(imagination,i);}
}

//#endif
