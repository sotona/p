﻿#ifndef OBSERVER_H
#define OBSERVER_H

#include <bitset>
#include <vector>
#include "e/mundus.h"
#include "oculus.h"
#include "whatIsee.h"
#include "location.h"

namespace info{
namespace param{
namespace observer{
const std::string fov = "FOV";
const std::string pk = "pk";
}
}}

namespace observer{
namespace perceptionem{

float vmagKfromFoV(float fov);

const char ground = 25; //поверхность планеты, на которой сидит наблюдатель


//float d_vmag_fov = 120./6.; //зависимость проницательной способности от угла обзора
/*Восприятие. что и как видно на небе. реальные объекты + вспомогательные*/
class Perceptionem{
    float vmag_fov; //зависимость проницательной способности от угла обзора
public:
 Perceptionem();
 float labels_k;     // коэф-т для вычисления предельной звёздой видимой величины подписей для астрообъектов
 float how_i_see;    // предельная видимая величина
 electum_necessarius::whatISee what_i_see;
// electum_necessarius::howIsee how_i_see_universe;
// electum_necessarius::howIsee how_i_see_labels;
 bool relative_vmag;                    // показывать звёзды разной яркости рзных размеров ?
};}



/*In endless nights
He observes the sky*/
class Observer{
public:
Observer();
~Observer();
oculus::Oculus eye;                                         //direction of the look of the observer
perceptionem::Perceptionem perception;                      //what may observer see in this c

info::ParamValues sel_info;                // инфа о выбранном астрообъекте

//void addAsterizms(const AsterismList& a);
//void addUserMarks(const CaelestisList& cl);
//void addUserMark(elementus_mundus::altus_mundus::Caelestis* c);

//const CaelestisList&         getCaelestis() const {return this->user_marks;}
//const AsterismList getAsterizms() const {return this->asterizms;}
//private:
//AsterismList asterizms;                                     // астеризмы пользователя
//CaelestisList user_marks;
};

}


#endif // OBSERVER_H
