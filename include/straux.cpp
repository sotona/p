﻿#include "straux.h"

namespace straux{
namespace trim{
using std::string;
//-----------------удалять с начала пока С
char* begin(char* s,char c){
if (s && strlen(s)){
unsigned i=0;
while(s[i]==c && s[i+1]!=0 && i+1<strlen(s))i++;
memmove(s,s+i,strlen(s)-i);
s[strlen(s)-i]=0;
return s;}else return "";}
//-----------------удалять с конца пока С
char* end(char* s,char c){
if (s && strlen(s)){
unsigned i=strlen(s)-1;
while(s[i]==c)i--; i++;
memmove(s,s,i); s[i]=0;
return s;}else return "";}
//-----------
char* beginend(char* s, char c){
char *ss=begin(s,c);
ss=end(ss,c);
return ss;}
//------------
char* before_char(char* s,char c){
unsigned i=0;
while(s[i]!=c && s[i+1]!=0)i++;
memmove(s,s,i);s[i]=0;
return s;}
//-----------
char* after_char(char* s,char c){
int i=strlen(s)-1;
while(s[i]!=c && i!=-1)i--;i++;
memmove(s,s+i,strlen(s)-i);
return s;}}

namespace get{
char* before_char(char* s,char c){
char *tmp;unsigned i=0;
while(s[i]!=c && s[i+1]!=0)i++;i++;
tmp=new char[i+1];
memcpy(tmp,s,i);
tmp[i]=0;
return tmp;}

char* after_char(char* s,char c){
char *tmp;unsigned i=strlen(s)-1;
while(s[i]!=c)i--;i++;
tmp=new char[i+1];
memcpy(tmp,s+i,strlen(s)-i);
tmp[strlen(s)-i]=0;
return tmp;}}

namespace trim{
//------
string begin(string s, char c){
unsigned i=0;string ss;
while (s[i]=='c' && s.size()>i) i++;
ss=s.substr(i);
return ss;}
//------
string end(string s, char c){
unsigned i=s.size()-1;string ss;
while (s[i]=='c') i--;
ss=s.substr(s.size()-i);
return ss;}
//-------
string beginend(string s, char c){
return end(begin(s,c),c);}
}

namespace get{
string before_char(string s, char c){
unsigned i=0;string ss;
for (i=0; i<s.size() && s[i]==c; i++);
ss=s.substr(0,i);
return ss;}
//-------
string after_char(string s, char c){
unsigned i=0;string ss;
for (i=s.size()-1; i>0 && s[i]==c; i--);
ss=s.substr(i,s.size()-i);
return ss;}
}

unsigned strarrlen(const std::string str_arr[]){
unsigned i=0; while (str_arr[i++].size()); return i;}

void copy(std::vector<std::string>& v,const std::string s[]){
v.resize(0); unsigned i=0;
while (s[i].size()) v.push_back(s[i++]);}
}
