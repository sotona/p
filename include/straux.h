﻿#ifndef STRAUX_H
#define STRAUX_H
#include <string>
#include <vector>
#include <string.h>
namespace straux{
namespace trim{
char* begin(char* s, char c);
char* end(char* s, char c);
char* beginend(char* s, char c);
char* before_char(char* s, char c);
char* after_char(char* s, char c);}

namespace get{
char* before_char(char* s, char c);
char* after_char(char* s, char c);}

using std::string;
namespace trim{
string begin(string s, char c);
string end(string s, char c);
string beginend(string s, char c);}
namespace get{
string before_char(string s, char c);
string after_char(string s, char c);}

/*возвращает длинну массива строк, заканчивающегося пустой строкой*/
unsigned strarrlen(const std::string str_arr[]);
void copy(std::vector<std::string>& v,const std::string[]);

}
#endif // STRAUX_H
