﻿/********************************************************************************
** Form generated from reading UI file 'menu.ui'
**
** Created: Tue 24. May 19:46:01 2011
**      by: Qt User Interface Compiler version 4.7.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MENU_H
#define UI_MENU_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>

QT_BEGIN_NAMESPACE

class Ui_MenuBar
{
public:
    QAction *time_inc;
    QAction *about;
    QAction *time_dec;
    QAction *stop_time;
    QAction *SetDateTime;
    QAction *current_time;
    QAction *setting;
    QAction *Set_normal_time;
    QAction *set_back_time;
    QAction *set_location;
    QMenu *menu;
    QMenu *menu_2;
    QMenu *menu_3;

    void setupUi(QMenuBar *MenuBar)
    {
        if (MenuBar->objectName().isEmpty())
            MenuBar->setObjectName(QString::fromUtf8("MenuBar"));
        MenuBar->setWindowModality(Qt::NonModal);
        MenuBar->resize(782, 20);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MenuBar->sizePolicy().hasHeightForWidth());
        MenuBar->setSizePolicy(sizePolicy);
        QFont font;
        font.setStyleStrategy(QFont::PreferDefault);
        MenuBar->setFont(font);
        MenuBar->setMouseTracking(true);
        MenuBar->setFocusPolicy(Qt::NoFocus);
        MenuBar->setWindowOpacity(1);
        MenuBar->setLayoutDirection(Qt::LeftToRight);
        MenuBar->setAutoFillBackground(true);
        MenuBar->setDefaultUp(false);
        MenuBar->setNativeMenuBar(true);
        time_inc = new QAction(MenuBar);
        time_inc->setObjectName(QString::fromUtf8("time_inc"));
        about = new QAction(MenuBar);
        about->setObjectName(QString::fromUtf8("about"));
        time_dec = new QAction(MenuBar);
        time_dec->setObjectName(QString::fromUtf8("time_dec"));
        stop_time = new QAction(MenuBar);
        stop_time->setObjectName(QString::fromUtf8("stop_time"));
        SetDateTime = new QAction(MenuBar);
        SetDateTime->setObjectName(QString::fromUtf8("SetDateTime"));
        current_time = new QAction(MenuBar);
        current_time->setObjectName(QString::fromUtf8("current_time"));
        setting = new QAction(MenuBar);
        setting->setObjectName(QString::fromUtf8("setting"));
        Set_normal_time = new QAction(MenuBar);
        Set_normal_time->setObjectName(QString::fromUtf8("Set_normal_time"));
        set_back_time = new QAction(MenuBar);
        set_back_time->setObjectName(QString::fromUtf8("set_back_time"));
        set_back_time->setCheckable(true);
        set_back_time->setChecked(false);
        set_location = new QAction(MenuBar);
        set_location->setObjectName(QString::fromUtf8("set_location"));
        menu = new QMenu(MenuBar);
        menu->setObjectName(QString::fromUtf8("menu"));
        menu_2 = new QMenu(MenuBar);
        menu_2->setObjectName(QString::fromUtf8("menu_2"));
        menu_3 = new QMenu(MenuBar);
        menu_3->setObjectName(QString::fromUtf8("menu_3"));

        MenuBar->addAction(menu->menuAction());
        MenuBar->addAction(menu_3->menuAction());
        MenuBar->addAction(menu_2->menuAction());
        menu->addAction(time_inc);
        menu->addAction(time_dec);
        menu->addAction(stop_time);
        menu->addSeparator();
        menu->addAction(SetDateTime);
        menu->addAction(current_time);
        menu->addSeparator();
        menu->addAction(Set_normal_time);
        menu->addAction(set_back_time);
        menu_2->addAction(about);
        menu_3->addAction(setting);
        menu_3->addAction(set_location);

        retranslateUi(MenuBar);

        QMetaObject::connectSlotsByName(MenuBar);
    } // setupUi

    void retranslateUi(QMenuBar *MenuBar)
    {
        time_inc->setText(QApplication::translate("MenuBar", "\321\203\321\201\320\272\320\276\321\200\320\270\321\202\321\214 \320\262\321\200\320\265\320\274\321\217 L", 0, QApplication::UnicodeUTF8));
        about->setText(QApplication::translate("MenuBar", "\320\236 \320\237\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\265", 0, QApplication::UnicodeUTF8));
        time_dec->setText(QApplication::translate("MenuBar", "\320\267\320\260\320\274\320\265\320\264\320\273\320\270\321\202\321\214 \320\262\321\200\320\265\320\274\321\217 J", 0, QApplication::UnicodeUTF8));
        stop_time->setText(QApplication::translate("MenuBar", "\320\236\321\201\321\202\320\260\320\275\320\276\320\262\320\270\321\202\321\214 \320\262\321\200\320\265\320\274\321\217 K", 0, QApplication::UnicodeUTF8));
        SetDateTime->setText(QApplication::translate("MenuBar", " \320\264\320\260\321\202\321\203/\320\262\321\200\320\265\320\274\321\217 D", 0, QApplication::UnicodeUTF8));
        current_time->setText(QApplication::translate("MenuBar", "\321\201\320\270\321\201\321\202\320\265\320\274\320\275\320\276\320\265 \320\262\321\200\320\265\320\274\321\217", 0, QApplication::UnicodeUTF8));
        setting->setText(QApplication::translate("MenuBar", "\320\276\321\202\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        Set_normal_time->setText(QApplication::translate("MenuBar", "\320\275\320\276\321\200\320\274\320\260\320\273\321\214\320\275\320\260\321\217 \321\201\320\272\320\276\321\200\320\276\321\201\321\202\321\214", 0, QApplication::UnicodeUTF8));
        set_back_time->setText(QApplication::translate("MenuBar", "\320\262\321\200\320\265\320\274\321\217 \320\262\320\277\320\265\321\200\320\265\320\264/\320\275\320\260\320\267\320\260\320\264", 0, QApplication::UnicodeUTF8));
        set_location->setText(QApplication::translate("MenuBar", "\320\277\320\276\320\273\320\276\320\266\320\265\320\275\320\270\320\265 \320\275\320\260\320\261\320\273\321\216\320\264\320\260\321\202\320\265\320\273\321\217", 0, QApplication::UnicodeUTF8));
        menu->setTitle(QApplication::translate("MenuBar", "\320\224\320\260\321\202\320\260 \320\270 \320\262\321\200\320\265\320\274\321\217", 0, QApplication::UnicodeUTF8));
        menu_2->setTitle(QApplication::translate("MenuBar", "\320\241\320\277\321\200\320\260\320\262\320\272\320\260", 0, QApplication::UnicodeUTF8));
        menu_3->setTitle(QApplication::translate("MenuBar", "\320\275\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(MenuBar);
    } // retranslateUi

};

namespace Ui {
    class MenuBar: public Ui_MenuBar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MENU_H
