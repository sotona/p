﻿#ifndef SETTING_H
#define SETTING_H

#include <QDialog>
#include  <bitset>
#include "../include/e2/whatIsee.h"
namespace Ui {
    class setting;
}

class Setting : public QDialog
{
    Q_OBJECT

public:
    explicit Setting(QWidget *parent = 0);
    ~Setting();
void setWhatISee(electum_necessarius::whatISee &_wIs);
private:
    Ui::setting *ui;
electum_necessarius::whatISee *wIs;

private slots:
    void on_star_stateChanged(int );
    void on_planet_stateChanged(int );
    void on_planet_orbit_stateChanged(int );
    void on_asterizm_stateChanged(int );
    void on_ecla_stateChanged(int );
    void on_az_stateChanged(int );
    void on_eqv_stateChanged(int );
    void on_ecl_stateChanged(int );
    void on_all_stateChanged(int );
    void on_all_orbit_stateChanged(int );
    void on_all_text_stateChanged(int );
    void on_star_text_stateChanged(int arg1);
    void on_odk_text_stateChanged(int arg1);
    void on_planet_text_stateChanged(int arg1);
    void on_odk_stateChanged(int arg1);
    void on_asterizm_text_stateChanged(int arg1);
    void on_ground_stateChanged(int arg1);
    void on_ds_image_stateChanged(int arg1);
    void on_ecliptic_stateChanged(int arg1);
};

#endif // SETTING_H
