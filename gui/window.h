﻿
#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QMainWindow>
#include <QPrinter>
#include <QSlider>
#include "keyboard.h"
QT_BEGIN_NAMESPACE
class QAction;
class GLWidget;
class QLabel;
class QPushButton;
class QToolBar;
class QAction;
QT_END_NAMESPACE

//! [0]

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window();
    ~Window();
 void setinterface(Interface *__interface){_interface=__interface;}
 public slots:
    void full_screen();

protected:

    void keyPressEvent(QKeyEvent *event);

private:

//       void CreateToolBar();
//       void SetSizeScreen();
        Interface *_interface;

    signals:
       void key_pressed(QKeyEvent* e);
//        void show_settings_window();






};
//! [0]

#endif
