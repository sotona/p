﻿#include <stdio.h>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <QtGui/QImage>

#include <vector>
#include "glwidget.h"
#include "include/e2/e/elementus_mundus.h"
#include "include/e2/location.h"
#include "include/e2/whatIsee.h"
#include "include/v/screen.h"

const char* textures_file = "textures/textures";

/* для расположений onScreenInfo */
const unsigned short left_top = 1;      // информация о просмотре. fov, fps, ...
const unsigned short right_top = 0;     // информация о выбранном астрообъекте (контекстная информация)
const unsigned short right_bottom = 2;  // время
const unsigned short left_bottom = 3;   // положение, <г.п.>

using namespace screen;
using namespace profundus_mundus;
using namespace observer;


/*создаёт один кусок многострочного текста, для вывода на экран*/
QString getText(op_info::OPInfo *opi,unsigned char what){
QString result="";
if (opi->info[what].size()){
info::ParamValues inf = opi->info[what];
int i=0,s;
std::string *order = opi->order[what];
while (order[i].size()){
if (inf[order[i]].size()){
s = opi->titles_show_params[order[i]];
if (abs(s)<=opi->getCompletness()){
QString t = QString(inf[order[i]].c_str());
t = QString(inf.at(order[i]).c_str());
if (s>0) t = QString(order[i].c_str())+": " + t;
result+=t+"\n";}}
i++;}
if (result.size()) result[result.size()-1] = ' ';}
return result;}

GLWidget::GLWidget(QGLContext *context, QWidget *parent) // в формате нет ничего лишнего?
    : /*QGLWidget(QGLFormat(QGL::SampleBuffers|QGL::AlphaChannel|QGL::DepthBuffer), parent)*/
      QGLWidget(QGLFormat(QGL::SampleBuffers|QGL::AlphaChannel|QGL::DepthBuffer), parent){
//user_mark = coordinates::Coordinate(4);
this->gl_timer = new QTimer(this);
connect(gl_timer, SIGNAL(timeout()), SLOT(updateGL()));
this->gl_timer->setSingleShot(1); //теперь таймер будет срабатывать только один раз
//таймер подсчёта fps
this->screen.fps_timer = new QTimer(this);
connect(screen.fps_timer, SIGNAL(timeout()), SLOT(update_fps()));
this->screen.fps_timer->setInterval(1000); screen.fps_timer->stop();
this->screen.frames_per = 0; //счетчик кадров
this->screen.fps = 0;       // текушее количество кадровсекунду
screen.show_infotext=1;
screen.telescope=0;
s=0; a=0; // for ds image alocation
screen.infotext = new op_info::OPInfo();
}
//-------------------------------------------------
GLWidget::~GLWidget(){
/*delete screen.font;
delete ss_font;
delete ds_font;*/}
//-----------------------------------------------------------
unsigned GLWidget::loadTextures(initialization::graphic::Texture_File& file_tex){unsigned tex_loaded=0;
std::ofstream log_file;
log_file.open("textures_loading.log");
g_containers::PlanetMap *gc_pl=new g_containers::PlanetMap();
//загрузка планет
planetary_system::PlanetList pl = e2.adb->data->getNativeSS().getPlanets();
for (u_model::PlanetList::const_iterator it=pl.begin(), end=pl.end(); it!=end; it++){
QPixmap pixmap_texture;
char *cwhat=strdup((*it)->name()); // std::string гавно, и его tolower не работает
std::string what = strlwr(cwhat); delete[] cwhat;
//QMessageBox::about(this, "", "before first pixmap loading");
if (file_tex.count(what)) pixmap_texture.load(file_tex[what].c_str());
if (!pixmap_texture.isNull()) {
GLuint tex_id = bindTexture(pixmap_texture,GL_TEXTURE_2D);
gc_pl->insert(std::pair<elementus_mundus::ID_Count_type,g_containers::Planet*>((*it)->absID(),new g_containers::Planet(tex_id,(*it)->name()))); tex_loaded++;}
else log_file << (*it)->name() <<" texture not found: "<< file_tex[what] << "\n";
for(unsigned i=0;i<(*it)->getNSats()->size();i++){
QPixmap pixmap_texture;
char *cwhat=strdup((*it)->getNSats()->at(i)->name()); // std::string гавно, и его tolower не работает
std::string what = strlwr(cwhat); delete[] cwhat;
if (file_tex.count(what)) pixmap_texture.load(file_tex[what].c_str());
if (!pixmap_texture.isNull()) {
GLuint tex_id = bindTexture(pixmap_texture,GL_TEXTURE_2D);
gc_pl->insert(std::pair<elementus_mundus::ID_Count_type,g_containers::Planet*>
              ((*it)->getNSats()->at(i)->absID(),new g_containers::Planet(tex_id,(*it)->getNSats()->at(i)->name()))); tex_loaded++;}
else log_file << (*it)->getNSats()->at(i)->name() <<" texture not found: "<< file_tex[what] << "\n";}}
visual_universe.setPlanetMap(gc_pl);
//загрузка текстур неба
const std::string sky_names[] = {"universe_optic", "universe_relict", "universe_infrared",""};
this->visual_universe.deep_space.sky_texture=new GLuint[3];
memset(this->visual_universe.deep_space.sky_texture, 0, 3*sizeof(GLuint));
unsigned char i=0;
while (sky_names[i].length()){
QPixmap sky_pixmap;
unsigned c = file_tex.count(sky_names[i]);
if (c) sky_pixmap.load(file_tex[sky_names[i]].c_str());
if (!sky_pixmap.isNull()) {;this->visual_universe.deep_space.sky_texture[i]=bindTexture(sky_pixmap,GL_TEXTURE_2D);
                            tex_loaded++;}
else log_file << sky_names[i]<< " texture not found: "<< file_tex[sky_names[i]] << " \n";
i++;}
log_file.close();
log_file << tex_loaded;
log_file << "textures loaded\n";
return tex_loaded;}
//------------------------
void GLWidget::initializeGL(){
std::ofstream log_file("init_gl.log");
glDepthFunc(GL_LESS);
glDepthRange(-1.0,1.0);
glShadeModel(GL_SMOOTH);
glEnable(GL_CULL_FACE);      // не рисовать задние грани многоугольников
glEnable(GL_DEPTH_TEST);
glDepthFunc(GL_LEQUAL); //тип теста глибины.
glEnable(GL_POINT_SMOOTH);
qglClearColor(Qt::black);
log_file << "craeting universe\n"; log_file.flush();
visual_universe.deep_space.createUniverse(*e2.adb->data->global_space);
log_file << "creating orbits\n"; log_file.flush();
visual_universe.updateOrbits(e2.adb->data->getNativeSS().getPlanets(),QDateTime(QDate(1984,12,31)), e2.umodel->lifeTime.Time);
log_file << "craeting sidus borders\n"; log_file.flush();
visual_universe.deep_space.createConstellations(e2.adb->data->global_space->getConstellations());
log_file << "initing textures\n"; log_file.flush();
initialization::graphic::Texture_File textures=initialization::graphic::texture_names(textures_file);
log_file << "loading textures\n"; log_file.flush();
/*создание шрифтов*/
loadTextures(textures);
screen.ps_font = new FTGLPixmapFont("arial.ttf");
if (screen.ps_font->Error()) {
QMessageBox::about(NULL,"error!","Font loading error! :(");
this->close();} else {
screen.ps_font->FaceSize(screen.screen_parameters.getPGElements().text.ps.width);
this->screen.ps_font->CharMap(ft_encoding_unicode);}
screen.ds_font = new FTPixmapFont("ariali.ttf");
if (screen.ds_font->Error()) {
QMessageBox::about(NULL,"error!","Font loading error! :(");
this->close();} else {
screen.ds_font->FaceSize(screen.screen_parameters.getPGElements().text.ds.width);
this->screen.ds_font->CharMap(ft_encoding_unicode);}
this->screen.constellation_font=new FTPixmapFont("Arial.ttf");
this->screen.constellation_font->FaceSize(screen.screen_parameters.getPGElements().text.constellation.width);
this->screen.constellation_font->CharMap(ft_encoding_unicode);

this->screen.grid_font=new FTGLPixmapFont("Arial.ttf");
this->screen.grid_font->FaceSize(screen.screen_parameters.getPGElements().text.grid.width);
this->screen.grid_font->CharMap(ft_encoding_unicode);

if(!initExtensions())QMessageBox::about(this,"error","OpenGL extensions not initialized");
screen.infotext->init_titles();}
//------------------------------------------------------
void GLWidget::resizeGL(int w, int h){
glViewport(0,0,w,h);
glMatrixMode(GL_PROJECTION);glLoadIdentity();
GLfloat x= GLfloat(w)/h;
gluPerspective(e2.Galileo_Galilei->eye.Zoom(),x,10,coordinates::very_far*2*5);
glMatrixMode(GL_MODELVIEW);glLoadIdentity();
screen.screen_parameters.resize(w,h);
setWindowTitle(QString::number(w)+"x"+QString::number(h));}
//-----------------------
void GLWidget::paintGL(){
QPainter p(this);
glDisable(GL_CULL_FACE); // это помогает
init_OnScreenInfo();
//glEnable(GL_CULL_FACE);
glLoadIdentity();
/*if (e2.Galileo_Galilei->perception.what_i_see.universe[universe::ps::planet])*/ e2.umodel->updateSS();
if (e2.umodel->updatePM()) this->visual_universe.deep_space.update(*e2.adb->data->global_space);
double ecl_eq_angle=u_model::EclipAnglEquator(e2.umodel->lifeTime.Time.date()); // не обязательно считать каждый раз
using namespace electum_necessarius::imagination;
glClearColor(0,0,0,0);
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//почему экран очищается и без этого сам?
glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
//if (e2.location->on_ground){
//float ha = e2.umodel->lifeTime.toGMTf();
// visualization::init::observerGPHAlook(e2.Galileo_Galilei->eye.whereIsee(),e2.location->gp,-ha);}
//else
visualization::init::observer_look(e2.Galileo_Galilei->eye.whereIsee());
/*положение наблюдателя*/{
visualization::init::position::in_solar_system(*e2.location);
/*if (e2.location->on_ground){
const elementus_mundus::proxima_mundus::OrbiterBody* ob =
    dynamic_cast<const elementus_mundus::proxima_mundus::OrbiterBody*>(e2.location->getPlanet());
if (ob) visualization::init::planet_rotation(*ob,e2.location->gp,e2.umodel->lifeTime);
visualization::init::position::geopos(*e2.location);}*/}

const screen::plan_elements pe=this->screen.screen_parameters.getPGElements();
const whatISee& wis = e2.Galileo_Galilei->perception.what_i_see;
using namespace electum_necessarius::universe;
glRotatef(-ecl_eq_angle,1,0,0);
/*deep space*/{
if (wis.things[universe::ds::ds_image])
visualization::universe::deep_sky::sky_texture(this->visual_universe.deep_space.sky_texture[0]);
visualization::universe::deep_sky::all(*e2.adb->data->global_space,
        visual_universe.deep_space,e2.Galileo_Galilei->perception, this->screen.screen_parameters); // + where?
if (wis.isLvisible(ds::astra))
visualization::imaginary_draw::labels<profundus_mundus::AstraList>(*screen.ds_font, e2.adb->data->global_space->getStars(), e2.Galileo_Galilei->perception, pe.text.ds);
if (wis.isLvisible(ds::dso))
visualization::imaginary_draw::labels<profundus_mundus::DSOList>(*screen.ds_font, e2.adb->data->global_space->getDSOs(), e2.Galileo_Galilei->perception, pe.text.ds);
if (wis.isLvisible(ds::constellation))
visualization::imaginary_draw::labels<profundus_mundus::SidusList>(*screen.ds_font, e2.adb->data->global_space->getConstellations(), pe.text.ds);
if (e2.adb->selected!=NULL && e2.adb->selected->coordinate.Type()==coordinates::type::equator)
visualization::imaginary_draw::selected_mark(*e2.adb->selected,*screen.ps_font, screen.screen_parameters.getPGElements().ds_sel_mark_color);
if (wis.isLvisible(ds::dso))
visualization::imaginary_draw::labels<profundus_mundus::DSOList>(*screen.ds_font, e2.adb->data->global_space->getDSOs(), pe.text.ds);
//эксперементальная хрень рисующая юзерскую метку
/*visualization::elementus::markers(e2.Galileo_Galilei->getCaelestis());*/}
glRotatef(ecl_eq_angle,1,0,0);
/*solar system*/{
//кординатная сетка для солнечной системы
if (wis.things[grid::ec])
visualization::imaginary_draw::grid(pe.grid.ecliptic.color,pe.grid.ecliptic.width, coordinates::very_far-2,18,24);
if (!e2.location->on_ground) visualization::init::local_observers_center(*e2.location);
glPushMatrix();
if (e2.adb->selected!=NULL && e2.adb->selected->coordinate.Type()==coordinates::type::ecliptic)
if (!e2.location->on_ground || (e2.location->on_ground && e2.adb->selected->absID()!=e2.location->getPlanet()->absID()))
if (/*если диаметр слишком большой то не рисуем. пока нормальная выделялка не появилась...*/1)
visualization::imaginary_draw::selected_mark(*e2.adb->selected,*screen.ps_font, screen.screen_parameters.getPGElements().ps_sel_mark_color);
visualization::universe::solar_system::all(this->e2, visual_universe, pe); // +48
glPopMatrix();
if (wis.labels[ps::planet]){ /* подписи к планетам */
elementus_mundus::ID_Count_type except=0;
if (e2.location->on_ground) except = e2.location->getPlanet()->absID();
visualization::imaginary_draw::ss_labels(*screen.ps_font,e2.adb->data->getNativeSS().getPlanets(),
e2.Galileo_Galilei->perception.what_i_see, except, pe.text.ps);
if (except!=e2.adb->data->getNativeSS().getStar()->absID()){
int addxy[2];
float d=location::arc_diametr(*e2.location,e2.adb->data->getNativeSS().getStar())/2;
d/=e2.Galileo_Galilei->eye.getFoV();
d*=width(); addxy[0]=0; addxy[1] = d;
visualization::imaginary_draw::label(*screen.ps_font,*e2.adb->data->getNativeSS().getStar(),pe.text.ps,addxy);}
}}
glRotatef(-ecl_eq_angle,1,0,0);

/* если наблюдатель на земле */
if (e2.location->on_ground){using elementus_mundus::proxima_mundus::OrbiterBody;
const OrbiterBody* ob = dynamic_cast<const OrbiterBody*>(e2.location->getPlanet());
if (wis.things[grid::az]){      /*azimutal grid*/
glPushMatrix();
visualization::init::planet_rotation(*ob,e2.location->gp,e2.umodel->lifeTime);
visualization::imaginary_draw::grid(*screen.ps_font,pe.grid.azimuthal.color,pe.grid.azimuthal.width, coordinates::very_far/100,18,24);

glPopMatrix();}
if (/*wis.things[ground]*/1) {       /* quasi ground */
glPushMatrix();glColor3f(1,1,1);
visualization::init::planet_rotation(*ob,e2.location->gp,e2.umodel->lifeTime);
visualization::elementus::quasiGround();
visualization::imaginary_draw::equator(coordinates::very_far,360);
/*draw azimuts*/{
using namespace coordinates;
glColor3f(0.,0.,.0/*color*/);
//glLineWidth(width);
glDisable(GL_TEXTURE_2D);
glPixelTransferf(GL_RED_BIAS,   /*pe.grid.azimuthal.color[0]*/0);
glPixelTransferf(GL_GREEN_BIAS, /*pe.grid.azimuthal.color[1]*/1);
glPixelTransferf(GL_BLUE_BIAS,  /*pe.grid.azimuthal.color[2]*/0);
char text[16];
double coord[3];
float d;
for (unsigned i=0; i<360; i+=15){
sprintf(text,"%d",i);
d=converting::toRad*(i);
// NORTH is -1,0,-1 direction by some reason
// 10000 - is experimental number.
coord[0]=-coordinates::very_far/10000*cos(d);
coord[1]=0;
coord[2]=-coordinates::very_far/10000*sin(d);
glEnable(GL_BLEND);
visualization::elementus::text(*screen.grid_font, coord, text);}
glDisable(GL_BLEND);
glEnable(GL_TEXTURE_2D);
}

glPopMatrix();}}

screen.frames_per++;
glDisable(GL_CULL_FACE);
if (screen.fps_timer->isActive()) gl_timer->start(16);
if (screen.telescope) print_ocular(p);
if (screen.show_infotext) print_onScreenInfo(p);}
//------------------------------------
void GLWidget::print_ocular(QPainter& p){
//screen.infotext->set_telescope(my_telescope.t, my_telescope.y[my_telescope.o_index]);
//e2.Galileo_Galilei->eye.Zoom(1);
if (my_telescope.y.size() && my_telescope.t.size()){
const optical_system t=my_telescope.getTelescope(),o=my_telescope.getOcular();
float fx = width()/e2.Galileo_Galilei->eye.getFoV();
float r = out_fow(t,o)*fx;
p.setPen(Qt::red);
p.drawEllipse(width()/2-r/2,height()/2-r/2, r,r);

unsigned fh=16;
QFontMetrics fm(QFont("System", fh));
int left = fm.width("telescope               ");

float y = 10;
p.drawText(width()-left, fh*y++,"telesconpe");
p.drawText(width()-left, fh*y++,QString("apperture: ") + QString::number(t.d)+" mm");
p.drawText(width()-left, fh*y++,QString("focal length: ")+ QString::number(t.f)+" mm");
p.drawText(width()-left, fh*y++,QString("yey_piece"));
p.drawText(width()-left, fh*y++,QString("FoV: ")       + QString::number(o.d)+" °");
p.drawText(width()-left, fh*y++,QString("focal length: ") + QString::number(o.f)+" mm");
p.drawText(width()-left, fh*y++,QString("magnification: ")
                + QString::number(magnification(t,o)));
}}


//--------------
void GLWidget::print_onScreenInfo(QPainter& p){
//const screen::col_wid pe=this->screen.screen_parameters.getPGElements().text.screen;
p.setCompositionMode(QPainter::CompositionMode_SourceOver);
p.setBrush(Qt::NoBrush);
const col_wid cw = this->screen.screen_parameters.getPGElements().text.screen;
p.setPen(QColor(cw.color[0],cw.color[1], cw.color[2]));
p.setFont(QFont("Arial",cw.width));                             // цвет - параметр
QRect rect(0,0,width(),height());
p.drawText(rect,Qt::AlignBottom    | Qt::AlignLeft,getText(screen.infotext, op_info::informations::time));
p.drawText(rect,Qt::AlignTop | Qt::AlignLeft,getText(screen.infotext, op_info::informations::context));
p.drawText(rect,Qt::AlignBottom    | Qt::AlignRight,getText(screen.infotext, op_info::informations::location));
p.drawText(rect,Qt::AlignTop | Qt::AlignRight,getText(screen.infotext, op_info::informations::observer));

/*на чем сидим*/
//text = info_bar[right_bottom].text; i=-1;
//while(text[++i].length()){
//s_text = text[i].toStdString();
// screen.font->Render(s_text.c_str(), s_text.size(),
//             FTPoint(width() - screen.font->Advance(s_text.c_str(), s_text.size()) - margin, theight*i+theight/2));}
/*просмотр*/
//text = info_bar[left_top].text;i=-1;
//while(text[++i].length()){
//s_text = text[i].toStdString();
// screen.font->Render(s_text.c_str(), s_text.size(),
//            FTPoint(margin,height() - theight*i-theight*1.5));}
/*контекстная информация*/
/*text = info_bar[right_top].text; i=-1;
while(text[++i].length()){
s_text = text[i].toStdString();
 screen.font->Render(s_text.c_str(), s_text.size(),
     FTPoint(this->width() - screen.font->Advance(s_text.c_str(), s_text.size()) - margin, this->height() - theight*i-theight*1.5));}
glPopMatrix();
glPopMatrix();*/}
//-------------------------

void GLWidget::init_OnScreenInfo(){
screen.infotext->update(e2);}
//-------------------------------------------------------------------------------
unsigned long GLWidget::faceAtPosition(const QPoint &p){
double delX = 15, delY = 15; //ширина и высота региона выбора
const int maxsize  = 16;
GLuint buff[maxsize];
GLint viewport[4];
//for (int i=0; i<maxsize; i++)
//buff[i]=0;
glGetIntegerv(GL_VIEWPORT, viewport); 
glSelectBuffer(maxsize, buff);
glMatrixMode(GL_PROJECTION);//glLoadIdentity();
glPushMatrix();
glRenderMode(GL_SELECT);
glLoadIdentity();
gluPickMatrix(p.x(), viewport[3]-p.y(),delX,delY,viewport);
GLfloat x= GLfloat(width())/height();
gluPerspective(e2.Galileo_Galilei->eye.Zoom(),x,10,coordinates::very_far*2);
screen.screen_parameters.resize(width(),height());
double ecl_eq_angle=u_model::EclipAnglEquator(e2.umodel->lifeTime.Time.date());
//рисование
{
glMatrixMode(GL_MODELVIEW);
glLoadIdentity();

/*положение наблюдателя*/
{
/*if (e2.location->on_ground) visualization::init::observerGPHAlook(e2.Galileo_Galilei->eye.whereIsee(),e2.location->gp,0);
else */ visualization::init::observer_look(e2.Galileo_Galilei->eye.whereIsee());
visualization::init::position::in_solar_system(*e2.location);
if (e2.location->on_ground){
/*visualization::init::planet_rotation(*location->getPlanet(),location->gp,p_model->lifeTime);*/
/*visualization::init::position::geopos(*location);*/}}

glRotatef(-ecl_eq_angle,1,0,0);
//if (location->on_ground) init_celestial_sphere(p_model->lifeTime, location->gp);
elementus_mundus::ID_Count_type nd;
if (e2.location->on_ground)  nd = e2.location->getPlanet()->absID();
else nd=0;
select_astro::draw::all(*e2.adb,*e2.location,e2.umodel->lifeTime.Time, e2.Galileo_Galilei->perception, nd);
}
glRotatef(-ecl_eq_angle,1,0,0);

unsigned found = glRenderMode(GL_RENDER);
glMatrixMode(GL_PROJECTION);
glPopMatrix();
glMatrixMode(GL_MODELVIEW);
e2.Galileo_Galilei->sel_info.clear();

if (found>0){e2.adb->activateID(buff[3]);
/*const elementus_mundus::altus_mundus::Astrum *a=dynamic_cast<const elementus_mundus::altus_mundus::Astrum*>(e2.adb->selected);
if (a){
FILE* f = fopen("ast.txt","a");
unsigned long id = a->getIDin("HR");
fprintf(f,"%ld, ",id);
fclose(f);
}

/*indexMundus.toSelect(buff,found);
//Galileo_Galilei->sel_info = visualization::universe::select::get_sel_info(*u_model,indexMundus);
//Galileo_Galilei->eye.setFocusedCoord(visualization::universe::select
//            ::gel_selected_coordinates(*u_model,indexMundus));
/*set coordinates for following for target*/
//visual_universe.select.sel_id=buff[3];
return  buff[3];}
else { e2.Galileo_Galilei->eye.setFocusedCoord(NULL);return -1;}
}
//-------------------------
void GLWidget::update_fps(){
screen.fps = 1000.0*screen.frames_per/screen.fps_timer->interval();
screen.frames_per = 0;}
//----------------------
void GLWidget::showEvent(QShowEvent *e){
this->gl_timer->start();
this->screen.fps_timer->start();}
//------------------------
void GLWidget::hideEvent(QHideEvent *){
this->gl_timer->stop();
this->screen.fps_timer->stop();}
//--------------------------------------------------
void GLWidget::keyPressEvent(QKeyEvent *e){
switch(e->key()){
case Qt::Key_T: screen.telescope=!screen.telescope; break;
case Qt::Key_0: if (my_telescope.o_index+1<my_telescope.y.size())my_telescope.o_index++; break;
case Qt::Key_9: if (my_telescope.o_index>0)my_telescope.o_index--; break;
case Qt::Key_Down: s+=2; break;
case Qt::Key_Up: s-=2; break;
case Qt::Key_Left: a+=2; break;
case Qt::Key_Right: a-=2; break;
case Qt::Key_Play: e2.umodel->lifeTime.stort();
}

/*unsigned key =  e->nativeVirtualKey();
KEYB_PROC foo=controls.keyboard_functions[key];
if (foo!=NULL)foo(e2,*e);
controls.prev_key_pressed = e->nativeVirtualKey();*/}
//--------------------------------------------------
void GLWidget::mousePressEvent(QMouseEvent *e){
controls.x=e->x();controls.y=e->y();
controls.move = 0;}
//-----------------------------------------------
void GLWidget::mouseReleaseEvent(QMouseEvent *e){
//все case должны быть процедурами.
switch(e->button()){
//выбор объекта
case Qt::LeftButton: {if (!controls.move){faceAtPosition(e->pos());}break;}
// фигня. получение аз. координат по клику. косяк с матр. проекции
case Qt::MidButton: {add_user_mark(e->x(), e->y(),*e2.Galileo_Galilei); break;}
default: {break;}}

}
//--------------------------------------------------
void GLWidget::mouseMoveEvent(QMouseEvent *e){
this->controls.move = 1;
//коэф-т передачи движения мыши в перемещение взгляда наблюдателя. должно быть параметром
float k = 1./(pow(this->e2.Galileo_Galilei->eye.Zoom()/60.,0.08))/5;
switch(e->buttons()){
case Qt::LeftButton: {motion::in_space(*e2.location,-controls.x+e->x(), -controls.y+e->y(),k/2);
                        controls.x=e->x(); controls.y=e->y(); break;}
case Qt::RightButton: {motion::eye(e2.Galileo_Galilei->eye,-controls.x+e->x(), e->y()-controls.y,k);
                        controls.x=e->x(); controls.y=e->y(); break;}}}
//--------------------------------------------------
void GLWidget::wheelEvent(QWheelEvent *e){
float d_zoom =0.8;
//float d_range = .15;
//величана зума - параметр
if (e2.location->on_ground || e->modifiers()==Qt::ControlModifier){ // изменение угла поля зрения
if (e->delta()>0) e2.Galileo_Galilei->eye.Zoom(d_zoom);
else e2.Galileo_Galilei->eye.Zoom(1./d_zoom);
resizeGL(width(), height());}
else{char mode=2;
if (e->modifiers()==Qt::ShiftModifier) mode=3; else
if (e->modifiers()==Qt::AltModifier) mode=1;
motion::along_radius(*e2.location,mode,e->delta()/120);}   // 120 это про всех?
}
//--------------------------------------------------
void GLWidget::mouseDoubleClickEvent(QMouseEvent *e){
/*Galileo_Galilei->eye.toFocus();*/}
//-----------------------------

void GLWidget::getEvent(QKeyEvent *e){
keyPressEvent(e);
}

void GLWidget::screenshot(const char *filename){

}

void GLWidget::shide_src_info(){
screen.show_infotext=!screen.show_infotext;}




/*
содержимое методов лаконичо
утечка памяти из GlWidget
ошибка при уменьшении проницаемости. [установлен порог]
*/
