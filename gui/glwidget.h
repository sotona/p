﻿#ifndef GLWIDGET_H
#define GLWIDGET_H
#include <FTGL/ftgl.h>
#include <QtOpenGL/QtOpenGL>
#include <map>
#include "include/e2/observer.h"
#include "include/e2/e/tempus.h"
#include "include/e2/e/elementus_mundus.h"
#include "include/v/visualization.h"
#include "include/e2/e/pModel.h"
#include "include/e2/e2.h"
#include "invisible_interface.h"
#include "include/op_info.h"
#include "initialization.h"
#include "include/e2/telescope.h"

//class GLWidget;


#include "keyboard.h"

#define toStr(smtng) QString::number(smtng)

class GLWidget : public QGLWidget{Q_OBJECT

public:
    GLWidget(QGLContext *context = 0, QWidget *parent = 0);
    ~GLWidget();
unsigned loadTextures(initialization::graphic::Texture_File& file_tex);

    //может быть с этими классами как-то иначе поступить
    //не здорово что glwidget про всё занет
    //это относится и к приватной хрени ниже.
    E2 e2;

    float s,a;

    telescope_etc my_telescope;

    struct controls{
    int x, y;                   //старые координаты мыши. для перемешения мышью
    bool move;                  //было ли движение предыдущим действием?
    int prev_key_pressed;
    } controls;

    struct Screen{
    op_info::OPInfo *infotext;                   // класс управляющий экранным текстом (кроме подписей)
    bool show_infotext;
    float fps;
    unsigned frames_per;
    screen::ScreenParameters screen_parameters;
    QTimer *fps_timer;
    FTPixmapFont *ps_font;
    FTPixmapFont *ds_font;
    FTPixmapFont *constellation_font;
    FTPixmapFont *grid_font;
    bool telescope;

    } screen;
protected:
    void initializeGL();
    void resizeGL(int w, int h);
    //events
    void mousePressEvent(QMouseEvent *);
    void mouseDoubleClickEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *e);
    void wheelEvent(QWheelEvent *);
    void keyPressEvent(QKeyEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

    void paintGL();



    void init_OnScreenInfo();
private: 
    void print_onScreenInfo_ftgl();
    void print_onScreenInfo(QPainter& p);
    unsigned long faceAtPosition(const QPoint &p);

    // переделать
        /*map<pair<string, byte>, string>;
        std::vector< std::pair<pair<string, string>, byte>> txt_;*/

    QTimer *gl_timer;

    public:
    g_containers::gMundus visual_universe;
    private slots: void update_fps();
                   void getEvent(QKeyEvent* e);
    void print_ocular(QPainter& p);

   public slots:
        void screenshot(const char* filename);   // сделать скриншот
        void shide_src_info();                   // screen info SHow/hIDE
};

#endif
