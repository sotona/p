﻿#include "find.h"
#include "ui_find.h"
#include <QMessageBox>
#include <QTextCodec>
Find::Find(QWidget *parent) :QDialog(parent),
    ui(new Ui::Find){
    ui->setupUi(this);
    QTextCodec *codec = QTextCodec::codecForName("CP1251");
    QTextCodec::setCodecForTr(codec);
}

Find::~Find(){delete ui;}

void Find::on_findline_textEdited(QString s){
std::list<elementus_mundus::BasicAstro*> tmp;
find_list.clear();
if (ui->Astrum->isChecked())
find_list=adb->findAstras(s.toStdString().c_str());
if (ui->Asterizm->isChecked()){
tmp=adb->findAsterizm(s.toStdString().c_str());
find_list.insert(find_list.end(),tmp.begin(),tmp.end());}
if (ui->Planet->isChecked()){
tmp.clear();
tmp=adb->findPlanet(s.toStdString().c_str());
find_list.insert(find_list.end(),tmp.begin(),tmp.end());}
if (ui->Comete->isChecked()){
tmp.clear();
tmp=adb->findComet(s.toStdString().c_str());
find_list.insert(find_list.end(),tmp.begin(),tmp.end());}
if (ui->DSO->isChecked()){
tmp.clear();
tmp=adb->findDSO(s.toStdString().c_str());
find_list.insert(find_list.end(),tmp.begin(),tmp.end());}
ui->findresult->setRowCount(find_list.size());
ui->findresult->setColumnCount(2);
ui->findresult->setColumnWidth(0,200);
ui->findresult->setColumnWidth(1,200);
ui->finded->setText(tr("Найденно: ")+QString::number(find_list.size()));
int i=0;
for(std::list<elementus_mundus::BasicAstro*>::const_iterator it=find_list.begin(),end=find_list.end(); it!=end; it++){
if (dynamic_cast<elementus_mundus::altus_mundus::Astrum*>(*it))
ui->findresult->setItem(i,1,new QTableWidgetItem(tr("звезда")));
if (dynamic_cast<profundus_mundus::Asterizm*>(*it))
ui->findresult->setItem(i,1,new QTableWidgetItem(tr("астеризм")));
if (dynamic_cast<elementus_mundus::proxima_mundus::Planet*>(*it))
ui->findresult->setItem(i,1,new QTableWidgetItem(tr("планета/спутник")));
if (dynamic_cast<elementus_mundus::proxima_mundus::Cometa*>(*it))
ui->findresult->setItem(i,1,new QTableWidgetItem(tr("комета")));
if (dynamic_cast<elementus_mundus::altus_mundus::dso::DSO*>(*it))
ui->findresult->setItem(i,1,new QTableWidgetItem(tr("объект дальнего космоса")));
ui->findresult->setItem(i,0,new QTableWidgetItem(QString((*it)->name())));
i++;}}

void Find::on_findresult_doubleClicked(QModelIndex index){
for(std::list<elementus_mundus::BasicAstro*>::const_iterator it=find_list.begin(),end=find_list.end(); it!=end; it++)
if (!strcmp((*it)->name(),ui->findresult->item(index.row(),0)->text().toStdString().c_str()))
adb->activateID((*it)->absID());
}

void Find::on_all_stateChanged(int state){
ui->Astrum->setChecked(state);
ui->Asterizm->setChecked(state);
ui->Planet->setChecked(state);
ui->Comete->setChecked(state);
ui->DSO->setChecked(state);
}
