#include "cats.h"
#include "ui_cats.h"

Cats::Cats(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Cats)
{
    ui->setupUi(this);
}

Cats::~Cats()
{
    delete ui;
}
void Cats::setCats(catalogues::Catalogues &cats){
cats = cats;
const catalogues::CatDescriptors* cat;
cat=cats.getDSCatsInfo().at(cat_types::ds::star);
this->ui->starcount_label->setText(QString::number(cat->total_records));
this->ui->starcatcount_label->setText(QString::number(cat->cat_descriptor.size()));
this->ui->tableWidget_starcat->setRowCount(cat->cat_descriptor.size());
for (unsigned i=0; i<cat->cat_descriptor.size(); i++){
    QTableWidgetItem *item = new QTableWidgetItem(QString(cat->cat_descriptor.at(i).cat_name.c_str()));
    this->ui->tableWidget_starcat->setItem(i,0,item);
    item = new QTableWidgetItem(QString::number(cat->cat_descriptor.at(i).filename.size()));
    this->ui->tableWidget_starcat->setItem(i,1,item);
    item = new QTableWidgetItem(QString::number(cat->cat_descriptor.at(i).total_records));
    this->ui->tableWidget_starcat->setItem(i,2,item);
}

}

void Cats::on_buttonBox_accepted()
{

}
