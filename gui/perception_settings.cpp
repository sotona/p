﻿#include "perception_settings.h"
#include "ui_perception_settings.h"

using namespace electum_necessarius::universe;
using namespace electum_necessarius::imagination;
Setting::Setting(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::setting){
    wIs=new electum_necessarius::whatISee();
    ui->setupUi(this);
connect(ui->all,SIGNAL(set),ui->star,SLOT(setChecked(bool)));
//this->setLocale(QLocale(QLocale::Russian));
}

Setting::~Setting(){
//    delete ui;
/*    delete wIs;*/}

void Setting::setWhatISee(electum_necessarius::whatISee &_wIs){

wIs=&_wIs;
ui->star->setChecked(wIs->things[ds::astra]);
ui->star_text->setChecked(wIs->things[ds::astra]);
/* asterism */
ui->planet->setChecked(wIs->things[ps::planet]);
ui->planet_text->setChecked(wIs->labels[ps::planet]);
ui->planet_orbit->setChecked(wIs->things[orbit]);
/* asterism */
ui->asterizm->setChecked(wIs->things[asterism]);
ui->asterizm_text->setCheckable(wIs->labels[asterism]);
/* grid */
ui->az->setChecked(wIs->things[grid::az]);
ui->eqv->setChecked(wIs->things[grid::ec]);
ui->ecl->setChecked(wIs->things[grid::eq]);
/* other */
ui->ecliptic->setChecked(wIs->things[ecliptic_disc]);
ui->ds_image->setCheckable(wIs->things[ds::ds_image]);
ui->ground->setCheckable(wIs->things[ground]);

ui->odk->setChecked(wIs->things[ds::dso]);
ui->odk_text->setCheckable(wIs->labels[asterism]);
}

void Setting::on_star_stateChanged(int state){
wIs->things[ds::astra]=state;}

void Setting::on_planet_stateChanged(int state){
wIs->things[ps::planet]=state;}

void Setting::on_planet_orbit_stateChanged(int state){
wIs->things[orbit]=state;}

void Setting::on_asterizm_stateChanged(int state){
wIs->things[asterism]=state;}

void Setting::on_ecla_stateChanged(int state){
wIs->things[ecliptic_disc]=state;
}

void Setting::on_az_stateChanged(int state){
  wIs->things[grid::az]=state;}

void Setting::on_eqv_stateChanged(int state){
wIs->things[grid::eq]=state;    }

void Setting::on_ecl_stateChanged(int state){
wIs->things[grid::ec]=state;}

void Setting::on_all_stateChanged(int state){
ui->star->setChecked(state);
ui->asterizm->setChecked(state);
ui->planet->setChecked(state);
ui->odk->setChecked(state);
ui->comete->setChecked(state);
ui->nsat->setChecked(state);}

void Setting::on_all_orbit_stateChanged(int state){
ui->planet_orbit->setChecked(state);
ui->comete_orbit->setChecked(state);
ui->nsat_orbit->setChecked(state);
}

void Setting::on_all_text_stateChanged(int state){
ui->star_text->setChecked(state);
ui->asterizm_text->setChecked(state);
ui->planet_text->setChecked(state);
ui->odk_text->setChecked(state);
ui->comete_text->setChecked(state);
ui->nsat_text->setChecked(state);}


void Setting::on_star_text_stateChanged(int arg1){
wIs->labels[ds::astra]=arg1;}

void Setting::on_odk_text_stateChanged(int arg1){
wIs->labels[ds::dso]=arg1;}

void Setting::on_planet_text_stateChanged(int arg1){
wIs->labels[ps::planet]=arg1;
}

void Setting::on_odk_stateChanged(int arg1){
wIs->things[ds::dso]=arg1;}

void Setting::on_asterizm_text_stateChanged(int state){
wIs->labels[asterism]=state;}

void Setting::on_ground_stateChanged(int state){
wIs->things[ground]=state;}

void Setting::on_ds_image_stateChanged(int state){
wIs->things[ds::ds_image]=state;}

void Setting::on_ecliptic_stateChanged(int state){
wIs->things[ecliptic_disc]=state;}
