﻿#ifndef FIND_H
#define FIND_H

#include <QDialog>
#include "../include/e2/e/astro_db.h"
#include <QModelIndex>


namespace Ui {
    class Find;
}

class Find : public QDialog
{
    Q_OBJECT

public:
    explicit Find(QWidget *parent = 0);
    void setADB(AstroDB& _adb){adb=&_adb;};
    AstroDB& ADB(){return *adb;};
    ~Find();

private slots:
    void on_findline_textEdited(QString );

    void on_findresult_doubleClicked(QModelIndex index);

    void on_all_stateChanged(int );

private:
    Ui::Find *ui;
    AstroDB *adb;
    std::list<elementus_mundus::BasicAstro*> find_list;
};

#endif // FIND_H
