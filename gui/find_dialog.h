﻿#ifndef FIND_DIALOG_H
#define FIND_DIALOG_H
#include "include/e2/e/mundus.h"
#include <QTreeView>
#include <QDialog>

namespace Ui {
    class find_dialog;
}

class find_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit find_dialog(QWidget *parent = 0);
    void fillTree(const sistema_solare::SistemaSolare& ss);
    ~find_dialog();

private:
    Ui::find_dialog *ui;
};

#endif // FIND_DIALOG_H
