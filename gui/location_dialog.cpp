﻿#include "location_dialog.h"
#include "ui_location_dialog.h"

GraphicsScene::GraphicsScene(qreal x, qreal y, qreal width, qreal height,location::Location& _location,QObject *parent):QGraphicsScene(parent){
setSceneRect(x,y,width,height);
this->location=&_location;
double *xy=coordinates::converting::inEquidistant(_location.gp.Latitude(),_location.gp.Longitude(),0,0);
gel=addEllipse(sceneRect().width()*xy[0]-2,sceneRect().height()-sceneRect().height()*xy[1]-2,4,4,QPen(),QBrush(Qt::red,Qt::SolidPattern));
gel->setZValue(1);
delete[] xy;}


void GraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *event){
removeItem(gel);
gel=addEllipse(event->scenePos().x()-2,event->scenePos().y()-2,4,4,QPen(),QBrush(Qt::red,Qt::SolidPattern));
gel->setZValue(1);
double* latlon=coordinates::converting::fromEquidistant(event->scenePos().x()/sceneRect().width(),
                                                       (sceneRect().height()-event->scenePos().y())/sceneRect().height(),0,0);
location->gp=location::geo_pos::GeoPos(latlon[0],latlon[1]);
loc();}

void GraphicsScene::set_point(){
removeItem(gel);
double *xy=coordinates::converting::inEquidistant(location->gp.Latitude(),location->gp.Longitude(),0,0);
gel=addEllipse(sceneRect().width()*xy[0]-2,sceneRect().height()-sceneRect().height()*xy[1]-2,4,4,QPen(),QBrush(Qt::red,Qt::SolidPattern));
gel->setZValue(1);
delete[] xy;
}

Location_dialog::Location_dialog(location::Location &_location,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Location_dialog){
ui->setupUi(this);
setFixedSize(this->size());
this->location=&_location;
set_location();
textures=initialization::graphic::texture_names("textures/textures");
scene= new GraphicsScene(0,0,ui->geo->width()-ui->geo->frameWidth()*2,ui->geo->height()-ui->geo->frameWidth()*2,_location);
connect(scene,SIGNAL(loc()),this,SLOT(set_location()));
connect(this,SIGNAL(spoint()),scene,SLOT(set_point()));
ui->geo->setScene(scene);
pixmap_item=scene->addPixmap(QPixmap());
pixmap_item->setZValue(0);}

Location_dialog::~Location_dialog(){
    delete ui;
    delete pixmap_item;
    delete scene;
}

void Location_dialog::set_gp(){
 int k;int n;
k=(ui->northern_South->currentIndex()==1)?-1:1;
n=(ui->east_west->currentIndex()==1)?-1:1;
location->gp= location::geo_pos::GeoPos(coordinates::converting::HorMinSecToFloat(k*ui->lat_deg->value(),k*ui->lat_minute->value(),k*ui->lat_second->value()),
                                        coordinates::converting::HorMinSecToFloat(n*ui->log_deg->value(),n*ui->log_minute->value(),n*ui->log_second->value()),
                                        ui->height->value());
spoint();
}
void Location_dialog::set_location(){
float lat_,log_;
lat_=location->gp.Latitude();
log_=location->gp.Longitude();
ui->height->setValue(location->gp.Height());
if (lat_<0){
ui->northern_South->setCurrentIndex(1); }
else
ui->northern_South->setCurrentIndex(0);
if (log_<0){
ui->east_west->setCurrentIndex(1); }
else
ui->east_west->setCurrentIndex(0);
int* lat=coordinates::converting::floatHourToHMS(fabs(lat_));
int* log=coordinates::converting::floatHourToHMS(fabs(log_));
 ui->lat_deg->setValue(lat[0]);
ui->lat_minute->setValue(lat[1]);
ui->lat_second->setValue(lat[2]);
ui->log_deg->setValue(log[0]);
ui->log_minute->setValue(log[1]);
ui->log_second->setValue(log[2]);}

void Location_dialog::fillTree(const planetary_system::PlanetarySystem &ss){
 planet_system=ss.getPlanets();
planetary_system::PlanetList pl= ss.getPlanets();
QTreeWidgetItem*  planet;
QTreeWidgetItem* nsat;
QTreeWidgetItem* current;
QTreeWidgetItem* Sun=new QTreeWidgetItem(ui->solare_list);
Sun->setText(0,QString(ss.getStar()->name()));
current=Sun;
for(planetary_system::PlanetList::const_iterator it=pl.begin(),end=pl.end(); it!=end;it++){
planet=new QTreeWidgetItem(Sun);
planet->setText(0,QString((*it)->name()));
if ((*it)==location->getPlanet())
current=planet;
if((*it)->getNSats()->size()>0)
for(unsigned i=0; i<(*it)->getNSats()->size();i++){
nsat=new QTreeWidgetItem(planet);
nsat->setText(0,QString((*it)->getNSats()->at(i)->name()));
if ((*it)->getNSats()->at(i)==location->getPlanet())
current=nsat;}}
ui->solare_list->setCurrentItem(current);
char *name=strlwr(strdup(location->getPlanet()->name()));
QPixmap map_(QString(textures[std::string(name)].c_str()));
map_=map_.scaled(ui->geo->width(),ui->geo->height(),Qt::KeepAspectRatio);
pixmap_item->setPixmap(map_);
delete name;
}

void Location_dialog::on_solare_list_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous){
for(planetary_system::PlanetList::const_iterator it=planet_system.begin(),end=planet_system.end(); it!=end;it++){
if (current->text(0)=="Sun"){
location->setPlanet(new elementus_mundus::proxima_mundus::Planet());
QPixmap map_;
map_=map_.scaled(ui->geo->width(),ui->geo->height(),Qt::KeepAspectRatio);
pixmap_item->setPixmap(map_);
break;}
if ((current->text(0)==QString((*it)->name()))){
location->setPlanet(*it);
char *name=strlwr(strdup(location->getPlanet()->name()));
QPixmap map_(QString(textures[std::string(name)].c_str()));
map_=map_.scaled(ui->geo->width(),ui->geo->height(),Qt::KeepAspectRatio);
pixmap_item->setPixmap(map_);
delete name;break;}
if (current->parent() && (*it)->getNSats()->size()>0){
for(unsigned i=0; i<(*it)->getNSats()->size();i++){
if (current->text(0)==QString((*it)->getNSats()->at(i)->name())){
location->setPlanet((*it)->getNSats()->at(i));
char *name=strlwr(strdup(location->getPlanet()->name()));
QPixmap map_(QString(textures[std::string(name)].c_str()));
map_=map_.scaled(ui->geo->width(),ui->geo->height(),Qt::KeepAspectRatio);
pixmap_item->setPixmap(map_);
delete name;
break;
}}}}}

void Location_dialog::on_lat_deg_valueChanged(int value ){set_gp();}
void Location_dialog::on_lat_minute_valueChanged(int ){set_gp();}
void Location_dialog::on_lat_second_valueChanged(int ){set_gp();}
void Location_dialog::on_northern_South_currentIndexChanged(QString ){set_gp();}
void Location_dialog::on_east_west_currentIndexChanged(int index){set_gp();}
void Location_dialog::on_log_deg_valueChanged(int ){set_gp();}
void Location_dialog::on_log_minute_valueChanged(int ){set_gp();}
void Location_dialog::on_log_second_valueChanged(int ){set_gp();}
void Location_dialog::on_height_valueChanged(int ){set_gp();}

void Location_dialog::on_extended_clicked(){
ui->geo->setVisible(!ui->geo->isVisible());
ui->geo_def->setVisible(!ui->geo_def->isVisible());
ui->minute->setVisible(!ui->minute->isVisible());
ui->second->setVisible(!ui->second->isVisible());
ui->deg->setVisible(!ui->deg->isVisible());
ui->lat_deg->setVisible(!ui->lat_deg->isVisible());
ui->lat_label->setVisible(!ui->lat_label->isVisible());
ui->lat_minute->setVisible(!ui->lat_minute->isVisible());
ui->lat_second->setVisible(!ui->lat_second->isVisible());
ui->log_deg->setVisible(!ui->log_deg->isVisible());
ui->log_label->setVisible(!ui->log_label->isVisible());
ui->log_minute->setVisible(!ui->log_minute->isVisible());
ui->log_second->setVisible(!ui->log_second->isVisible());
ui->height->setVisible(!ui->height->isVisible());
ui->height_label->setVisible(!ui->height_label->isVisible());
ui->east_west->setVisible(!ui->east_west->isVisible());
ui->northern_South->setVisible(!ui->northern_South->isVisible());
ui->coordinates->setVisible(!ui->coordinates->isVisible());
if (ui->buttonBox->orientation()==Qt::Horizontal)
ui->buttonBox->setOrientation(Qt::Vertical);else
ui->buttonBox->setOrientation(Qt::Horizontal);
layout()->setSizeConstraint( QLayout::SetFixedSize );
adjustSize();
}
