#ifndef CATS_H
#define CATS_H

#include <QDialog>
#include "../include/e2/e/catalogues.h"

namespace Ui {
    class Cats;
}

class Cats : public QDialog
{
    Q_OBJECT

public:
    explicit Cats(QWidget *parent = 0);
    void setCats(catalogues::Catalogues& cats);
    catalogues::Catalogues *cats;                   // �� �����.
    ~Cats();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Cats *ui;
};

#endif // CATS_H
