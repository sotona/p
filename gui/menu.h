﻿#ifndef MENU_H
#define MENU_H
#include <QMenuBar>
//#include "glwidget.h"
#include "keyboard.h"

namespace Ui {
class MenuBar;
}
class GLWidget;
class MenuBar:public QMenuBar
{
    Q_OBJECT
private:
    Interface  *_interface;
    Ui::MenuBar *ui;

public:
    MenuBar(QWidget *parent = 0);
    MenuBar(QWidget *parent,Interface &__interface);
    ~MenuBar();
    void setinterface(Interface &__interface){_interface=&__interface;}
signals:
    void on_fulscreen();
    void exit_program();

private slots:
    void on_time_inc_triggered();
    void on_time_dec_triggered();


    void on_SetDateTime_triggered();
    void on_current_time_triggered();
    void on_stop_time_triggered();
    void on_set_back_time_triggered();
    void on_Set_normal_time_triggered();
    void on_set_location_triggered();
    void on_setting_map_triggered();
    void on_find_triggered();
    void on_about_program_triggered();
    void on_fullscreen_triggered();
    void on_about_Qt_triggered();
    void on_action_triggered();
    void on_controls_help_triggered();
    void on_action_2_triggered();
    void on_settings_triggered();
    void on_cats_triggered();
    void on_on_display_settings_triggered();
};

#endif // MENU_H
