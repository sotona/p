﻿#include "menu.h"
#include <QMessageBox>
#include <QTextBrowser>
#include <QLabel>
#include "ui_menu.h"
#include "keyboard.h"
MenuBar::MenuBar(QWidget *parent) :
    QMenuBar(parent),
    ui(new Ui::MenuBar){
    ui->setupUi(this);
}
MenuBar::MenuBar(QWidget *parent,Interface &__interface):
QMenuBar(parent),_interface(&__interface),
ui(new Ui::MenuBar){
ui->setupUi(this);
}
MenuBar::~MenuBar(){
    delete ui;}

void MenuBar::on_time_inc_triggered(){
   QKeyEvent *ke=new QKeyEvent(QEvent::None,Qt::Key_L,Qt::NoModifier,NULL,false,1);
   _interface->do_something(ke);}

void MenuBar::on_time_dec_triggered(){
    QKeyEvent *ke=new QKeyEvent(QEvent::None,Qt::Key_J,Qt::NoModifier,NULL,false,1);
    _interface->do_something(ke);}

void MenuBar::on_SetDateTime_triggered(){
    QKeyEvent *ke=new QKeyEvent(QEvent::None,Qt::Key_F2,Qt::NoModifier,NULL,false,1);
    _interface->do_something(ke);}

void MenuBar::on_current_time_triggered(){
    QKeyEvent *ke=new QKeyEvent(QEvent::None,186,Qt::NoModifier,NULL,false,1);
    _interface->do_something(ke);
}

void MenuBar::on_stop_time_triggered(){
    QKeyEvent *ke=new QKeyEvent(QEvent::None,Qt::Key_K,Qt::NoModifier,NULL,false,1);
    _interface->do_something(ke);}

void MenuBar::on_setting_map_triggered(){}

void MenuBar::on_set_back_time_triggered(){
    QKeyEvent *ke=new QKeyEvent(QEvent::None,Qt::Key_Backspace,Qt::NoModifier,NULL,false,1);
    _interface->do_something(ke);
}

void MenuBar::on_Set_normal_time_triggered(){
    QKeyEvent *ke=new QKeyEvent(QEvent::None,222,Qt::NoModifier,NULL,false,1);
    _interface->do_something(ke);}

void MenuBar::on_set_location_triggered(){
    QKeyEvent *ke=new QKeyEvent(QEvent::None,Qt::Key_F4,Qt::NoModifier,NULL,false,1);
    _interface->do_something(ke);}

void MenuBar::on_find_triggered(){
    QKeyEvent *ke=new QKeyEvent(QEvent::None,Qt::Key_F3,Qt::NoModifier,NULL,false,1);
    _interface->do_something(ke);}

void MenuBar::on_about_program_triggered(){
QTextFormat tf;
// как это всё вывести столбиком, чтобы текст в каждой строке был точно по центру?
QMessageBox::about(this,"About program","Authors\nVetrov Sergey\nsome Terentiev Pavel\nand\nother noname peoples help us");}

void MenuBar::on_fullscreen_triggered(){
on_fulscreen();}

void MenuBar::on_about_Qt_triggered(){
QMessageBox::aboutQt(this);}

void MenuBar::on_action_triggered(){
exit_program();}

void MenuBar::on_controls_help_triggered(){
    QTextBrowser *ch = new QTextBrowser;
    ch->setText(QString("There is beautiful text about controls with pictures\n")
                        +QString("there is loaded (generated) from controls file"));
    ch->show();
}

void MenuBar::on_action_2_triggered(){
    QTextBrowser *ch = new QTextBrowser;
        ch->setText("There might been checkboxes of cats and other information about cats.");
        ch->show();
}

void MenuBar::on_settings_triggered(){
QTextBrowser *ch = new QTextBrowser;
    ch->setText("There might been settings");
    ch->show();
}

void MenuBar::on_cats_triggered(){
    QKeyEvent *ke=new QKeyEvent(QEvent::None,Qt::Key_F3,Qt::ControlModifier,NULL,false,1);
    _interface->do_something(ke);
}

void MenuBar::on_on_display_settings_triggered(){
    QKeyEvent *ke=new QKeyEvent(QEvent::None,Qt::Key_F5,Qt::NoModifier,NULL,false,1);
    _interface->do_something(ke);
}
