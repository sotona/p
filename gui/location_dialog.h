﻿#ifndef FIND_DIALOG_H
#define FIND_DIALOG_H
#include "include/e2/e/mundus.h"
#include <QTreeView>
#include<QTreeWidgetItem>
#include <QDialog>
#include "../include/e2/location.h"
#include <map>
#include <string>
#include "initialization.h"
#include "QGraphicsScene"
#include "QGraphicsPixmapItem"
#include "QGraphicsSceneMouseEvent"

namespace Ui {
    class Location_dialog;
}

class GraphicsScene:public QGraphicsScene{
Q_OBJECT
public:
    GraphicsScene(qreal x, qreal y, qreal width, qreal height,location::Location &_location,QObject *parent = 0);
private:
    location::Location *location;
    QGraphicsEllipseItem *gel;
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
signals:
        void loc();
public slots:
        void set_point();
};

class Location_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Location_dialog(location::Location &_location,QWidget *parent = 0);
    void fillTree(const planetary_system::PlanetarySystem& ss);
    ~Location_dialog();

private slots:
    void on_solare_list_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous);
    void on_lat_deg_valueChanged(int );
    void on_lat_minute_valueChanged(int );
    void on_lat_second_valueChanged(int );
    void on_northern_South_currentIndexChanged(QString );
    void on_east_west_currentIndexChanged(int index);
    void on_log_deg_valueChanged(int );
    void on_log_minute_valueChanged(int );
    void on_log_second_valueChanged(int );
    void on_height_valueChanged(int );
    void on_extended_clicked();

private:
    GraphicsScene *scene;
    QGraphicsPixmapItem *pixmap_item;
    Ui::Location_dialog *ui;
    planetary_system::PlanetList planet_system;
    location::Location *location;
    std::map<std::string,std::string> textures;
signals:
    void spoint();
public slots:
    void set_gp();
    void set_location();
};

#endif // FIND_DIALOG_H
