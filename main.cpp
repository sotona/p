﻿#include <fstream>
#include <ostream>
#include <QApplication>
#include <QSplashScreen>

#include "include/e2/e/tempus.h"
#include "include/e2/location.h"

#include "include/v/visualization.h"
#include "initialization.h"
#include "keyboard.h"
#include "include/e2/e2.h"

#include "gui/menu.h"
#include "gui/glwidget.h"
#include "gui/window.h"
#include <QtPlugin>

#include "include/e2/telescope.h"

GLWidget *glw;

namespace war{ //programm warnings messages
const char* warn_messages[] = {"No catalogs founds. Sky and solar system will be inaccessible :|",
                             "No textures loaded. You dont see sky images, and planets surfaces :|"};
const unsigned char no_cats = 0;
const unsigned char no_texture = 1;}

namespace init_stages{
const char* cats = "initialazing catalogues...";
const char* e2 = "initializing planetarium data...";
const char* cats_content = "loading catalogues...";
const char* key_act = "initializing keyboard functions...";
const char* gra_int = "initializing graphic and interface...";
const char* final_graphic = "final initializing of graphic...";
}

inline void update_splash(QApplication& a, QSplashScreen* s, const char* text){
s->showMessage(text,Qt::AlignLeft | Qt::AlignBottom, Qt::white);
s->showMessage(dont_panic,Qt::AlignRight,QColor(255,0,0));
s->repaint();a.processEvents();}

//---------------------------------------------
int main(int argc, char *argv[]){
QApplication a(argc, argv);
std::ofstream log_file(file::log::main); // главный лог

log_file << "checking OpenGL support...  ";
if (!QGLFormat::hasOpenGL()){
QMessageBox::about(NULL,"Sorry", "this system has no OpenGL support. Program will not work :(");
log_file << "this system has no OpenGL support\n exit.";log_file.close();return a.exec();}
log_file << "okay, this system has supporting OpenGL.\n";

QSplashScreen *splash = initialization::splash_screen(a,file::splash);
update_splash(a,splash,init_stages::cats);
log_file << init_stages::cats; log_file.flush();

catalogues::Catalogues *cat = new catalogues::Catalogues(file::cat_file);
if (!cat->isOpen()) {QMessageBox::about(NULL,"warning!", war::warn_messages[war::no_cats]);
log_file << "no catalogues found" << "\n"; log_file.flush();
a.exit();
return 0;   // освобождение памяти? не не слышал.
}
update_splash(a,splash,init_stages::cats_content);
log_file << init_stages::cats_content << "\n"; log_file.flush();

AstroData ad = cat->load_data();
AstroDB *adb = new AstroDB(ad);
E2 e2;
e2.cat = cat;
e2.adb = adb;

update_splash(a,splash,init_stages::e2);
log_file << init_stages::e2 << "\n"; log_file.flush();

e2.umodel = initialization::engine::pModel(ad);
e2.location = initialization::engine::Location(file::location, e2.adb->data->getNativeSS().getPlanets());
e2.Galileo_Galilei = initialization::engine::observer(file::observer);

log_file << init_stages::key_act << "\n"; log_file.flush();
KeyActArray *keyact=new KeyActArray;
init_keyb_actions(*keyact);

log_file << init_stages::gra_int << "\n"; log_file.flush();
GLWidget *gl_widget = new GLWidget;
screen::plan_elements pge;
gl_widget->e2=e2;
initialization::screen_params::graphic_elements(pge,file::graparam);
gl_widget->screen.screen_parameters.setPGElements(pge);
gl_widget->my_telescope=initialization::telescope(file::telescope);
Interface *_interface=new Interface(*keyact,e2);
/*интерфейс - виджет*/
QObject::connect(_interface, SIGNAL(inc_scr_info()),  gl_widget->screen.infotext,SLOT(inc_scr_info()));
QObject::connect(_interface, SIGNAL(dec_scr_info()),  gl_widget->screen.infotext,SLOT(dec_scr_info()));
QObject::connect(_interface, SIGNAL(shide_scr_info()),gl_widget,SLOT(shide_src_info()));
QObject::connect(_interface, SIGNAL(screenshot()),    gl_widget,SLOT(screenshot()));

MenuBar *menu = new MenuBar;
QObject::connect(menu,SLOT(triggered(QAction*)),_interface,SLOT(recv_asction(QAction*)));

menu->setinterface(*_interface);

Window window;
QObject::connect(&window,SIGNAL(key_pressed(QKeyEvent*)),gl_widget,SLOT(getEvent(QKeyEvent*))); //передача события о нажатой клавише. не работатет не со слотами.
QObject::connect(menu, SIGNAL(on_fulscreen()), &window,SLOT(full_screen()));
QObject::connect(menu, SIGNAL(exit_program()), &window,SLOT(close()));

window.setCentralWidget(gl_widget);
window.setMenuBar(menu);
window.setinterface(_interface);
log_file << init_stages::final_graphic << "\n"; log_file.flush();

{int width = a.desktop()->width();       /*если рабочий стол расширен на два монитора*/
if (width/a.desktop()->height() > 16./9.) width/=2;
window.resize(width/1.61, a.desktop()->height()/1.61);}

log_file.close();
splash->showMessage("Initializing graphic...",Qt::AlignLeft | Qt::AlignBottom, Qt::white);
window.showNormal();

splash->finish(&window);
delete splash;
return a.exec();
}




/*
• может быть замутить консоль для проги?
 тогда пригодилась бы функция задания угла поля зрения
 геопоса
 времени
 
• измерение угловых расстояний а-ля гугл мапс
• дисковое поле зрения
• f1 - справка. управление.
• если бы я пользовался этой прогой то мне захотелось бы видеть  линию горизонта. хотя бы линию.
 и конечно ставить небольшие метки, щёлкая на которые я бы смог прочитать написать коммент к этой точке
 небесной сферы.
 комменты могли бы быть вечными или однодневными и листая журнал я бы смог посмотреть комменты за определённый день.


+ см. комменты где-то ещё на какой-то на бумажке

*/
