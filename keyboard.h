﻿#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <QObject>
#include <QAction>
#include "include/e2/e2.h"
#include "QKeyEvent"
#include "QDateTimeEdit"
void set_central_planet(E2& e2);

typedef int(*KEYB_PROC)(E2&, int mode);
#define KeyActArray std::map<int,KEYB_PROC>

class Interface:public QObject{
Q_OBJECT
private:
 E2 e2;
 KeyActArray key_actions;
public:
 Interface(){}
 Interface(KeyActArray _key_actions,E2 _e2);
 void set_e2(E2 _e2){e2=_e2;}
 void setkey_actions(KeyActArray _key_actions){key_actions=_key_actions;}
 ~Interface(){}
 int do_something(QKeyEvent*);
 public slots:
 void recv_asction(QAction* a);

 signals:
  void dec_scr_info();
  void inc_scr_info();
  void shide_scr_info();
  void screenshot();
};

namespace key_actions{
namespace cntrl_parameters{
#define hor_quantum 1
#define vert_quantum 1
#define time_quantum 10
#define time_quantumX 1.125
#define vmag_q 0.5
#define vmag_k 1./32
}

int left(E2& e2,int mode);
int setdatetime(E2& e2,int mode);
int right(E2& e2, int mode);
int up(E2& e2, int mode);
int down(E2& e2, int mode);
int time_stort(E2& e2, int mode);
int inc_time_speed(E2& e2, int mode);
int dec_time_speed(E2& e2, int mode);
int a_key(E2& e2, int mode);
int grid(E2& e2, int mode);
int fullscreen(E2& e2, int mode);
int esc(E2& e2, int mode);
int set_current_time(E2& e2, int mode);
int normal_time_speed(E2& e2, int mode);
int n_astr(E2& e2, int mode);
int sh_caelestis(E2& e2, int mode);
int sh_planets(E2& e2, int mode);
int to_from_space(E2& e2, int mode);
int observer_settings(E2& e2, int mode);
int set_back_time(E2& e2, int mode);
int set_location(E2& e2, int mode);
//----------------------------------------------
/*int esc(E2& e2,const QKeyEvent &e){
}*/}

namespace motion{
void in_space(location::Location& location, int x_motion,int y_motion, double motion_k);
void eye(oculus::Oculus &eye, int x_motion, int y_motion, double k_motion);
//accyracy_mode: high(1), normal(2), low(3);
void along_radius(location::Location& location,unsigned char accuracy_mode, int _delta);}

void init_keyb_actions(KeyActArray& keyboard_functions);

#endif // KEYBOARD_H
