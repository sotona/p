﻿#ifndef INVISIBLE_INTERFACE_H
#define INVISIBLE_INTERFACE_H
#include "include/e2/oculus.h"
#include "include/e2/observer.h"
#include "GL/glu.h"
using namespace electum_necessarius;

void add_user_mark(int x, int y, observer::Observer& o);

#endif // INVISIBLE_INTERFACE_H
